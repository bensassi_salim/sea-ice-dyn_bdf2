! This module contains the routines that are used to limit the computational domain and 
! set boundary conditions.
!
! Original Author: Clinton Seinen
!
! The contained routines are:
!           - compress_ind_dist
!           - create_comp_msk_dist
!           - correct_boundary
!           - correct_dist
!           - uselandBC
!           - inIce 
!           - locate_halo_cells 
!           - update_halo_points
!           - clear_halo_points
!           - uBC
!           - vBC
!           - dudxBC
!           - dudyBC
!           - dvdxBC
!           - dvdyBC

module domain_routines

! -----------------------------------------------------------------------------!
    !                       Module References                           !
    use modelparams         ! includes physical, numerical, and grid parameters
    use initialization      ! includes routines for initializing the solver and includes grid parameters that are defined according to dx and will remain unchanged
    use csv_file            ! module for writing data to csv format
    use validsoln_routines  ! module containing calcs for the solution to the validation problem
! -----------------------------------------------------------------------------!

implicit none

! -----------------------------------------------------------------------------!
    !                           Contains                                !
! -----------------------------------------------------------------------------!

contains

!===========================================================================================!
!                               COMPRESS INDICES                                            !
!===========================================================================================!
    subroutine compress_ind_dist(indxui, indxuj, indxvi, indxvj, &
                        indxTi, indxTj, indxNi, indxNj, indxAdvVi, indxAdvVj, &
                        nU_pnts, nV_pnts, nT_pnts, nN_pnts, nAdvVols, &
                        dist_T, dist_N, &
                        nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)
        !=====================================================================!
        ! This subroutine creates a compressed index in order to limit 
        ! calculations to the locations where sea ice is located. Doing this
        ! will set the size of our system matrix to be nU_pnts + nV_pnts. 
        ! U and V points are contained in the domain if the distance function
        ! is positive at the point.
        !
        ! This subroutine also limits the calculations at T and N points, 
        ! according to if they are contained in the ice or not.
        ! 
        ! This routine also determines what node points are required for the 
        ! central scheme that is used for advection and stores their locations.
        !   -   The points are flagged in the loop used in setting the T points 
        !       in the computational domain. 
        !   -   The loop through the node points then stores the necessary locations.
        !
        !=====================================================================!
        
        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) ::  &
            nxU,    nyU,        &   ! limit of indices for u points (not including ghost cells)
            nxV,    nyV,        &   ! limit of indices for v points (not including ghost cells)
            nxT,    nyT,        &   ! limit of indices for T points (not including ghost cells)
            nxN,    nyN             ! limit of indices for N points (not including ghost cells)
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_T
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            dist_N
        
        !==================!
        !------In/out------!
        !==================!
        integer(kind = int_kind), intent(inout) :: &
            nU_pnts,    nV_pnts,    &   ! number of U/V points in comp. domain 
            nT_pnts,    nN_pnts,    &   ! number of T/N points in comp. domain 
            nAdvVols                    ! number of Advection Volumes (Node points)
        integer(kind = int_kind), dimension ((nxU+2*gc_U)*(nyU+2*gc_U)), intent(inout) :: &
            indxUi,     indxUj                          ! grid locations of u points in the comp. domain
        integer(kind = int_kind), dimension ((nxV+2*gc_V)*(nyV+2*gc_V)), intent(inout) :: &
            indxVi,     indxVj                          ! grid locations of v points in the comp. domain
        integer(kind = int_kind), dimension ((nxT+2*gc_T)*(nyT+2*gc_T)), intent(inout) :: &    
            indxTi,     indxTj                          ! grid locations of T points in the comp. domain
        integer(kind = int_kind), dimension (nxN*nyN), intent(inout) :: &    
            indxNi,     indxNj,     &                   ! grid locations of N points in the comp. domain
            indxAdvVi,  indxAdvVj                       ! grid locations of advection volumes (node points)

        !==================!
        ! -- LOCAL VARS -- !
        !==================!
        integer(kind = int_kind) :: &
            i,  j   ! local indices
        real(kind = dbl_kind) :: &
            dist    ! stores interpolated distance values
        logical, dimension(nxN, nyN) :: &
            AdvVol  ! used to flag where control volumes are required

        ! initialize arrays !
        AdvVol = .false. 
        indxUi = 0
        indxUj = 0
        indxVi = 0
        indxVj = 0
        indxTi = 0
        indxTj = 0
        indxAdvVi = 0
        indxAdvVj = 0

        !---nU_pnts----!
        nU_pnts = 0
        do j = 1, nyU
            do i = 1, nxU
                ! interpolate distance value !
                dist = (dist_T(i-1,j) + dist_T(i,j))/2

                if ((dist < 0) .or. (lmsk(i-1,j) .or. lmsk(i,j))) then
                    ! If the u point is not with-in the ice pack or is on the edge of a 
                    ! land cell, don't include in the comp. domain    
                else
                    nU_pnts = nU_pnts + 1
                    indxUi(nU_pnts) = i
                    indxUj(nU_pnts) = j
                end if
            end do
        end do
        
        !---nV_pnts----!
        nV_pnts = 0
        do j = 1, nyV
            do i = 1, nxV
                ! interpolate distance value !
                dist = (dist_T(i,j-1) + dist_T(i,j))/2

                if ((dist < 0) .or. (lmsk(i, j-1) .or. lmsk(i, j))) then
                    ! If the v point is not with-in the ice pack or is on the edge of a 
                    ! land cell, don't include in the comp. domain
                else
                    nV_pnts = nV_pnts + 1
                    indxVi(nV_pnts) = i
                    indxVj(nV_pnts) = j
                end if
            end do
        end do
        
        !---nT_pnts----!
        ! NOTE: this currently only checks for one row of ghost cells as only one row will be needed for most computations.
        nT_pnts = 0
        do j = 0, nyT + 1
            do i = 0, nxT + 1
                if (dist_T(i,j) < 0) then   
                    ! T point not in ice, ignore in calculations 
                else
                    ! count and store location for comp domain !
                    nT_pnts = nT_pnts + 1                                   
                    indxTi(nT_pnts) = i
                    indxTj(nT_pnts) = j

                    ! flag surrounding node points as advection volumes !
                    AdvVol(i  ,j  ) = .true.
                    AdvVol(i+1,j  ) = .true.
                    AdvVol(i+1,j+1) = .true.
                    AdvVol(i  ,j+1) = .true.
                end if
            end do
        end do

        nN_pnts     = 0
        nAdvVols    = 0
        !---nN_pnts-----!
        do j = 1, nyN
            do i = 1, nxN

                ! Flag Points for comp. domain !
                if (dist_N(i,j) < 0) then 
                    ! N point not in ice, ignore in calculations
                else
                    nN_pnts = nN_pnts + 1                               
                    indxNi(nN_pnts) = i
                    indxNj(nN_pnts) = j
                end if 

                ! Count and store locations for Advection Volumes !
                if (AdvVol(i,j)) then 
                    nAdvVols = nAdvVols + 1
                    indxAdvVi(nAdvVols) = i 
                    indxAdvVj(nAdvVols) = j
                end if 
            end do                                                 
        end do
    end subroutine compress_ind_dist

!===========================================================================================!
!                                   CREAT COMP MSK                                          !
!===========================================================================================!
    subroutine create_comp_msk_dist(ulmsk, uimsk, vlmsk, vimsk, &
                        ugrid, vgrid, dist_T, dist_N, dist_gx_T, dist_gy_T, &
                        indxUi, indxUj, indxVi, indxVj, &
                        nV_pnts, nU_pnts, nxU, nyU, nxV, nyV, nxN, nyN, nxT, nyT)
    
        !======================================================================
        ! This subroutine creates the computational masks needed for:
        !   - the construction of the matrix vector multiplication (Au) and the RHS 
        !     vector b. 
        !   - the calculation of delta in the viscosity calculations
        !   - the calculation of the water drag coefficients
        !
        ! The arrays *lmsk and *lmsk enforce a dirichlet BC at land
        ! locations (u=v=0), while *imsk and *imsk enforce an Neumann BC at 
        ! open boundaries.
        !
        ! NOTES:
        !       -   the land masks are only activated if ice is actually attached to
        !           land cells (i.e. the distance function at that point is "near zero" 
        !           according to land_inf in modelparams.f90) or it is expected that 
        !           the ice will contact the land within the next time step.
        ! 
        !       -   in some instances the *lmsk and *ismk will both be activated
        !           (set to 0) but the value of *lmsk will override the *imsk value 
        !           (likewise for *lmsk and *imsk).
        !
        !       -   As of April 11th, 2017, I added 12 more masks to u and v points
        !           which are required to properly masks the following derivatives at U points:
        !               - d2u/dxdy, d2v/dx2, d2v/dy2
        !           and the following derivatives at V points:
        !               - d2v/dxdy, d2u/dx2, d2u/dy2.
        !           Note that the first 8 masks (i.e. uimsk(i,j,1:8)) are used to mask
        !           the direct neighbours; these are sufficient for most calculations
        !           but the last 12 (i.e. uimsk(i,j,9:20)) are used to mask the outer 
        !           points. 
        !======================================================================
        
        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxN,    nyN,        &   ! limit of indices for node points
            nxT,    nyT,        &   ! limit of indices for tracer points
            nxU,    nyU,        &   ! limit of indices for U points
            nxV,    nyV,        &   ! limit of indices for V points
            nU_pnts,            &   ! number of U points in computational domain
            nV_pnts                 ! number of V points in computational domain 
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj          ! grid locations of u points in comp. domain 
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj          ! grid locations of v points in comp. domain 
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_T,             &   ! distance function solution 
            dist_gx_T, dist_gy_T    ! gradient components of the distance function
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            dist_N                  ! distance function solution at N points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                   ! u velocities
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                   ! v velocities

        !==================!    
        !------In/out------!
        !==================!
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(inout) :: &
            ulmsk, uimsk            ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(inout) :: &
            vlmsk, vimsk            ! computational masks for V points

        !==================!    
        ! -- LOCAL VARS ---!
        !==================!
        integer(kind = int_kind) :: &
            i,j, ij         ! local indices
 
        !==========================!    
        !  Masking Around U-points !
        !==========================!
            ulmsk = 1
            uimsk = 1
            
            do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)
                
                !===========================!
                !  Mask direct U neighbours !
                !===========================!
                    !---enforce B.C. at (i,j-1) u-component---!
                        ! check for land connection at node (i,j) and open water at u(i,j-1) !
                        if (lmsk(i-1,j-1) .or. lmsk(i,j-1)) then 
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        ulmsk(i,j,1) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i, j-1, nxT, nyT, nxN, nyN))        uimsk(i,j,1) = 0
                    
                    !---enforce B.C. at (i-1,j) u-component---!
                        if (lmsk(i-2,j)) then
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i-1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,2) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i-1,j, nxT, nyT, nxN, nyN))         uimsk(i,j,2) = 0
                    
                    !---enforce B.C. at (i+1,j) u-component---!
                        if (lmsk(i+1,j)) then
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,3) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i+1, j, nxT, nyT, nxN, nyN))        uimsk(i,j,3) = 0
                    
                    !---enforce B.C. at (i,j+1) u-component---!
                        ! check for land connection at node (i,j+1) and open water at u(i,j+1)
                        if (lmsk(i-1,j+1) .or. lmsk(i,j+1)) then 
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,4) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i, j+1, nxT, nyT, nxN, nyN))        uimsk(i,j,4) = 0

                !===========================!   
                !  Mask direct V neighbours !
                !===========================!
                    !---enforce B.C. at (i-1,j) v-component---!
                        if (lmsk(i-1,j-1)) then
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i-1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,5) = 0
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i-1, j, nxT, nyT, nxN, nyN))        uimsk(i,j,5) = 0
                    
                    !---enforce B.C. at (i,j) v-component----!
                        if (lmsk(i,j-1)) then
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        ulmsk(i,j,6) = 0    
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i, j, nxT, nyT, nxN, nyN))          uimsk(i,j,6) = 0
                    
                    !---enforce B.C. at (i-1,j+1) v-component--!
                        if (lmsk(i-1,j+1)) then
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i-1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    ulmsk(i,j,7) = 0             
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i-1, j+1, nxT, nyT, nxN, nyN))      uimsk(i,j,7) = 0
                    
                    !---enforce B.C. at (i,j+1) v-component---!
                        if (lmsk(i,j+1)) then
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,8) = 0             
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i, j+1, nxT, nyT, nxN, nyN))        uimsk(i,j,8) = 0

                !=====================================================!
                ! Mask U-components needed for mixed derivatives in U !
                !=====================================================!
                    !---enforce B.C. at (i-1,j-1) u-component---!
                        if (lmsk(i-1,j-1) .or. lmsk(i-2,j-1)) then
                            if (lmsk(i-1,j-1)) then 
                                ! check for land conneciton at N(i-1,j) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j, nxT, nyT, nxN ,nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,9) = 0
                            else
                                ! check for land connection at u(i-1,j-1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    ulmsk(i,j,9) = 0
                            endif 
                        endif 

                        if (.not. inIce('upnt', dist_T, dist_N, i-1, j-1, nxT, nyT, nxN, nyN))          uimsk(i,j,9) = 0
                    
                    !---enforce B.C. at (i+1, j-1) u-component---!
                        if (lmsk(i,j-1) .or. lmsk(i+1,j-1)) then 
                            if (lmsk(i,j-1)) then 
                                ! check for land connection at N(i+1,j) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,10) = 0
                            else
                                ! check for land connection at u(i+1,j-1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j-1, nxT, nyT, nxN, nyN, nxU ,nyU, nxV, nyV))    ulmsk(i,j,10) = 0
                            endif 
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i+1, j-1, nxT, nyT, nxN, nyN))          uimsk(i,j,10) = 0

                    !---enforce B.C. at (i-1, j+1) u-component---!
                        if (lmsk(i-2,j+1) .or. lmsk(i-1,j+1)) then 
                            if (lmsk(i-1,j+1)) then 
                                ! check for land connection at N(i-1,j+1) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    ulmsk(i,j,11) = 0
                            else
                                ! check for land connection at u(i-1,j+1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j+1, nxT, nyT, nxN, nyN, nxU ,nyU, nxV, nyV))    ulmsk(i,j,11) = 0
                            endif 
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i-1, j+1, nxT, nyT, nxN, nyN))          uimsk(i,j,11) = 0

                    !---enforce B.C. at (i+1, j+1) u-component---!
                        if (lmsk(i,j+1) .or. lmsk(i+1,j+1)) then 
                            if (lmsk(i,j+1)) then 
                                ! check for land connection at N(i+1,j+1) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    ulmsk(i,j,12) = 0
                            else
                                ! check for land connection at u(i+1,j+1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j+1, nxT, nyT, nxN, nyN, nxU ,nyU, nxV, nyV))    ulmsk(i,j,12) = 0
                            endif 
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i+1, j+1, nxT, nyT, nxN, nyN))          uimsk(i,j,12) = 0

                !===============================================================!
                ! Mask V components necessary for un-mixed 2nd derivatives in V !
                !===============================================================!
                    ! v(i-1,j-1) !
                        if (lmsk(i-1,j-1) .or. lmsk(i-1,j-2)) then
                            if (lmsk(i-1,j-1)) then 
                                ! check for land connection at v(i-1,j) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,13) = 0
                            else
                                ! check for land conneciton at v(i-1,j-1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    ulmsk(i,j,13) = 0
                            endif 
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i-1, j-1, nxT, nyT, nxN, nyN))          uimsk(i,j,13) = 0

                    ! v(i,j-1) !
                        if (lmsk(i,j-1) .or. lmsk(i,j-2)) then
                            if (lmsk(i,j-1)) then 
                                ! check for land connection at v(i,j) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        ulmsk(i,j,14) = 0
                            else
                                ! check for land conneciton at v(i-1,j-1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,14) = 0
                            endif 
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i, j-1, nxT, nyT, nxN, nyN))            uimsk(i,j,14) = 0

                    ! v(i-2,j) !
                        if (lmsk(i-2,j)) then
                            ! check for land connection at N(i-1,j) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, & 
                                ugrid, vgrid, i-1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))          ulmsk(i,j,15) = 0
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i-2, j, nxT, nyT, nxN, nyN))            uimsk(i,j,15) = 0

                    ! v(i+1,j) !
                        if (lmsk(i+1,j)) then
                            ! check for land connection at N(i+1,j) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, & 
                                ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))          ulmsk(i,j,16) = 0
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i+1, j, nxT, nyT, nxN, nyN))            uimsk(i,j,16) = 0

                    ! v(i-2,j+1) !
                        if (lmsk(i-2,j)) then
                            ! check for land connection at N(i-1,j+1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, & 
                                ugrid, vgrid, i-1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        ulmsk(i,j,17) = 0
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i-2, j+1, nxT, nyT, nxN, nyN))          uimsk(i,j,17) = 0

                    ! v(i+1,j+1) !
                        if (lmsk(i+1,j)) then
                            ! check for land connection at N(i+1,j+1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, & 
                                ugrid, vgrid, i+1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        ulmsk(i,j,18) = 0
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i+1, j+1, nxT, nyT, nxN, nyN))          uimsk(i,j,18) = 0

                    ! v(i-1,j+2) !
                        if (lmsk(i-1,j+1) .or. lmsk(i-1,j+2)) then 
                            if (lmsk(i-1,j+1)) then
                                ! check for land connection at v(i-1,j+1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    ulmsk(i,j,19) = 0
                            else
                                ! check for land connection at v(i-1,j+2) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j+2, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    ulmsk(i,j,19) = 0
                            endif
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i-1, j+2, nxT, nyT, nxN, nyN))          uimsk(i,j,19) = 0
                    ! v(i,j+2) !
                        if (lmsk(i,j+1) .or. lmsk(i,j+2)) then 
                            if (lmsk(i,j+1)) then
                                ! check for land connection at v(i,j+1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,20) = 0
                            else
                                ! check for land connection at v(i,j+2) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j+2, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      ulmsk(i,j,20) = 0
                            endif
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i, j+2, nxT, nyT, nxN, nyN))            uimsk(i,j,20) = 0
            end do
        
        !=========================!
        ! Masking Around V points !
        !=========================!
            vlmsk = 1
            vimsk = 1
        
            do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                !===========================!
                !  Mask direct U neighbours !
                !===========================!
                    !--enforce B.C. at (i,j-1) u-component--!
                        if (lmsk(i-1,j-1)) then 
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,1) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i, j-1, nxT, nyT, nxN, nyN))        vimsk(i,j,1) = 0

                    !--enforce B.C. at (i+1,j-1) u-component--!
                        if (lmsk(i+1,j-1)) then 
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,2) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i+1, j-1, nxT, nyT, nxN, nyN))      vimsk(i,j,2) = 0

                    !--enforce B.C. at (i,j) u-component--!
                        if (lmsk(i-1,j)) then 
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        vlmsk(i,j,3) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i, j, nxT, nyT, nxN, nyN))          vimsk(i,j,3) = 0

                    !--enforce B.C. at (i+1,j) u-component--!
                        if (lmsk(i+1,j)) then 
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,4) = 0
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i+1, j, nxT, nyT, nxN, nyN))        vimsk(i,j,4) = 0
                
                !============================!
                !  Mask direct V neighbours  !
                !============================!
                    !  enforce B.C. at (i,j-1) v-component  !
                        if (lmsk(i,j-2)) then 
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,5) = 0
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i, j-1, nxT, nyT, nxN, nyN))        vimsk(i,j,5) = 0
                    
                    !  enforce B.C. at (i-1,j) v-component  !
                        ! check for land connection at node (i,j) and open water at v(i-1,j)
                        if (lmsk(i-1,j) .or. lmsk(i-1,j-1)) then 
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        vlmsk(i,j,6) = 0
                            endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i-1, j, nxT, nyT, nxN, nyN))        vimsk(i,j,6) = 0
                            
                    !  enforce B.C. at (i+1,j) v-component  !
                        ! check for land connection at node (i+1,j) and open water at v(i+1,j)
                        if (lmsk(i+1,j) .or. lmsk(i+1,j-1)) then 
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,7) = 0
                            endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i+1, j, nxT, nyT, nxN, nyN))        vimsk(i,j,7) = 0
                
                    !  enforce B.C. at (i,j+1) v-component  !
                        if (lmsk(i,j+1)) then 
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,8) = 0
                        endif

                        if (.not. inIce('vpnt', dist_T, dist_N, i, j+1, nxT, nyT, nxN, nyN))        vimsk(i,j,8) = 0

                !=====================================================!
                ! Mask V-components needed for mixed derivatives in V !
                !=====================================================!
                    ! enforce BC at v(i-1,j-1) !
                        if (lmsk(i-1,j-2) .or. lmsk(i-1,j-1)) then
                            if (lmsk(i-1,j-1)) then 
                                ! check for land connection at N(i,j-1) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,9) = 0
                            else
                                ! check for land connection at v(i-1,j-1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,9) = 0
                            endif 
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i-1, j-1, nxT, nyT, nxN, nyN))          vimsk(i,j,9) = 0

                    ! enforce BC at v(i+1,j-1) !
                        if (lmsk(i+1,j-2) .or. lmsk(i+1,j-1)) then
                            if (lmsk(i+1,j-1)) then 
                                ! check for land connection at N(i+1,j-1) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,10) = 0
                            else
                                ! check for land connection at v(i+1,j-1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,10) = 0
                            endif 
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i+1, j-1, nxT, nyT, nxN, nyN))          vimsk(i,j,10) = 0

                    ! enforce BC at v(i-1,j+1) !
                        if (lmsk(i-1,j) .or. lmsk(i-1,j+1)) then
                            if (lmsk(i-1,j)) then 
                                ! check for land connection at N(i,j+1) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,11) = 0
                            else
                                ! check for land connection at v(i-1,j+1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,11) = 0
                            endif 
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i-1, j+1, nxT, nyT, nxN, nyN))          vimsk(i,j,11) = 0

                    ! enforce BC at v(i+1,j+1) !
                        if (lmsk(i+1,j) .or. lmsk(i+1,j+1)) then
                            if (lmsk(i+1,j)) then 
                                ! check for land connection at N(i+1,j+1) !
                                if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,12) = 0
                            else
                                ! check for land connection at v(i+1,j+1) !
                                if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,12) = 0
                            endif 
                        endif 

                        if (.not. inIce('vpnt', dist_T, dist_N, i+1, j+1, nxT, nyT, nxN, nyN))          vimsk(i,j,12) = 0

                !===============================================================!
                ! Mask U components necessary for un-mixed 2nd derivatives in U !
                !===============================================================!
                    ! u(i,j-2) !
                        if (lmsk(i,j-2)) then 
                            ! check for land connection at N(i,j-1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))          vlmsk(i,j,13) = 0
                        endif 

                        if (.not. inIce('upnt', dist_T, dist_N, i, j-2, nxT, nyT, nxN, nyN))            vimsk(i,j,13) = 0

                    ! u(i+1,j-2) !
                        if (lmsk(i,j-2)) then 
                            ! check for land connection at N(i+1,j-1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        vlmsk(i,j,14) = 0
                        endif 

                        if (.not. inIce('upnt', dist_T, dist_N, i+1, j-2, nxT, nyT, nxN, nyN))          vimsk(i,j,14) = 0

                    ! u(i-1,j-1) !
                        if (lmsk(i-1,j-1) .or. lmsk(i-2,j-1)) then
                            if (lmsk(i-1,j-1)) then 
                                ! check for land connection at u(i,j-1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,15) = 0
                            else
                                ! check for land connection at u(i-1,j-1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,15) = 0
                            endif
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i-1, j-1, nxT, nyT, nxN, nyN))          vimsk(i,j,15) = 0

                    ! u(i+2,j-1) !
                        if (lmsk(i+1,j-1) .or. lmsk(i+2,j-1)) then
                            if (lmsk(i+1,j-1)) then 
                                ! check for land connection at u(i+1,j-1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,16) = 0
                            else
                                ! check for land connection at u(i+2,j-1) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+2, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))    vlmsk(i,j,16) = 0
                            endif
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i+2, j-1, nxT, nyT, nxN, nyN))          vimsk(i,j,16) = 0

                    ! u(i-1,j) !
                        if (lmsk(i-1,j) .or. lmsk(i-2,j)) then
                            if (lmsk(i-1,j)) then 
                                ! check for land connection at u(i,j) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        vlmsk(i,j,17) = 0
                            else
                                ! check for land connection at u(i-1,j) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i-1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,17) = 0
                            endif
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i-1, j, nxT, nyT, nxN, nyN))            vimsk(i,j,17) = 0

                    ! u(i+2,j) !
                        if (lmsk(i+1,j) .or. lmsk(i+2,j)) then
                            if (lmsk(i+1,j)) then 
                                ! check for land connection at u(i+1,j) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,18) = 0
                            else
                                ! check for land connection at u(i+2,j) !
                                if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                    ugrid, vgrid, i+2, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))      vlmsk(i,j,18) = 0
                            endif
                        endif

                        if (.not. inIce('upnt', dist_T, dist_N, i+2, j, nxT, nyT, nxN, nyN))            vimsk(i,j,18) = 0

                    ! u(i,j+1) !
                        if (lmsk(i,j+1)) then 
                            ! check for land connection at N(i,j+1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))          vlmsk(i,j,19) = 0
                        endif 

                        if (.not. inIce('upnt', dist_T, dist_N, i, j+1, nxT, nyT, nxN, nyN))            vimsk(i,j,19) = 0

                    ! u(i+1,j+1) !
                        if (lmsk(i,j+1)) then 
                            ! check for land connection at N(i+1,j+1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV))        vlmsk(i,j,20) = 0
                        endif 

                        if (.not. inIce('upnt', dist_T, dist_N, i+1, j+1, nxT, nyT, nxN, nyN))          vimsk(i,j,20) = 0
            end do
    end subroutine create_comp_msk_dist

!===========================================================================================!
!                                 correct_boundary                                          !
!===========================================================================================!
    subroutine correct_boundary(time, h_T, A_T, dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN)
        !=======================================================================================
        ! At twice the frequency of correct_dist, this routine checks if there are any points inside 
        ! the zero level set of the distance function with A or h = 0. If there is, it sets 
        ! both h and A to zero and sets the value of dist_T to -dx/2 and then calls 
        ! correct_dist in order to correct the zero level set and properly set the boundary.
        !
        ! Note this routine doesn't perform this check on land cells, as h and A are forced to be 
        ! zero in these cells, so if the zero level set encroaches on a land cell due to numerical 
        ! error, a distance function correction isn't forced.
        !=======================================================================================

        !==================!
        ! ------IN ------- !
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxT, nyT, nxN, nyN      ! index limits for the T and N grid 
        real(kind = dbl_kind), intent (in) :: &
            time

        !==================!
        ! ---- IN/OUT ---- !
        !==================!
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (inout) :: &
            dist_T, dist_gx_T, dist_gy_T,   &   ! distance function and its derivatives - tracer points 
            A_T, h_T                            ! area fraction and thickness 
        real(kind = dbl_kind), dimension (nxN, nyN), intent (inout) :: &
            dist_N 

        !==================!
        ! -- LOCAL VARS ---!
        !==================!
        integer(kind = int_kind) :: &
            i, j
        real(kind = dbl_kind) :: &
            flag, domainlim 
        logical :: &
            correct             ! flag used to determine if correct_dist is called or not 

            flag = 1.d0
            domainlim = -dx/2   ! defines the smallest value of the distance function, at T points, where we know an ice boundary is in the cell 

        !====================!
        ! Initialize correct !
        !====================!
            correct = .False.

        !===========================================================!
        ! Loop through T grid and determine if correction is needed !
        !===========================================================!
            do i = 1-gc_T,nxT+gc_T
                do j = 1-gc_T,nyT+gc_T

                    ! Check for land cells and ignore if cell in question is a land cell !
                    if (.not. lmsk(i,j)) then 
                        if (dist_T(i,j) > domainlim) then 
                            ! thickness and area fraction should be non-zero !

                            flag = A_T(i,j)*h_T(i,j)

                            if (flag == 0.d0) then 
                                ! ice boundary should be moved !

                                    correct     = .true.

                                    dist_T(i,j) = domainlim 
                                    A_T(i,j)    = 0.d0 
                                    h_T(i,j)    = 0.d0 
                            end if 
                        end if 
                    else 
                        ! do nothing !
                    end if 
                end do 
            end do 

        !========================================!
        ! Correct distance function if necessary !
        !========================================!
            if (correct) then 
                print *, ""
                print *, "Additional correction of distance function required."
                print *, ""
                call correct_dist(time, dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN)
            end if 

    end subroutine correct_boundary

!===========================================================================================!
!                                   correct_dist                                            !
!===========================================================================================!
    subroutine correct_dist(time, dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN)

        !=======================================================================================
        ! During the advection step, this routine is used to correct the distance function 
        ! values after it has been advected. This is done as the Advection routine should 
        ! advect the zero level set accurately, it will distort it else where.
        !
        ! This routine is very similar to the init_dist_fnctn_stp_crit() routine in the
        ! initialization module; please refer to it for more details. This routine doesn't do the 
        ! same initialization step and lets the stopping criteria be tested after each iteration.
        ! It should converge in very little iterations.
        !
        ! Prior to updating the distance function, this routine first goes through a 
        ! re-initialization phase where it checks if h or A is zero where phi > 0; this shouldn't
        ! happen. If either h or A is zero, it sets the other to be zero also, and flips the sign 
        ! of the distance function to be negative.
        !=======================================================================================

        !==================!
        ! ------IN ------- !
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxT, nyT, nxN, nyN              ! indices limits for T points and N points
        real(kind = dbl_kind), intent(in) :: &
            time                            ! current time level

        !==================!
        ! ---- IN/OUT ---- !
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (inout) :: &
            dist_T, dist_gx_T,  dist_gy_T   ! distance function at T points and gradients
        real(kind = dbl_kind), dimension(nxN, nyN), intent (inout) :: &
            dist_N

        !==================!
        ! -- LOCAL VARS ---!
        !==================!
        integer(kind = int_kind) :: &
            i, j, iter_count,               &   ! local indices and counters
            M                                   ! number of points used in stopping criteria 

        integer(kind = int_kind), parameter :: &
            max_iter    = 300000                 ! max iteration

        real(kind = dbl_kind), parameter :: &
            eps_dist    = dx,               &   ! used to define a smooth sign function
            dt_dist     = dx/10_dbl_kind,   &   ! "time step" used. This is purely numerical. See Sussman 1994
            beta        = 10                    ! used in stopping criteria  

        real(kind = dbl_kind) :: &
            stop_crit, sum,                 &   ! used in determining stopping criteria
            a, b, c, d,                     &   ! used to calc derivatives
            a_neg, b_neg, c_neg, d_neg,     &   ! used in distance value updates
            a_pos, b_pos, c_pos, d_pos                               
                  
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) ::  &
            dist_T_prev,                    &   ! stores previous iterations values
            sign_T                              ! smooth sign function

        real(kind = dbl_kind), dimension(1:max_iter) :: &
            iter_update                         ! stores the new sum/M value at each iteration for convergence study


        ! -----Determine Stopping Criteria and Initialize Values ----- !

            M           = 0
            sum         = 0
            iter_count  = 1

            sign_T      = 0.d0
            dist_T_prev = 0.d0

            stop_crit = beta*dt_dist*dx*dx/(x_extent**2)

        ! -----Initialize Sign Function----- !

            do i = 1-gc_T, nxT+gc_T
                do j = 1-gc_T, nyT+gc_T
                        sign_T(i, j) = dist_T(i, j)/sqrt(dist_T(i, j)**2 + eps_dist**2) 
                enddo
            enddo

        ! ---------------------- Solve Distance Function at T points ---------------------------- !
            do 
                ! store previous iteration
                dist_T_prev = dist_T

                ! update values everywhere
                do i = 1-gc_T, nxT+gc_T
                    do j = 1-gc_T, nyT+gc_T

                    !------------ Using scheme in Sussman 1994 ------------------------!
                    ! NOTE: the if statements check for boundary conditions at the edge 
                    ! domain (including ghost cells)
                    !------------------------------------------------------------------!
                        !------------ Calc X derivatives -------------------!
                        if (i == 1-gc_T) then
                            a = (dist_T_prev(i+1, j) - dist_T_prev(i, j))/dx
                        else
                            a = (dist_T_prev(i, j) - dist_T_prev(i-1, j))/dx
                        endif

                        if (i == nxT+gc_T) then 
                            b = (dist_T_prev(i, j) - dist_T_prev(i-1, j))/dx
                        else
                            b = (dist_T_prev(i+1, j) - dist_T_prev(i, j))/dx
                        endif   

                        !------------ Calc Y derivatives -------------------!
                        if (j == 1-gc_T) then 
                            c = (dist_T_prev(i, j+1) - dist_T_prev(i, j))/dx
                        else
                            c = (dist_T_prev(i, j) - dist_T_prev(i, j-1))/dx
                        endif

                        if (j == nyT+gc_T) then
                            d = (dist_T_prev(i, j) - dist_T_prev(i, j-1))/dx
                        else
                            d = (dist_T_prev(i, j+1) - dist_T_prev(i, j))/dx
                        endif

                        a_pos = max(0.0_dbl_kind,a)
                        b_pos = max(0.0_dbl_kind,b)
                        c_pos = max(0.0_dbl_kind,c)
                        d_pos = max(0.0_dbl_kind,d)

                        a_neg = min(0.0_dbl_kind,a)
                        b_neg = min(0.0_dbl_kind,b)
                        c_neg = min(0.0_dbl_kind,c)
                        d_neg = min(0.0_dbl_kind,d)

                        if (dist_T(i,j) > 0) then 
                            dist_T(i,j) = dist_T_prev(i,j) + dt_dist*sign_T(i,j)*(1 - sqrt(max((a_pos**2), (b_neg**2)) + max((c_pos**2), (d_neg**2))))
                        elseif (dist_T(i,j) < 0) then
                            dist_T(i,j) = dist_T_prev(i,j) + dt_dist*sign_T(i,j)*(1 - sqrt(max((a_neg**2), (b_pos**2)) + max((c_neg**2), (d_pos**2))))
                        else
                            dist_T(i,j) = dist_T_prev(i,j)
                        endif
                            
                    enddo
                enddo

                !--------------------Store new gradient and calculate stopping criteria ------------------------!
                do i = 1-gc_T, nxT+gc_T
                    do j = 1-gc_T, nyT+gc_T
                        !------------ Calc X derivatives -------------------!
                        if (i == 1-gc_T) then
                            a = (dist_T(i+1, j) - dist_T(i, j))/dx
                        else
                            a = (dist_T(i, j) - dist_T(i-1, j))/dx
                        endif

                        if (i == nxT+gc_T) then 
                            b = (dist_T(i, j) - dist_T(i-1, j))/dx
                        else
                            b = (dist_T(i+1, j) - dist_T(i, j))/dx
                        endif   

                        !------------ Calc Y derivatives -------------------!
                        if (j == 1-gc_T) then 
                            c = (dist_T(i, j+1) - dist_T(i, j))/dx
                        else
                            c = (dist_T(i, j) - dist_T(i, j-1))/dx
                        endif

                        if (j == nyT+gc_T) then
                            d = (dist_T(i, j) - dist_T(i, j-1))/dx
                        else
                            d = (dist_T(i, j+1) - dist_T(i, j))/dx
                        endif

                        dist_gx_T(i,j) = (a+b)/2
                        dist_gy_T(i,j) = (c+d)/2

                        !---------------- Update Sum at points where |phi| is less than 5dx -------------------------!
                        if (abs(dist_T(i,j)) < 5*dx) then
                            ! don't include point in stopping criteria test !
                        else 
                            sum = sum + abs(dist_T(i,j) - dist_T_prev(i,j))
                            M = M + 1
                        endif

                    enddo
                enddo

                iter_update(iter_count) = sum/M

                !----------------- Check Stopping Criteria -------------------!
                if (sum/M < stop_crit) then
                    ! stopping criteria met, terminate outer do loop !
                    print *, 'Distance function corrected in ', iter_count, 'iterations'

                    ! if (mod(time, sv_interval) == 0) then 
                    !     if (time == sv_interval) then 
                    !         open(unit = 9999, file = './output/Phi_Crrctn_itercount.dat', status = 'replace')
                    !         call csv_write_integer(9999, iter_count, .true.)
                    !     elseif (time == T) then 
                    !         open(unit = 9999, file = './output/Phi_Crrctn_itercount.dat', status = 'old')
                    !         call csv_write_integer(9999, iter_count, .true.)
                    !         close(9999)
                    !     else
                    !         open(unit = 1, file = './output/Phi_Crrctn_itercount.dat', status = 'old')
                    !         call csv_write_integer(9999, iter_count, .true.)
                    !     end if 
                    ! end if 

                    EXIT

                elseif (iter_count == distcrrctn_lim) then
                    ! max iteractions met, terminate outer do loop !
                    print *, 'Distance function corrected using the correction limit of ', distcrrctn_lim

                    ! if (mod(time, sv_interval) == 0) then 
                    !     if (time == sv_interval) then 
                    !         open(unit = 9999, file = './output/Phi_Crrctn_itercount.dat', status = 'replace')
                    !         call csv_write_integer(9999, iter_count, .true.)
                    !     elseif (time == T) then 
                    !         open(unit = 9999, file = './output/Phi_Crrctn_itercount.dat', status = 'old')
                    !         call csv_write_integer(9999, iter_count, .true.)
                    !         close(9999)
                    !     else
                    !         open(unit = 1, file = './output/Phi_Crrctn_itercount.dat', status = 'old')
                    !         call csv_write_integer(9999, iter_count, .true.)
                    !     end if 
                    ! end if 

                    EXIT

                else
                    ! continue
                endif

                ! reset sum and M and update iter_cout !
                iter_count = iter_count + 1
                sum = 0
                M = 0
            enddo
                
            ! Set distance values at node points !
            do i = 1, nxN
                do j = 1, nyN
                    dist_N(i,j) = (dist_T(i,j) + dist_T(i,j-1) + dist_T(i-1,j-1) + dist_T(i-1,j))/4
                end do 
            end do

    end subroutine correct_dist

!===========================================================================================!
!                               Mask Previous TimeStep                                      !
!===========================================================================================!
    subroutine MskPrevTstp(ugrid, vgrid, prevUDom, prevVDom, dist_gx_T_prev, dist_gy_T_prev, &
                        indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT)
        !===================================================================================
        ! This routine checks the new computational domain to see if there are points now
        ! included that weren't in the previous time-step. If new points are considered, 
        ! this rouine sets the velocities using Neumann conditions from neighbouring points
        ! that were previously in ice.
        !
        ! This routine also sets the velocities outside new comp domain to zero.
        !===================================================================================

        !===================!
        !       In          !
        !===================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV, nxT, nyT,   &   ! index limits for U, V, and T points 
            nU_pnts, nV_pnts                    ! number of U and V points in the new computational domain 
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                      ! locations of U points in comp domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                      ! locations of V points in comp domain 

        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_gx_T_prev, dist_gy_T_prev      ! gradients of previous domain's distance function 

        logical, dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            prevUDom                            ! previous U comp domain
        logical, dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            prevVDom                            ! previous V comp domain

        !===========!
        !   Inout   !
        !===========!
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(inout) :: &
            ugrid                               ! u grid velocities
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(inout) :: &
            vgrid                               ! v grid velocities

        !===========!
        !   Local   !
        !===========!
        integer(kind = int_kind) :: &
            i, j, ij,   &
            xdir, ydir          ! directions of gradients ! 
        real(kind = dbl_kind) :: &
            dist_gx_tmp,    &   ! x-component of distance fnction grad 
            dist_gy_tmp         ! y-component of distance fnction grad
        logical, dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
            newUDom             ! new U comp domain 
        logical, dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
            newVDom             ! new V comp domain 

            newUDom = .False.
            newVDom = .False.

        !===================================!
        !   Compare old/new comp domain     !
        !===================================!
            ! U-points !
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                newUDom(i,j) = .True.

                if (prevUDom(i,j)) then 
                    ! no special consideration is needed !
                else 
                    ! domain has changed -> mask velocity !

                    ! interpolate gradients !
                    dist_gx_tmp = (dist_gx_T_prev(i-1,j) + dist_gx_T_prev(i,j))/2
                    dist_gy_tmp = (dist_gy_T_prev(i-1,j) + dist_gy_T_prev(i,j))/2

                    ! determine direction of increasing distance fnctn !
                    xdir = int(sign(1.0, dist_gx_tmp))
                    ydir = int(sign(1.0, dist_gy_tmp))

                    ! check for dominant direction !
                    if (abs(dist_gx_tmp) > abs(dist_gy_tmp)) then 
                        ! look in x-direction !
                        ugrid(i,j) = ugrid(i+xdir,j)

                        ! print warning if assumption is wrong !
                        if (.not. prevUDom(i+xdir,j)) then 
                            print *, ""
                            print *, "MskPrevTstp Warning 1: see routine in domain_routines"
                            print *, "When masking U point", i, j
                            print *, "the routine tried to use U point", i+xdir, j
                            print *, "but the point wasn't in the previous domain."
                            print *, ""
                        end if 
                    else
                        ! look in the y-direction !
                        ugrid(i,j) = ugrid(i,j+ydir)

                        ! print warning if assumption is wrong !
                        if (.not. prevUDom(i,j+ydir)) then 
                            print *, ""
                            print *, "MskPrevTstp Warning 2: see routine in domain_routines"
                            print *, "When masking U point", i, j
                            print *, "the routine tried to use U point", i, j+ydir
                            print *, "but the point wasn't in the previous domain."
                            print *, ""
                        end if
                    end if 
                end if 
            end do 

            ! V-points !
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                newVDom(i,j) = .True.

                if (prevVDom(i,j)) then 
                    ! no special consideration is needed !
                else
                    ! domain has changed -> mask velocity !

                    ! interpolate gradients !
                    dist_gx_tmp = (dist_gx_T_prev(i,j-1) + dist_gx_T_prev(i,j))/2
                    dist_gy_tmp = (dist_gy_T_prev(i,j-1) + dist_gy_T_prev(i,j))/2

                    ! determine direction of increasing distance fnctn !
                    xdir = int(sign(1.0, dist_gx_tmp))
                    ydir = int(sign(1.0, dist_gy_tmp))

                    ! check for dominant direction !
                    if (abs(dist_gx_tmp) > abs(dist_gy_tmp)) then 
                        ! look in the x-direction !
                        vgrid(i,j) = vgrid(i+xdir,j)

                        ! print warning if assumption is wrong !
                        if (.not. prevVDom(i+xdir,j)) then 
                            print *, ""
                            print *, "MskPrevTstp Warning 3: see routine in domain_routines"
                            print *, "When masking V point", i, j
                            print *, "the routine tried to use V point", i+xdir, j
                            print *, "but the point wasn't in the previous domain."
                            print *, ""
                        end if 
                    else 
                        ! look in the y-direction !
                        vgrid(i,j) = vgrid(i,j+ydir)

                        ! print warning if assumption is wrong !
                        if (.not. prevVDom(i,j+ydir)) then 
                            print *, ""
                            print *, "MskPrevTstp Warning 4: see routine in domain_routines"
                            print *, "When masking V point", i, j
                            print *, "the routine tried to use V point", i, j+ydir
                            print *, "but the point wasn't in the previous domain."
                            print *, ""
                        end if
                    end if 
                end if 
            end do   

        !===============================================================!
        !  Set velocities to 0 outside the new computational domain     !
        !===============================================================!
            ! U-points !
            do i = 1 - gc_U, nxU + gc_U
                do j = 1 - gc_U, nyU + gc_U
                    if (.not. newUDom(i,j)) then 
                        ugrid(i,j) = 0.d0
                    else
                        ! do nothing !
                    end if 
                end do 
            end do 

            ! V-points !
            do i = 1 - gc_V, nxV + gc_V
                do j = 1 - gc_V, nyV + gc_V
                    if (.not. newVDom(i,j)) then 
                        vgrid(i,j) = 0.d0
                    else
                        ! do nothing !
                    end if 
                end do 
            end do
    end subroutine MskPrevTstp

!===========================================================================================!
!                             Store UV Computational Domain                                 !
!===========================================================================================!
    subroutine StrUVCmpDomain(prevUDom, prevVDom, indxUi, indxUj, indxVi, indxVj, &
                                nU_pnts, nV_pnts, nxU, nyU, nxV, nyV)
        !===================================================================================
        ! This routine flags what U and V points that were in the computational domain for the 
        ! previous timestep. This is needed as we have a moving domain, and we will need to 
        ! mask velocities from the previous time-step if they weren't in the computational 
        ! domain.
        !===================================================================================

        !==================!
        ! ------IN ------- !
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &   ! index limits for U and V points
            nU_pnts, nV_pnts            ! number of U and V points in the computational domain 
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj              ! locations of U points 
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj              ! locations of V points 

        !==================!
        ! ---- IN/OUT ---- !
        !==================!
        logical, dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(inout) :: &
            prevUDom                    ! ugrid computational domain
        logical, dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(inout) :: &
            prevVDom                    ! vgrid computational domain 

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij 

            !===================================!
            !   Initialize logicals to false    !
            !===================================!
                prevUDom = .False.
                prevVDom = .False. 

            !===================!
            !   Store U Domain  !
            !===================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    prevUDom(i,j) = .True. 
                end do 

            !===================!
            !   Store V Domain  !
            !===================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    prevVDom(i,j) = .True.
                end do 
    end subroutine StrUVCmpDomain

!===========================================================================================!
!                         Use Land Boundary Condition ?                                     !
!===========================================================================================!
    logical function uselandBC(point_type, dist_T, dist_N, dist_gx_T, dist_gy_T, ugrid, vgrid, &
                                i_ind, j_ind, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)

        !====================================================================================
        ! This function is used to determine if land boundary conditions should be used or 
        ! not. It returns a logical value of .True. or .False.
        !
        ! A Land BC is used if:
        !   
        !   -   Ice is actually attached to land; this is determined through the value of the
        !       distance function at the point in question. We define an area of influence,
        !       where the ice is close enough to the land, where for practical purposes the 
        !       ice can be considered attached.
        !
        !   -   The ice is moving towards the land and it is expected that the ice will hit 
        !       it in the interval of [t, t + dt]. Of course, we can't determine if it will
        !       without a doubt, but we can get a good idea if it will.
        !
        ! NOTE:
        !
        !   -   At some points, we need to consider node points instead of just u and v points;
        !       this is because land cells take up entire cells and for certain configurations
        !       the actual resulting land boundary will be at the node point, and not a u or 
        !       v point.
        !
        !   -   As the distance function value is located on T-points and N-points, linear interpolations
        !       of the T-point values are used to get values at u and v points; this value is then multiplied by negative
        !       one so we have a positive measurement of the distance to the ice front.
        ! 
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        character(len = 4), intent(in) :: &
            point_type                          ! defines what type of point is being considered: u, v, or node point
        integer(kind = int_kind), intent(in) :: &
            nxT, nyT, nxN, nyN, &               ! needed for array limits
            nxU, nyU, nxV, nyV, &               ! needed for array limits
            i_ind, j_ind                        ! indices for point being considered
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_T, dist_gx_T, dist_gy_T        ! distance function solution and its gradients
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            dist_N
        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            ugrid                               ! u velocities
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            vgrid                               ! v velocities

        !==================!
        !----Local Vars----!
        !==================!
        real(kind = dbl_kind) :: &
            dist, dist_x, dist_y,           &   ! distance to ice front: magnitude and in component form 
            dist_grad, dist_gx, dist_gy,    &   ! gradient: magnitude and component form 
            x_point, y_point,               &   ! grid location of potential land boundary
            x_front, y_front,               &   ! grid locations of the ice front
            u_front, v_front,               &   ! veloctiy of ice front
            vel_perp                            ! scalar velocity 
        real(kind = dbl_kind), dimension (4) :: &
            proximity                           ! local array that holds the proximity of neighbouring u and v points to the ice front point
        integer(kind = int_kind) :: &
            i, j, k, l, ii, jj, count           ! local indices and counters
        integer(kind = int_kind), dimension (4) :: &
            iced_i, iced_j                      ! local arrays that store the location of points in ice near the ice front

        ! Determine distance to ice front and gradients at the point of interest
        if (point_type == 'upnt') then
            ! check at u-point !

            dist    = -1.*(dist_T(i_ind, j_ind) + dist_T(i_ind - 1, j_ind))/2  
            dist_gx = (dist_gx_T(i_ind, j_ind) + dist_gx_T(i_ind - 1, j_ind))/2
            dist_gy = (dist_gy_T(i_ind, j_ind) + dist_gy_T(i_ind - 1, j_ind))/2
            x_point = x_u(i_ind)
            y_point = y_u(j_ind)

        else if (point_type == 'vpnt') then 
            ! check at v-point !

            dist    = -1.*(dist_T(i_ind, j_ind) + dist_T(i_ind, j_ind - 1))/2
            dist_gx = (dist_gx_T(i_ind, j_ind) + dist_gx_T(i_ind, j_ind - 1))/2
            dist_gy = (dist_gy_T(i_ind, j_ind) + dist_gy_T(i_ind, j_ind - 1))/2
            x_point = x_v(i_ind)
            y_point = y_v(j_ind)

        else if (point_type == 'node') then 
            ! check at node-point !
            
            ! dist    = -1.*(dist_T(i_ind, j_ind) + dist_T(i_ind, j_ind - 1) + dist_T(i_ind - 1, j_ind - 1) + dist_T(i_ind - 1, j_ind))/4
            dist    = -1.*dist_N(i_ind, j_ind)
            dist_gx = (dist_gx_T(i_ind, j_ind) + dist_gx_T(i_ind, j_ind - 1) + dist_gx_T(i_ind - 1, j_ind - 1) + dist_gx_T(i_ind - 1, j_ind))/4
            dist_gy = (dist_gy_T(i_ind, j_ind) + dist_gy_T(i_ind, j_ind - 1) + dist_gy_T(i_ind - 1, j_ind - 1) + dist_gy_T(i_ind - 1, j_ind))/4
            x_point = x_N(i_ind)
            y_point = y_N(j_ind)

        else
            ! improperly defined point type: stop program !
            print *, "Miss defined point type occured when checking boundary conditions."
            print *, "Check routine for generating the computational masks."
            stop
        endif

        if (dist < land_inf) then
            ! the ice is close enough to be considered attached !
            uselandBC = .True.
        else
            ! the ice is not close, but we now check if it is moving towards it !

            ! locate the ice front !
            dist_grad   = sqrt(dist_gx**2 + dist_gy**2)
            dist_x      = dist*(dist_gx/dist_grad)
            dist_y      = dist*(dist_gy/dist_grad)
            x_front     = x_point + dist_x
            y_front     = y_point + dist_y

            ! locate the nearest u and v points that are in ice !
            !----- U - points --------!
                ! find u-point "down" and to the "left" (i.e. the nearest point in the negative y and x directions) !
                i   = floor(x_front/dx) + 1
                j   = floor(y_front/dx - 0.5) + 1  

                ! find surrounding u-points in ice !
                count = 0 
                proximity = 999999
                do k = i, i + 1
                    do l = j, j + 1

                        if (inIce( 'upnt', dist_T, dist_N, k, l, nxT, nyT, nxN, nyN)) then
                            ! point is in ice !
                            count               = count + 1
                            iced_i(count)       = k
                            iced_j(count)       = l 
                            proximity(count)    = sqrt((x_front - x_u(k))**2 + (y_front - y_u(l))**2)
                        else
                            ! point isn't in ice, don't store location !
                        end if
                    end do
                end do

                ! set vel to 0 is no u-points are in ice !
                if (count == 0) then
                    ! print *, "Warning: when setting boundary conditions at ", point_type, i_ind, j_ind, " the uselandBC function failed locate neighbouring u-points in ice."
                    u_front = 0.d0
                else
                    ! find closest u_point that is in ice and set velocity at the ice front to that value !
                    ii      = iced_i(minloc(proximity(1:count),1))
                    jj      = iced_j(minloc(proximity(1:count),1))
                    u_front = ugrid(ii, jj)
                endif

            !----- V - points --------!
                ! find v-point "down" and to the "left" (i.e. the nearest point in the negative y and x directions) !
                i   = floor(x_front/dx - 0.5) + 1
                j   = floor(y_front/dx) + 1

                ! find surrounding v-points in ice !
                count = 0 
                proximity = 999999
                do k = i, i + 1
                    do l = j, j + 1

                        if (inIce( 'vpnt', dist_T, dist_N, k, l, nxT, nyT, nxN, nyN)) then
                            ! point is in ice !
                            count               = count + 1
                            iced_i(count)       = k
                            iced_j(count)       = l 
                            proximity(count)    = sqrt((x_front - x_v(k))**2 + (y_front - y_v(l))**2)
                        else
                            ! point isn't in ice, don't store location !
                        end if

                    end do
                end do

                ! set vel to 0 if no v-points are in ice !
                if (count == 0) then
                    ! print *, "Warning: when setting boundary conditions at ", point_type, i_ind, j_ind, " the uselandBC function failed locate neighbouring v-points in ice."
                    v_front = 0.d0
                else 
                    ! find closest v_point that is in ice and set velocity at the ice front to that value !     
                    ii      = iced_i(minloc(proximity(1:count),1))
                    jj      = iced_j(minloc(proximity(1:count),1))
                    v_front = vgrid(ii, jj)
                endif

            ! determine the velocity of the ice front in the direction of the boundary point being considered !
            vel_perp = (dist_gx/dist_grad)*u_front + (dist_gy/dist_grad)*v_front

            if (vel_perp >= 0) then
                ! Ice is not moving towards the land, enforce open boundary condition !
                uselandBC = .false.
            else
                ! Ice is moving towards land, we now check how fast !
                if ((dist + vel_perp*dt/2) < dist/2) then
                    ! Ice has reduced its distance to land by a factor of two at t + dt/2, enforce land B.C.
                    uselandBC = .true.
                else
                    uselandBC = .false.
                endif
            endif
        endif
    end function uselandBC

!===========================================================================================!
!                                    In Ice ?                                               !
!===========================================================================================!
    logical function inIce(point_type, dist_T, dist_N, i_ind, j_ind, nxT, nyT, nxN, nyN)

        !======================================================================================
        ! This function is a simple method to query if a given point is in ice or not; it was
        ! written to clean up some of the boundary condition routines, as they were getting
        ! quite messy.
        !
        ! As we solve the distance function at T-points, we require interpolation to get 
        ! the solutions at u, v, and node points; this function approximates these with 
        ! a bilinear approximation.
        !
        ! NOTE: 
        !       -   we don't want the model to consider points that are land points as ice 
        !           points, even if the ice is attached right at said point, so this function
        !           also enforces that.
        !=====================================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            i_ind,  j_ind,  &   ! indices of the point being queried
            nxT,    nyT,    &   ! T-point limits
            nxN,    nyN         ! N-point limits 
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_T              ! distance function at T-points 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            dist_N
        character(len = 4), intent(in) :: &
            point_type          ! defines what type of point is being considered: u, v, or node point

        !==================!
        !----Local Vars----!
        !==================!
        real(kind = dbl_kind) :: &
            dist                ! distance value for the point in question
        logical :: &
            land

        if (point_type == 'upnt') then
            ! check at u-point !
            dist    = (dist_T(i_ind, j_ind) + dist_T(i_ind - 1, j_ind))/2
            land    = (lmsk(i_ind, j_ind) .or. lmsk(i_ind - 1, j_ind))
        else if (point_type == 'vpnt') then 
            ! check at v-point !
            dist    = (dist_T(i_ind, j_ind) + dist_T(i_ind, j_ind - 1))/2
            land    = (lmsk(i_ind, j_ind) .or. lmsk(i_ind, j_ind - 1))
        else if (point_type == 'node') then 
            ! check at node-point !
            ! dist    = (dist_T(i_ind, j_ind) + dist_T(i_ind, j_ind - 1) + dist_T(i_ind - 1, j_ind - 1) + dist_T(i_ind - 1, j_ind))/4
            dist    = dist_N(i_ind, j_ind)
            land    = (lmsk(i_ind, j_ind) .or. lmsk(i_ind, j_ind - 1) .or. lmsk(i_ind - 1, j_ind - 1) .or. lmsk(i_ind - 1, j_ind))
        else
            ! improperly defined point type: stop program !
            print *, "Miss defined point type occured when checking boundary conditions."
            print *, "Check routine for generating the computational masks."
            stop
        endif  
        
        if ((dist >= 0) .and. .not. land) then
            ! point is in ice !
            inIce = .True.    
        else
            ! point is not in ice !
            inIce = .False.
        endif   

    end function inIce

!===========================================================================================!
!                               Locate Halo Cells                                           !
!===========================================================================================!
    subroutine locate_halo_points(haloTi, haloTj, haloNi, haloNj, nHT_pnts, nHN_pnts, &
                                    indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts, &
                                    dist_T, dist_N, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        !====================================================================================
        ! This subroutine goes through the u and v points in the computational domain and 
        ! locates Tracer and Node points where halo cells are needed and stores their indices 
        ! in haloTi, haloTj, haloNi, haloNj. With these arrays, the halo cell values can 
        ! be updated easily and quickly. Additionally, this will allow for us to implement more 
        ! advanced methods of setting the halo cells at a later date.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,                 &   ! indices limit for U and V points 
            nxT, nyT, nxN, nyN,                 &   ! indices limit for T and N points 
            nU_pnts, nV_pnts                        ! number of U and V points in the computational domain 
        integer(kind = int_kind), dimension((nxU+2*gc_U)*(nyU+2*gc_U)), intent(in) :: &
            indxUi, indxUj                          ! location of U points in the comp domain 
        integer(kind = int_kind), dimension((nxV+2*gc_V)*(nyV+2*gc_V)), intent(in) :: &
            indxVi, indxVj                          ! location of V points in the comp domain

        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_T                                  ! distance function solution 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            dist_N

        !==================!
        !----- In/out -----!
        !==================!
        integer(kind = int_kind), intent(inout) :: &
            nHT_pnts, nHN_pnts                      ! will hold the number of halo points
        integer(kind = int_kind), dimension((nxT+2*gc_T)*(nyT+2*gc_T)), intent(inout) :: &
            haloTi, haloTj                          ! will contain the location of the halo tracer points 
        integer(kind = int_kind), dimension(nxN*nyN), intent(inout) :: & 
            haloNi, haloNj                          ! will contain the location of the halo node points 

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij                                ! local indices 
        integer(kind = int_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            HT_loc                                  ! a local array of 0's and 1's that will be used to determine the locations of halo points 
        integer(kind = int_kind), dimension(nxN, nyN) :: &
            HN_loc                                  ! a local array of 0's and 1's that will be used to determine the locations of tracer points 
        real(kind = dbl_kind) :: &
            dist1, dist2                            ! will hold the interpolated distance value at node points

        ! Initialize !
        HT_loc      = 0
        HN_loc      = 0
        nHT_pnts    = 0
        nHN_pnts    = 0
        haloTi      = 0
        haloTj      = 0
        haloNi      = 0
        haloNj      = 0

        ! U-points !
        do ij = 1, nU_pnts
            ! get indices !
            i = indxUi(ij)
            j = indxUj(ij)

            ! check which needed viscosity points are outside of the ice !
            if (dist_T(i,j)     < 0)    HT_loc(i  ,j  ) = 1   ! Tracer (i  ,j  )
            if (dist_T(i-1,j)   < 0)    HT_loc(i-1,j  ) = 1   ! Tracer (i-1,j  )

            if (dist_N(i,j) < 0) then
                HN_loc(i  ,j  ) = 1     ! Node(i  ,j  )
            end if 
            if (dist_N(i,j+1) < 0) then
                HN_loc(i  ,j+1) = 1     ! Node(i  ,j+1)
            end if 

        end do 

        ! V-points !
        do ij = 1, nV_pnts
            ! get indices !
            i = indxVi(ij)
            j = indxVj(ij)

            ! check which needed viscosity points are outside of the ice !
            if (dist_T(i,j)     < 0)    HT_loc(i  ,j  ) = 1     ! Tracer (i  ,j  )
            if (dist_T(i,j-1)   < 0)    HT_loc(i  ,j-1) = 1     ! Tracer (i  ,j-1)

            if (dist_N(i,j) < 0) then
                HN_loc(i  ,j  ) = 1     ! Node(i  ,j  )
            end if 
            if (dist_N(i+1,j) < 0) then
                HN_loc(i+1,j  ) = 1     ! Node(i+1,j  )
            end if 

        end do 

            ! save H*_loc arrays for testing !
            ! open(unit = 1, file = "../Validation_Tests/halo_cell_tests/HN_loc", status = "unknown")
            ! open(unit = 2, file = "../Validation_Tests/halo_cell_tests/HT_loc", status = "unknown")
            ! call csv_write_integer_2d(1, HN_loc)
            ! call csv_write_integer_2d(2, HT_loc)
            ! close(1)
            ! close(2)

        ! store locations of Halo cells !
        ! T points !
        do i = 1-gc_T, nxT+gc_T
            do j = 1-gc_T, nyT+gc_T 
                if (HT_loc(i,j) == 1) then 
                    nHT_pnts            = nHT_pnts + 1
                    haloTi(nHT_pnts)    = i
                    haloTj(nHT_pnts)    = j 
                else
                    ! don't store index !
                end if 
            end do 
        end do 

        ! N points !
        do i = 1, nxN
            do j = 1, nyN
                if (HN_loc(i,j) == 1) then 
                    nHN_pnts            = nHN_pnts + 1
                    haloNi(nHN_pnts)    = i 
                    haloNj(nHN_pnts)    = j
                else 
                    ! don't store index !
                end if 
            end do 
        end do 

            ! save index arrays for testing !
            open(unit = 1, file = "../Validation_Tests/halo_cell_tests/haloTi", status = "unknown")
            open(unit = 2, file = "../Validation_Tests/halo_cell_tests/haloTj", status = "unknown")
            open(unit = 3, file = "../Validation_Tests/halo_cell_tests/haloNi", status = "unknown")
            open(unit = 4, file = "../Validation_Tests/halo_cell_tests/haloNj", status = "unknown")
            call csv_write_integer_1d(1, haloTi)
            call csv_write_integer_1d(2, haloTj)
            call csv_write_integer_1d(3, haloNi)
            call csv_write_integer_1d(4, haloNj)
            close(1)
            close(2)
            close(3)
            close(4)

    end subroutine locate_halo_points

!===========================================================================================!
!                               Update Halo Points                                          !
!===========================================================================================!
    subroutine update_halo_points(zeta_T, eta_T, zeta_N, eta_N, dist_gx_T, dist_gy_T, &
                                haloTi, haloTj, haloNi, haloNj, nHN_pnts, nHT_pnts, &
                                nxT, nyT, nxN, nyN)

        !====================================================================================
        ! This routine updates the viscosities at the halo points using a first order 
        ! extrapolation. It determines what direction to extrapolate in by querying the
        ! gradient of the distance function. This routine should be called after each 
        ! non-linear iteration.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT, nyT, nxN, nyN,         &   ! indices limits for T and N points 
            nHN_pnts, nHT_pnts              ! number of halo node and tracer points 
        integer(kind = int_kind), dimension(nHT_pnts), intent(in) :: &
            haloTi, haloTj                  ! arrays containing the location of halo tracer points 
        integer(kind = int_kind), dimension(nHN_pnts), intent(in) :: &
            haloNi, haloNj                  ! arrays containing the location of halo node points 

        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_gx_T, dist_gy_T            ! distance function solution and associated gradient estimates

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            zeta_T, eta_T                   ! Ice strength and viscosities at T points 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(inout) :: &
            zeta_N, eta_N                   ! viscosities at N points 

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij,                   &   ! local counters and indices
            dir                             ! will be used to set the direction for the extrapolation

        real(kind = dbl_kind) :: &
            dist_gx, dist_gy                ! holds the interpolated gradient values at the node points 

        ! T points ! 
        do ij = 1, nHT_pnts

            ! get indices !
            i = haloTi(ij)
            j = haloTj(ij)

            ! check for dominant dist_grad component !
            if (abs(dist_gx_T(i,j)) > abs(dist_gy_T(i,j))) then 
                ! extrapolate in the x-direction !

                ! set direction !
                dir = int(sign(1.0, dist_gx_T(i,j)))

                ! extrapolate ! 
                eta_T(i,j)  = 2*eta_T(i + dir, j)   - eta_T(i + 2*dir, j)
                zeta_T(i,j) = 2*zeta_T(i + dir, j)  - zeta_T(i + 2*dir, j)
            else 
                ! extrapolate in the y-direction !

                ! set direction !
                dir = int(sign(1.0, dist_gy_T(i,j)))

                ! extrapolate !  
                eta_T(i,j)  = 2*eta_T(i, j + dir)    - eta_T(i, j + 2*dir)
                zeta_T(i,j) = 2*zeta_T(i, j + dir)   - zeta_T(i, j + 2*dir)
            end if 
        end do 

        ! N points !
        do ij = 1, nHN_pnts

            ! get indices ! 
            i = haloNi(ij)
            j = haloNj(ij)

            ! interpolate gradients !
            dist_gx = (dist_gx_T(i-1,j-1) + dist_gx_T(i,j-1) + dist_gx_T(i-1,j) + dist_gx_T(i,j))/4     
            dist_gy = (dist_gy_T(i-1,j-1) + dist_gy_T(i,j-1) + dist_gy_T(i-1,j) + dist_gy_T(i,j))/4

            ! check for dominant dist_grad component !
            if (abs(dist_gx) > abs(dist_gy)) then
                ! extrapolate in the x-direction !

                ! set direction !
                dir = int(sign(1.0, dist_gx))

                ! extrapolate !
                eta_N(i,j)  = 2*eta_N(i + dir, j)   - eta_N(i + 2*dir, j)
                zeta_N(i,j) = 2*zeta_N(i + dir, j)  - zeta_N(i + 2*dir, j)
            else
                ! extrapolate in the y-direction !

                ! set direction !
                dir = int(sign(1.0, dist_gy))

                ! extrapolate
                eta_N(i,j)  = 2*eta_N(i, j + dir) - eta_N(i, j + 2*dir)
                zeta_N(i,j) = 2*zeta_N(i, j + dir) - zeta_N(i, j + 2*dir)
            end if 
        end do

    end subroutine update_halo_points

!===========================================================================================!
!                               Clear Halo Points                                           !
!===========================================================================================!
    subroutine clear_halo_points(zeta_T, eta_T, zeta_N, eta_N, &
                                haloTi, haloTj, haloNi, haloNj, nHN_pnts, nHT_pnts, &
                                nxT, nyT, nxN, nyN)

        !====================================================================================
        ! This routine resets the Halo point values to zero, prior to each advection step and 
        ! after the non-linear solve.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT, nyT, nxN, nyN,         &   ! indices limits for T and N points 
            nHN_pnts, nHT_pnts              ! number of halo node and tracer points 
        integer(kind = int_kind), dimension(nHT_pnts), intent(in) :: &
            haloTi, haloTj                  ! arrays containing the location of halo tracer points 
        integer(kind = int_kind), dimension(nHN_pnts), intent(in) :: &
            haloNi, haloNj                  ! arrays containing the location of halo node points 

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            zeta_T, eta_T                   ! Ice strength and viscosities at T points 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(inout) :: &
            zeta_N, eta_N                   ! viscosities at N points 

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij                        ! local counters and indices

            ! T points !
            do ij = 1, nHT_pnts
                i = haloTi(ij)
                j = haloTj(ij)

                zeta_T(i,j) = 0.
                eta_T(i,j)  = 0.
            end do 

            ! N points !
            do ij = 1, nHN_pnts
                i = haloNi(ij)
                j = haloNj(ij)

                zeta_N(i,j) = 0.
                eta_N(i,j)  = 0.
            end do
    end subroutine clear_halo_points

!===========================================================================================!
!                               Dirichlet BC U-points                                       !
!===========================================================================================!
    real(kind = dbl_kind) function uBC(x,y,t)

        !====================================================================================
        ! This function is used to define the Dirichlet boundary conditions at U points, which is used 
        ! when land boundaries are present. If toy_soln = .false. the program solves the SIME
        ! and thus uses homogenous dirichlet boundary conditions, but if toy_soln = .true. then
        ! in-homogenous dirichlet conditions are used, according to the toy solution.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        if (.not. valid_pblm) then
            uBC = 0.
        else
            uBC = toysoln_w1(x,y,t)
        endif
    end function uBC

!===========================================================================================!
!                               Dirichlet BC V-points                                       !
!===========================================================================================!
    real(kind = dbl_kind) function vBC(x,y,t)

        !====================================================================================
        ! This function is used to define the Dirichlet boundary conditions at V points, which is used 
        ! when land boundaries are present. If toy_soln = .false. the program solves the SIME
        ! and thus uses homogenous dirichlet boundary conditions, but if toy_soln = .true. then
        ! in-homogenous dirichlet conditions are used, according to the toy solution.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        if (.not. valid_pblm) then
            vBC = 0.
        else
            vBC = toysoln_w2(x,y,t)
        endif
    end function vBC

!===========================================================================================!
!                               Neumann BC du/dx                                            !
!===========================================================================================!
    real(kind = dbl_kind) function dudxBC(x,y,t)

        !====================================================================================
        ! This function sets Neumann conditions for u values in the x direction. If toy_soln = 
        ! .false. homogenous neumann conditions are used. If .true. then in homogenous conditions
        ! are used, according to the derivatives of the toy solution.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        if (.not. valid_pblm) then 
            dudxBC = 0.
        else
            dudxBC = (16./(x_extent**2))*(2*x - x_extent)*toysoln_w2(x,y,t)
        end if 
    end function dudxBC

!===========================================================================================!
!                               Neumann BC du/dy                                            !
!===========================================================================================!
    real(kind = dbl_kind) function dudyBC(x,y,t)

        !====================================================================================
        ! This function sets Neumann conditions for u values in the y direction. If toy_soln = 
        ! .false. homogenous neumann conditions are used. If .true. then in homogenous conditions
        ! are used, according to the derivatives of the toy solution.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        if (.not. valid_pblm) then 
            dudyBC = 0.
        else
            dudyBC = (16./(y_extent**2))*(2*y - y_extent)*toysoln_w2(x,y,t)
        end if 
    end function dudyBC

!===========================================================================================!
!                               Neumann BC dv/dx                                            !
!===========================================================================================!
    real(kind = dbl_kind) function dvdxBC(x,y,t)

        !====================================================================================
        ! This function sets Neumann conditions for v values in the x direction. If toy_soln = 
        ! .false. homogenous neumann conditions are used. If .true. then in homogenous conditions
        ! are used, according to the derivatives of the toy solution.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        if (.not. valid_pblm) then 
            dvdxBC = 0.
        else
            dvdxBC = (-16./(x_extent**2))*(2*x - x_extent)*toysoln_w1(x,y,t)
        end if 
    end function dvdxBC

!===========================================================================================!
!                               Neumann BC dv/dy                                            !
!===========================================================================================!
    real(kind = dbl_kind) function dvdyBC(x,y,t)

        !====================================================================================
        ! This function sets Neumann conditions for v values in the y direction. If toy_soln = 
        ! .false. homogenous neumann conditions are used. If .true. then in homogenous conditions
        ! are used, according to the derivatives of the toy solution.
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        if (.not. valid_pblm) then 
            dvdyBC = 0.
        else
            dvdyBC = (-16./(y_extent**2))*(2*y - y_extent)*toysoln_w1(x,y,t)
        end if 
    end function dvdyBC
end module domain_routines
