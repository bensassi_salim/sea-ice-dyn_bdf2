! This module contains the main solver routines for JFNKsolver.f90
! Original Author: Clinton Seinen
!
! The contained routines are:
!           - calc_b_0_update_forcing_r02_BDF1           
!           - calc_b_0_update_forcing_r02
!           - calc_b_BDF1
!           - calc_b
!           - calc_Au_r02_BDF1                           
!           - calc_Au_r02
!           - calc_Au_homogenous_BDF1 
!           - calc_Au_homogenous
!           - calc_Dbw_BDF1 
!           - calc_Dbw
!           - jacobian_multiplication_r02_BDF1           
!           - jacobian_multiplication_r02
!           - gmres_solve_r02_BDF1                                    
!           - gmres_solve_r02
!           - JFNKsolve_r02_BDF1                         
!           - JFNKsolve_r02
!           - SmoothUV
!           - AdvectTracers
!           - set_Tvels
!           - calc_slopes
!           - minmod
!           - calc_nrm2

module solver_routines

! -----------------------------------------------------------------------------!
    !                       Module References                           !
    use modelparams         ! includes physical, numerical, and grid parameters
    use forcing             ! includes routines for updating forcing data
    use initialization      ! includes initialization routines and grid parameters that are defined according to dx and will remain unchanged
    use var_routines        ! includes routines for calculating variables needed in the solver (for example, viscosity)
    use domain_routines     ! includes routines to limit the domain and set BCs
    use csv_file            ! module for writing data to csv format

! -----------------------------------------------------------------------------!

implicit none

    include "mkl_rci.fi"    ! needed for gmres routines
! -----------------------------------------------------------------------------!
    !                           Contains                                !
! -----------------------------------------------------------------------------!

                                             !=================!
!============================================!  Start of BDF1  !===============================================!
                                             !=================!

contains
!===========================================================================================!
!                           Calc b_0 and update forcing arrays                              !
!===========================================================================================!

!===========================================================================================!
!                           Calc b_0 and update forcing arrays r02                          !
!===========================================================================================!
    subroutine calc_b_0_update_forcing_r02_BDF1(b_0, ugrid, vgrid, Cw_u, Cw_v, &
                                    uocn_u, vocn_u, uocn_v, vocn_v, uwnd_u, vwnd_u, uwnd_v, vwnd_v, &
                                    zeta_u, zeta_v, eta_u, eta_v, h_u, h_v, P_T, ulmsk, uimsk, vlmsk, vimsk, &
                                    zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                    nxU, nyU, nxV, nyV, nxT, nyT, nU_pnts, nV_pnts, & 
                                    indxUi, indxUj, indxVi, indxVj, &
                                    time)

        !===================================================================================
        ! NOTE: This is an updated version of the routine which uses the reformulation of the 
        !       rheology term, i.e. where we calculate the viscsity gradients in closed form 
        !       at U and V points
        !
        ! This subroutine calculates the portion of b that is not dependent on the 
        ! current solution. 
        !
        ! This portion of b includes terms resulting from the crank-nicolson scheme and 
        ! those from forcing terms that do not depend on the current solution.
        !
        ! Note 1:   in calculating the crank-nicolson component, numerical masks are used
        !           in order to create the necessary boundary conditions.
        !
        ! Note 2:   in calculating the forcing part of b_0, forcing is required from the 
        !           previous time step and the current one; as a result, this routine also
        !           updates the forcing arrays, uocn, vocn, uwnd, and vwnd 
        !
        ! Note 3:   like the Au routine, when checking for masking this routine only checks
        !           the first 8 masks as these are associated with the velocities needed for 
        !           this operation; the remaining 12 masks are used in the viscosity calculations.
        !===================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain 
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                                ! current time level
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                                 ! ice strength at T points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosity values - U points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! viscosity derivatives - U points 
            h_u, Cw_u                           ! ice thickness and non-linear water drag coefficient - u points         
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosity values - V points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! viscosity derivatives - V points
            h_v, Cw_v                           ! ice thickness and non-linear water drag coefficient - v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                               ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                               ! v velocity on the grid

        !==================!
        ! ----- In/Out ----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            b_0                             ! linear portion of the b vector
        real(kind = dbl_kind), dimension (nxU, nyU), intent(inout) :: &
            uocn_u, vocn_u, uwnd_u, vwnd_u  ! ocean and wind velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(inout) :: &
            uocn_v, vocn_v, uwnd_v, vwnd_v  ! ocean and wind velocities at v points

        !==================!
        ! ----- Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i,j, ij 

        real(kind = dbl_kind) :: &      
            cnst_1, cnst_2, cnst_3, cnst_4,                 &   ! constants implemented to aid in computation of b_0
            cnst_5, cnst_6, cnst_7, cnst_8,                 &
            cnst_9, cnst_10, cnst_11, cnst_12,              &
            c1, c2, c3, c4, c5, c6, c7,                     &   ! coefficients for the crank-nicolson portion of b_0; implemented to clean up code
            c8, c9,                                         &
            f1, f2, f3, f4, f5, f6,                         &   ! individual forcing components; implemented to clean up code and aid in routine validation
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p1m1, u_p0p0, &   ! masked velocities
            v_m1p0, v_p0p0, v_m1p1, v_p0p1, v_p0m1, v_p1p0, &
            halfdx

            !============================!
            ! Define necessary constants !
            !============================!
                halfdx = dx/2
                
                cnst_3 = rho*f 
                cnst_4 = rho/dt 
                cnst_5 = rho_a*Cd_a
                cnst_6 = rho*f
                cnst_7 = 1./(2*dx)

            !===============================================================================!
            ! Populate portion of b_0 - Crank-Nicolson and forcing from previous time level !
            !===============================================================================! 
                !===============================!
                !   U-component of the SIME     !
                !===============================!
                do ij = 1,nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    
                    ! calculate Crank-Nicolson coefficients !
                    c3 = -cnst_4*h_u(i,j)                                                                       ! u(i  ,j  ) !
                    
                    ! calculate forcing components for previous time level !
                    f1 = -cnst_5*sqrt(uwnd_u(i,j)**2 + vwnd_u(i,j)**2)*(uwnd_u(i,j)*cos_a - vwnd_u(i,j)*sin_a)   ! air drag
                  !  f2 = -(Cw_u(i,j)/2)*(uocn_u(i,j)*cos_w - vocn_u(i,j)*sin_w)                                 ! water drag 
                    f3 = cnst_6*h_u(i,j)*vocn_u(i,j)                                                             ! sea surface tilt
                    f4 = cnst_7*(P_T(i,j) - P_T(i-1,j))                                                          ! ice strength gradient
                                                 
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        !============================================================!
                        ! Use masked velocities as at least one of the first 8 masks !
                        ! are activated.                                             !
                        !============================================================! 
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time - dt)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time - dt))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time - dt)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time - dt)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time - dt))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time - dt)

                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time - dt)                           

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time - dt)                             

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time - dt)                         

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt))) &
                                    +   (1- ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time - dt)   

                        !===================================!
                        ! Populate the vector using masking !
                        !===================================!
                        b_0(ij) =    c3*ugrid(i,j)    &                                
                                     +   f1 + f3 + f4
                    else
                        !=============================================================!
                        ! Masking isn't required. Populate the vector without masking !
                        !=============================================================!
                        b_0(ij) =  c3*ugrid(i  ,j  )                                                 &
                                     +   f1 + f3 + f4
                    end if
                end do 

                !===============================!
                !   V-component of the SIME     !
                !===============================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                   

                  
                    c7 = -cnst_4*h_v(i,j)                                                                       ! v(i  ,j  ) !
                   
                    ! calculate forcing components from the previous time leve !
                    f1 = -cnst_5*sqrt(uwnd_v(i,j)**2 + vwnd_v(i,j)**2)*(vwnd_v(i,j)*cos_a + uwnd_v(i,j)*sin_a)  ! air drag
                  !  f2 = -(Cw_v(i,j)/2)*(vocn_v(i,j)*cos_w + uocn_v(i,j)*sin_w)                                 ! water drag
                    f3 = -cnst_6*h_v(i,j)*uocn_v(i,j)                                                           ! sea surface tilt
                    f4 = cnst_7*(P_T(i,j) - P_T(i,j-1))                                                         ! ice strength gradient

                    if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        !============================================================!
                        ! Use masked velocities as at least one of the first 8 masks !
                        ! are activated.                                             !
                        !============================================================! 
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time - dt)                         
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time - dt)                       
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time - dt)                           
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt)))  &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time - dt)                           

                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time - dt))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time - dt)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time - dt)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time - dt))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time - dt)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time - dt) 

                        b_0(nU_pnts + ij)   =    c7*vgrid(i,j)                           &
                                                +   f1 + f3 + f4 
                    else
                        !========================!
                        ! Masking isn't required !
                        !========================!
                        b_0(nU_pnts + ij)   =    c7*vgrid(i  ,j  )                         &
                                                 +   f1 + f3 + f4
                    end if
                end do

            !======================================!
            ! Update forcing to current time level !
            !======================================!
                call get_ocn_forcing(uocn_u, uocn_v, vocn_u, vocn_v, time, &          ! located in forcing module
                            nxT, nyT, nxU, nyU, nxV, nyV)
                call get_wnd_forcing(uwnd_u, uwnd_v, vwnd_u, vwnd_v, time, &          ! located in forcing module
                                nxU, nyU, nxV, nyV)

           
    end subroutine calc_b_0_update_forcing_r02_BDF1

!===========================================================================================!
!                                          Calc b                                           !
!===========================================================================================!
    subroutine calc_b_BDF1(b, b_0, Cw_u, Cw_v, &
                    uocn_u, vocn_u, uocn_v, vocn_v, &
                    nxU, nyU, nxV, nyV, &
                    nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)
        !===================================================================================
        ! This subroutine calculates b by taking adding b_0 and the component of forcing
        ! that depends on the current sea ice velocity (ocean forcing). These two parts of b 
        ! were split for computational purposes. When a new non-linear iterate is calculated, 
        ! this split allows us to update b by modifying the ocean forcing term and leave 
        ! b_0 unchanged; limiting our computations.
        !===================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            uocn_u, vocn_u, Cw_u            ! ocean velocities and non-linear water drag coef. at U points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            uocn_v, vocn_v, Cw_v            ! ocean velocities and non-linear water drag coef. at V points
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(in) :: &
            b_0

        !==================!
        ! ----- Inout -----!
        !==================!
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(inout) :: &
            b

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i,j, ij 
        real(kind = dbl_kind) :: & 
            f7                              ! holds the updated water drag forcing - called f7 to match the naming convention from the calc_b_0_update_forcing routine

        !====== U - points ======!
        do ij = 1, nU_pnts
            ! get indices
            i = indxUi(ij)
            j = indxUj(ij)

            ! new water drag forcing 
            f7 = -(Cw_u(i,j))*(uocn_u(i,j)*cos_w - vocn_u(i,j)*sin_w)

            ! calculate b
            b(ij) = b_0(ij) + f7

        enddo

        !====== V - points ======!
        do ij = 1, nV_pnts
            ! get indices
            i = indxVi(ij)
            j = indxVj(ij)

            ! new water drag forcing 
            f7 = -(Cw_v(i,j))*(vocn_v(i,j)*cos_w + uocn_v(i,j)*sin_w)

            ! calculate b
            b(nU_pnts + ij) = b_0(nU_pnts + ij) + f7

        enddo
    end subroutine calc_b_BDF1

!===========================================================================================!
!                                          Calc Au                                          !
!===========================================================================================!
   
!===========================================================================================!
!                                   Calc Au - r02                                           !
!===========================================================================================!
    subroutine calc_Au_r02_BDF1(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk, u_vect)

        !========================================================================================
        ! NOTE: This is the updated version of the routine where the reformulation of the rheology 
        !       term is used; i.e. we calculate the derivatives of the viscosities in closed form.
        !
        ! This subroutine calculates the matrix vector product, Au, where A is our linearized 
        ! coef matrix and u is the velocity vector. When checking for masking, this routine only 
        ! looks at the first 8 masks; the other 12 are only needed in the viscosity calculations.
        !
        ! Note that this routine is also used in the calculation of the jacobian approximation.
        !
        !========================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                                ! current time value
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosity values - U points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! viscosity derivatives - U points 
            h_u, Cw_u                           ! ice thickness and non-linear water drag coefficient - U points      
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosity values - V points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! viscosity derivatives - V points
            h_v, Cw_v                           ! ice thickness and non-linear water drag coefficient - V points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                               ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                               ! v velocity on the grid
        real(kind = dbl_kind), optional, dimension(nU_pnts + nV_pnts), intent (in) :: &
            u_vect                              ! velocity in vector form, this is only used for gmres validation 

        !==================!
        ! ----- Inout------!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            Au 
        
        !==================!
        ! ----- Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij
        real(kind = dbl_kind), allocatable, dimension(:,:) :: &
            ugrid_vect, vgrid_vect              ! if u_vect is present these will hold its values on the grid 
        real(kind = dbl_kind) :: &
            cnst_1, cnst_2, cnst_3, cnst_4,                 &   ! arbitrary constants 
            cnst_5, cnst_6, cnst_7, cnst_8, cnst_9,         &   ! constants used in calculation of matrix coefficients
            c1, c2, c3, c4, c5,                             &   ! coefficients for the crank-nicolson portion of Au; implemented to clean up code
            c6, c7, c8, c9,                                 &   
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p1m1, u_p0p0, &   ! masked velocities
            v_m1p0, v_p0p0, v_m1p1, v_p0p1, v_p0m1, v_p1p0, &
            halfdx 

        !===============================!
        !   Define Necessary Constants  !
        !===============================!
            halfdx = dx/2
            cnst_1 = 1./(dx*dx)
            cnst_2 = 1./(2*dx)
            cnst_3 = rho*f
            cnst_4 = rho/dt

        !==============================================!
        ! Perform Mat-Vec Using the gridded velocities !
        !==============================================!
        if( .not. present(u_vect)) then 

            !===============================!
            !   U-component of the SIME     !
            !===============================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (cnst_3*h_u(i,j) + Cw_u(i,j)*sin_w)/4
                    cnst_6 = eta_gx_u(i,j) + zeta_gx_u(i,j)
                    cnst_7 = eta_u(i,j) + zeta_u(i,j)
                    cnst_8 = zeta_gx_u(i,j) - eta_gx_u(i,j) + eta_gy_u(i,j)
                    cnst_9 = zeta_gx_u(i,j) - eta_gx_u(i,j) - eta_gy_u(i,j)

                    ! calculate coefficients !
                    c1 = -cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                          ! u(i  ,j-1) coef !
                    c2 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! u(i-1,j  ) coef !
                    c3 = -cnst_4*h_u(i,j) - Cw_u(i,j)*cos_w - (2./(dx*dx))*(2*eta_u(i,j) + zeta_u(i,j))     ! u(i  ,j  ) coef !
                    c4 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! u(i+1,j  ) coef !
                    c5 = cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                           ! u(i  ,j+1) coef !
                    c6 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i-1,j  ) coef !
                    c7 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i  ,j  ) coef !
                    c8 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i-1,j+1) coef !
                    c9 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i  ,j+1) coef !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time)

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time) 

                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(ij)  =   c1*u_p0m1 + c2*u_m1p0 + c3*ugrid(i,j) + c4*u_p1p0 + c5*u_p0p1 &
                                    +   c6*v_m1p0 + c7*v_p0p0 + c8*v_m1p1 + c9*v_p0p1 
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(ij)  =   c1*ugrid(i  ,j-1) + c2*ugrid(i-1,j  ) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) + c5*ugrid(i  ,j+1) &
                                    +   c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i-1,j+1) + c9*vgrid(i  ,j+1)
                    end if 
                end do 

            !===============================!
            !   V-component of the SIME     !
            !===============================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (-cnst_3*h_v(i,j) - Cw_v(i,j)*sin_w)/4
                    cnst_6 = eta_gy_v(i,j) + zeta_gy_v(i,j)
                    cnst_7 = eta_v(i,j) + zeta_v(i,j)
                    cnst_8 = zeta_gy_v(i,j) - eta_gy_v(i,j) + eta_gx_v(i,j)
                    cnst_9 = zeta_gy_v(i,j) - eta_gy_v(i,j) - eta_gx_v(i,j)

                    ! calculate coefficients !
                    c1 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i  ,j-1) !
                    c2 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i+1,j-1) !
                    c3 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i  ,j  ) !
                    c4 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i+1,j  ) !
                    c5 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! v(i  ,j-1) !
                    c6 = -cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                          ! v(i-1,j  ) !
                    c7 = -cnst_4*h_v(i,j) - Cw_v(i,j)*cos_w - (2./(dx*dx))*(2*eta_v(i,j) + zeta_v(i,j))     ! v(i  ,j  ) !
                    c8 = cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                           ! v(i+1,j  ) !
                    c9 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! v(i  ,j+1) !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                     if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)                            

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time) 
                            
                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(nU_pnts + ij)    =   c1*u_p0m1 + c2*u_p1m1 + c3*u_p0p0 + c4*u_p1p0 &
                                                +   c5*v_p0m1 + c6*v_m1p0 + c7*vgrid(i,j) + c8*v_p1p0 + c9*v_p0p1
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(nU_pnts + ij)    =   c1*ugrid(i  ,j-1) + c2*ugrid(i+1,j-1) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) &
                                                +   c5*vgrid(i  ,j-1) + c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i+1,j  ) + c9*vgrid(i  ,j+1)
                    end if
                end do

        !=================================================!
        ! Perform Mat-Vec Using vector form of velocities !
        !=================================================!
        else
            allocate(ugrid_vect(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), vgrid_vect(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V))
            do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)
                    ugrid_vect(i,j) = u_vect(ij)
                end do
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)
                    vgrid_vect(i,j) = u_vect(ij + nU_pnts)
            end do

            !===============================!
            !   U-component of the SIME     !
            !===============================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (cnst_3*h_u(i,j) + Cw_u(i,j)*sin_w)/4
                    cnst_6 = eta_gx_u(i,j) + zeta_gx_u(i,j)
                    cnst_7 = eta_u(i,j) + zeta_u(i,j)
                    cnst_8 = zeta_gx_u(i,j) - eta_gx_u(i,j) + eta_gy_u(i,j)
                    cnst_9 = zeta_gx_u(i,j) - eta_gx_u(i,j) - eta_gy_u(i,j)

                    ! calculate coefficients !
                    c1 = -cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                          ! u(i  ,j-1) coef !
                    c2 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! u(i-1,j  ) coef !
                    c3 = -cnst_4*h_u(i,j) - Cw_u(i,j)*cos_w - (2./(dx*dx))*(2*eta_u(i,j) + zeta_u(i,j))     ! u(i  ,j  ) coef !
                    c4 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! u(i+1,j  ) coef !
                    c5 = cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                           ! u(i  ,j+1) coef !
                    c6 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i-1,j  ) coef !
                    c7 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i  ,j  ) coef !
                    c8 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i-1,j+1) coef !
                    c9 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i  ,j+1) coef !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid_vect(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid_vect(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid_vect(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid_vect(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid_vect(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid_vect(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid_vect(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid_vect(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time)

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid_vect(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid_vect(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid_vect(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid_vect(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid_vect(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid_vect(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid_vect(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid_vect(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid_vect(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid_vect(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid_vect(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid_vect(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid_vect(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid_vect(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid_vect(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid_vect(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time) 

                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(ij)  =   c1*u_p0m1 + c2*u_m1p0 + c3*ugrid_vect(i,j) + c4*u_p1p0 + c5*u_p0p1 &
                                    +   c6*v_m1p0 + c7*v_p0p0 + c8*v_m1p1 + c9*v_p0p1 
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(ij)  =   c1*ugrid_vect(i  ,j-1) + c2*ugrid_vect(i-1,j  ) + c3*ugrid_vect(i  ,j  ) + c4*ugrid_vect(i+1,j  ) + c5*ugrid_vect(i  ,j+1) &
                                    +   c6*vgrid_vect(i-1,j  ) + c7*vgrid_vect(i  ,j  ) + c8*vgrid_vect(i-1,j+1) + c9*vgrid_vect(i  ,j+1)
                    end if 
                end do 

            !===============================!
            !   V-component of the SIME     !
            !===============================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (-cnst_3*h_v(i,j) - Cw_v(i,j)*sin_w)/4
                    cnst_6 = eta_gy_v(i,j) + zeta_gy_v(i,j)
                    cnst_7 = eta_v(i,j) + zeta_v(i,j)
                    cnst_8 = zeta_gy_v(i,j) - eta_gy_v(i,j) + eta_gx_v(i,j)
                    cnst_9 = zeta_gy_v(i,j) - eta_gy_v(i,j) - eta_gx_v(i,j)

                    ! calculate coefficients !
                    c1 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i  ,j-1) !
                    c2 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i+1,j-1) !
                    c3 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i  ,j  ) !
                    c4 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i+1,j  ) !
                    c5 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! v(i  ,j-1) !
                    c6 = -cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                          ! v(i-1,j  ) !
                    c7 = -cnst_4*h_v(i,j) - Cw_v(i,j)*cos_w - (2./(dx*dx))*(2*eta_v(i,j) + zeta_v(i,j))     ! v(i  ,j  ) !
                    c8 = cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                           ! v(i+1,j  ) !
                    c9 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! v(i  ,j+1) !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                     if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid_vect(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid_vect(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid_vect(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid_vect(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid_vect(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid_vect(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid_vect(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid_vect(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid_vect(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid_vect(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid_vect(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid_vect(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid_vect(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid_vect(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid_vect(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid_vect(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)                            

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid_vect(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid_vect(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid_vect(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid_vect(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid_vect(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid_vect(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid_vect(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid_vect(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time) 
                            
                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(nU_pnts + ij)    =   c1*u_p0m1 + c2*u_p1m1 + c3*u_p0p0 + c4*u_p1p0 &
                                                +   c5*v_p0m1 + c6*v_m1p0 + c7*vgrid_vect(i,j) + c8*v_p1p0 + c9*v_p0p1
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(nU_pnts + ij)    =   c1*ugrid_vect(i  ,j-1) + c2*ugrid_vect(i+1,j-1) + c3*ugrid_vect(i  ,j  ) + c4*ugrid_vect(i+1,j  ) &
                                                +   c5*vgrid_vect(i  ,j-1) + c6*vgrid_vect(i-1,j  ) + c7*vgrid_vect(i  ,j  ) + c8*vgrid_vect(i+1,j  ) + c9*vgrid_vect(i  ,j+1)
                    end if
                end do
        end if  
    end subroutine calc_Au_r02_BDF1

!===========================================================================================!
!                                   Calc Au - homogenous BCs                                !
!===========================================================================================!
    subroutine calc_Au_homogenous_BDF1(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                ulmsk, uimsk, vlmsk, vimsk, u_vect)

        !========================================================================================
        ! This routine uses the new treatement of the rheology term (where the closed form of the 
        ! viscosity gradients are used) to calculate the matrix vector product with homogenous BCs
        ! on u. This was created because when we perform the jacobian multiplication we want to use 
        ! homogenous conditions on delta u.
        !
        ! NOTE: the vector functionality hasn't been incorporated yet.
        !
        !========================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                                ! current time value
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosity values - U points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! viscosity derivatives - U points 
            h_u, Cw_u                           ! ice thickness and non-linear water drag coefficient - U points      
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosity values - V points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! viscosity derivatives - V points
            h_v, Cw_v                           ! ice thickness and non-linear water drag coefficient - V points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                               ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                               ! v velocity on the grid
        real(kind = dbl_kind), optional, dimension(nU_pnts + nV_pnts), intent (in) :: &
            u_vect                              ! velocity in vector form, this is only used for gmres validation 

        !==================!
        ! ----- Inout------!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            Au 
        
        !==================!
        ! ----- Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij
        real(kind = dbl_kind), allocatable, dimension(:,:) :: &
            ugrid_vect, vgrid_vect                              ! if u_vect is present these will hold its values on the grid 
        real(kind = dbl_kind) :: &
            cnst_1, cnst_2, cnst_3, cnst_4,                 &   ! arbitrary constants 
            cnst_5, cnst_6, cnst_7, cnst_8, cnst_9,         &   ! constants used in calculation of matrix coefficients
            c1, c2, c3, c4, c5,                             &   ! coefficients for the crank-nicolson portion of Au; implemented to clean up code
            c6, c7, c8, c9,                                 &   
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p1m1, u_p0p0, &   ! masked velocities
            v_m1p0, v_p0p0, v_m1p1, v_p0p1, v_p0m1, v_p1p0, &
            halfdx 

        !===============================!
        !   Define Necessary Constants  !
        !===============================!
            halfdx = dx/2
            cnst_1 = 1./(dx*dx)
            cnst_2 = 1./(2*dx)
            cnst_3 = rho*f
            cnst_4 = rho/dt

        !==============================================!
        ! Perform Mat-Vec Using the gridded velocities !
        !==============================================!
        if( .not. present(u_vect)) then 

            !===============================!
            !   U-component of the SIME     !
            !===============================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (cnst_3*h_u(i,j) + Cw_u(i,j)*sin_w)/4
                    cnst_6 = eta_gx_u(i,j) + zeta_gx_u(i,j)
                    cnst_7 = eta_u(i,j) + zeta_u(i,j)
                    cnst_8 = zeta_gx_u(i,j) - eta_gx_u(i,j) + eta_gy_u(i,j)
                    cnst_9 = zeta_gx_u(i,j) - eta_gx_u(i,j) - eta_gy_u(i,j)

                    ! calculate coefficients !
                    c1 = -cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                          ! u(i  ,j-1) coef !
                    c2 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! u(i-1,j  ) coef !
                    c3 = -cnst_4*h_u(i,j) - Cw_u(i,j)*cos_w - (2./(dx*dx))*(2*eta_u(i,j) + zeta_u(i,j))     ! u(i  ,j  ) coef !
                    c4 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! u(i+1,j  ) coef !
                    c5 = cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                           ! u(i  ,j+1) coef !
                    c6 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i-1,j  ) coef !
                    c7 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i  ,j  ) coef !
                    c8 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i-1,j+1) coef !
                    c9 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i  ,j+1) coef !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*ugrid(i  ,j  ))
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*ugrid(i  ,j  ))
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*ugrid(i  ,j  ))
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*ugrid(i  ,j  ))

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                    vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            vgrid(i  ,j  ) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         vgrid(i-1,j+1) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   vgrid(i  ,j+1)) 

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                    vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            vgrid(i-1,j  ) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         vgrid(i  ,j+1) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   vgrid(i-1,j+1)) 

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                    vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            vgrid(i  ,j+1) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         vgrid(i-1,j  ) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   vgrid(i  ,j  )) 

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                    vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         vgrid(i  ,j  ) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   vgrid(i-1,j  )) 

                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(ij)  =   c1*u_p0m1 + c2*u_m1p0 + c3*ugrid(i,j) + c4*u_p1p0 + c5*u_p0p1 &
                                    +   c6*v_m1p0 + c7*v_p0p0 + c8*v_m1p1 + c9*v_p0p1 
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(ij)  =   c1*ugrid(i  ,j-1) + c2*ugrid(i-1,j  ) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) + c5*ugrid(i  ,j+1) &
                                    +   c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i-1,j+1) + c9*vgrid(i  ,j+1)
                    end if 
                end do 

            !===============================!
            !   V-component of the SIME     !
            !===============================!
                do ij = 1, nV_pnts

                    i = indxVi(ij)
                    j = indxVj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (-cnst_3*h_v(i,j) - Cw_v(i,j)*sin_w)/4
                    cnst_6 = eta_gy_v(i,j) + zeta_gy_v(i,j)
                    cnst_7 = eta_v(i,j) + zeta_v(i,j)
                    cnst_8 = zeta_gy_v(i,j) - eta_gy_v(i,j) + eta_gx_v(i,j)
                    cnst_9 = zeta_gy_v(i,j) - eta_gy_v(i,j) - eta_gx_v(i,j)

                    ! calculate coefficients !
                    c1 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i  ,j-1) !
                    c2 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i+1,j-1) !
                    c3 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i  ,j  ) !
                    c4 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i+1,j  ) !
                    c5 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! v(i  ,j-1) !
                    c6 = -cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                          ! v(i-1,j  ) !
                    c7 = -cnst_4*h_v(i,j) - Cw_v(i,j)*cos_w - (2./(dx*dx))*(2*eta_v(i,j) + zeta_v(i,j))     ! v(i  ,j  ) !
                    c8 = cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                           ! v(i+1,j  ) !
                    c9 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! v(i  ,j+1) !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                     if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                ugrid(i  ,j-1) &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          ugrid(i  ,j  ) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       ugrid(i+1,j-1) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* ugrid(i+1,j  )) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                ugrid(i+1,j-1) &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          ugrid(i+1,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       ugrid(i  ,j-1) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* ugrid(i  ,j  )) 
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          ugrid(i  ,j-1) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       ugrid(i+1,j  ) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* ugrid(i+1,j-1)) 
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          ugrid(i+1,j-1) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       ugrid(i  ,j  ) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* ugrid(i  ,j-1))                           

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*vgrid(i,j))  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*vgrid(i,j))
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*vgrid(i,j))
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*vgrid(i,j)) 
                            
                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(nU_pnts + ij)    =   c1*u_p0m1 + c2*u_p1m1 + c3*u_p0p0 + c4*u_p1p0 &
                                                +   c5*v_p0m1 + c6*v_m1p0 + c7*vgrid(i,j) + c8*v_p1p0 + c9*v_p0p1
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(nU_pnts + ij)    =   c1*ugrid(i  ,j-1) + c2*ugrid(i+1,j-1) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) &
                                                +   c5*vgrid(i  ,j-1) + c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i+1,j  ) + c9*vgrid(i  ,j+1)
                    end if
                end do

        !=================================================!
        ! Perform Mat-Vec Using vector form of velocities !
        !=================================================!
        else

        end if  

    end subroutine calc_Au_homogenous_BDF1

!===========================================================================================!
!                                      Calc Dbw                                             !
!===========================================================================================!
    subroutine calc_Dbw_BDF1(Dbw, w, time, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk)

        !====================================================================
        !  This subroutine calculates the Db(u)w portion of the jacobian 
        !  multiplication, where Db(u) is the Jacobian matrix associated with
        !  the vector b, with respect to u.
        !
        ! Note that due to the nature of this multiplication, homogenous BCs 
        ! are always used for the vector w, but non-homogenous BCs may be used 
        ! for u, depending on if the validation problem is being run or not.
        !====================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind) :: & 
            nxU, nyU, &             ! indices limit - U points (not including ghost cells)
            nxV, nyV, &             ! indices limit - V points (not including ghost cells)
            nU_pnts, nV_pnts        ! number of U and V points in the computational domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj          ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj          ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk            ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk            ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                    ! current time value
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            uocn_u, vocn_u
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            uocn_v, vocn_v
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                   ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                   ! v velocity on the grid
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(in) :: &
            w                       ! the arbitrary vector

        !==================!
        ! ---- In/out  ----!
        !==================!
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(inout) :: &
            Dbw                     ! the resulting vector from the multplication 

        !==================!
        ! ----- Local  ----!
        !==================!
        integer(kind = int_kind) :: & 
            i, j, ij

        real(kind = dbl_kind) :: &
            cu_1, cu_2, cu_3, cu_4, &   ! constants used in the calculation of Dbw.
            cv_1, cv_2, cv_3, cv_4, &   
            w1, w2, w3, w4,         &   ! masked velocity deltas for arbitrary w vector
            u1, u2, u3, u4,         &   ! masked u velocities
            v1, v2, v3, v4,         &   ! masked v velocities 
            halfdx
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
            w_u                         ! the vector w placed on the grid at U points 
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
            w_v                         ! the vector w placed on the grid at V points 

        w_u = 0.
        w_v = 0.
        Dbw = 0.

        halfdx = dx/2

        ! place w on the grid !
        do ij = 1, nU_pnts
            i = indxUi(ij)
            j = indxUj(ij)

            w_u(i,j) = w(ij)
        end do 
        do ij = 1, nV_pnts
            i = indxVi(ij)
            j = indxVj(ij)

            w_v(i,j) = w(ij + nU_pnts)
        end do

        !====== Calc Dbw =========!
        ! U-points !
        do ij = 1, nU_pnts
            i = indxUi(ij)
            j = indxUj(ij)

            ! calc constants !
            cu_1 = -rho_w*Cd_w*(uocn_u(i,j)*cos_w - vocn_u(i,j)*sin_w)
            cu_2 = ugrid(i,j) - uocn_u(i,j)

            if ((product(ulmsk(i,j,:)) == 0) .or. (product(uimsk(i,j,:)) == 0)) then
                ! Masking is required as at least of the masks is activated !
                ! mask velocities at the surrounding v points around the u point of interest

                ! v(i-1,j) !
                v1 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                         vgrid(i-1,j  ) &
                        +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                        +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                        +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)
    
                ! v(i,j) !
                v2 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                         vgrid(i  ,j  ) &  
                        +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                        +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                        +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                ! v(i-1,j+1) !
                v3 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                         vgrid(i-1,j+1) &  
                        +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                        +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                        +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                ! v(i,j+1) !
                v4 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                         vgrid(i  ,j+1) &
                        +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                        +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                        +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                        +   (1- ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time) 

                w1 = ulmsk(i,j,5)*(uimsk(i,j,5)*w_v(i-1,j  )  +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*w_v(i  ,j  )  +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*w_v(i-1,j+1) +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*w_v(i  ,j+1)) 
                w2 = ulmsk(i,j,6)*(uimsk(i,j,6)*w_v(i  ,j  )  +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*w_v(i-1,j  )  +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*w_v(i  ,j+1) +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*w_v(i-1,j+1))
                w3 = ulmsk(i,j,7)*(uimsk(i,j,7)*w_v(i-1,j+1)  +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*w_v(i  ,j+1)  +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*w_v(i-1,j  ) +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*w_v(i  ,j  )) 
                w4 = ulmsk(i,j,8)*(uimsk(i,j,8)*w_v(i  ,j+1)  +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*w_v(i-1,j+1)  +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*w_v(i  ,j  ) +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*w_v(i-1,j  ))   

                ! calc remaining constants
                cu_3 = (v1 + v2 + v3 + v4)/4 - vocn_u(i,j)
                cu_4 = sqrt(cu_2**2 + cu_3**2)

                ! calc Dbw
                Dbw(ij) = cu_1*cu_2*w_u(i,j)/cu_4 + (cu_1*cu_3/(4*cu_4))*(w1 + w2 + w3 + w4) 
            else
                ! Masking isn't required

                ! calc remaining constants
                cu_3 = (vgrid(i,j) + vgrid(i-1,j) + vgrid(i-1,j+1) + vgrid(i,j+1))/4 - vocn_u(i,j)
                cu_4 = sqrt(cu_2**2 + cu_3**2)

                ! calc Dbw
                Dbw(ij) = cu_1*cu_2*w_u(i,j)/cu_4 + (cu_1*cu_3/(4*cu_4))*(w_v(i-1,j) + w_v(i,j) + w_v(i-1,j+1) + w_v(i,j+1)) 
            endif
        enddo

        ! V-points !
        do ij = 1, nV_pnts
            i = indxVi(ij)
            j = indxVj(ij)

            ! calc constants !
            cv_1 = -rho_w*Cd_w*(vocn_v(i,j)*cos_w + uocn_v(i,j)*sin_w)
            cv_2 = vgrid(i,j) - vocn_v(i,j)

            if ((product(vlmsk(i,j,:)) == 0) .or. (product(vimsk(i,j,:)) == 0)) then
                ! Masking is required as at least of the masks is activated !
                ! mask velocities at the surounding u points around the v point of interest
               
                ! u(i,j-1) !
                u1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                     ugrid(i  ,j-1)  &
                        + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                        + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                        + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                
                ! u(i+1,j-1) !
                u2 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                     ugrid(i+1,j-1)  &
                        + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                        + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                
                ! u(i,j) !
                u3 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                     ugrid(i  ,j  ) & 
                        + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                        + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                
                ! u(i+1,j) !
                u4 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                     ugrid(i+1,j  ) & 
                        + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                        + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)

                w1 = vlmsk(i,j,1)*(vimsk(i,j,1)*w_u(i  ,j-1) + vimsk(i,j,3)*(1 - vimsk(i,j,1))*w_u(i  ,j  ) + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*w_u(i+1,j-1)    +   (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*w_u(i+1,j  ))
                w2 = vlmsk(i,j,2)*(vimsk(i,j,2)*w_u(i+1,j-1) + vimsk(i,j,4)*(1 - vimsk(i,j,2))*w_u(i+1,j  ) + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*w_u(i  ,j-1)    +   (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*w_u(i  ,j  ))
                w3 = vlmsk(i,j,3)*(vimsk(i,j,3)*w_u(i  ,j  ) + vimsk(i,j,1)*(1 - vimsk(i,j,3))*w_u(i  ,j-1) + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*w_u(i+1,j  )    +   (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*w_u(i+1,j-1))
                w4 = vlmsk(i,j,4)*(vimsk(i,j,4)*w_u(i+1,j  ) + vimsk(i,j,2)*(1 - vimsk(i,j,4))*w_u(i+1,j-1) + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*w_u(i  ,j  )    +   (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*w_u(i  ,j-1))

                ! calc remaining constants
                cv_3 = (u1 + u2 + u3 + u4)/4 - uocn_v(i,j)
                cv_4 = sqrt(cv_3**2 + cv_2**2)

                ! calc Dbw
                Dbw(nU_pnts + ij) = cv_1*cv_2*w_v(i,j)/cv_4 + (cv_1*cv_3/(4*cv_4))*(w1 + w2 + w3 + w4) 
            else
                ! Masking isn't required !

                ! calc remaining constants
                cv_3 = (ugrid(i,j) + ugrid(i+1,j) + ugrid(i+1,j-1) + ugrid(i,j-1))/4 - uocn_v(i,j)
                cv_4 = sqrt(cv_3**2 + cv_2**2)

                ! calc Dbw
                Dbw(nU_pnts + ij) = cv_1*cv_2*w_v(i,j)/cv_4 + (cv_1*cv_3/(4*cv_4))*(w_u(i,j-1) + w_u(i+1,j-1) + w_u(i,j) + w_u(i+1,j)) 
            endif  
        enddo
    end subroutine calc_Dbw_BDF1

!===========================================================================================!
!                                Jacobian Approximation                                     !
!===========================================================================================!
   
!===========================================================================================!
!                            Jacobian Approximation r02                                     !
!===========================================================================================!
    subroutine jacobian_multiplication_r02_BDF1(Jw, w, Au, time, ugrid, vgrid, &
                                        zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                        Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                        nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                        indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)
        !====================================================================
        ! This subroutine approximates the action of the jacobian on an 
        ! arbitrary vector w. NOTE: This routine uses the new matrix A formulation, 
        ! where we calculate the close forms of the viscosity derivatives.
        !
        ! The first order approximation is as follows:
        !
        !   Jw = (A(u + eps*w)u - A(u)u)/eps + A(u)w - Db(u)w
        !
        !   where Db(u) is the Jacobian Matrix of the vecotr b with respect 
        !   to u.
        !
        ! The second order approximation is:
        !
        !   Jw = (A(u + eps*w)u - A(u - eps*w)u)/2*eps + A(u)w - Db(u)w
        !
        !   Note:   Au_peps ==> A(u + eps*w)u
        !           Au_meps ==> A(u - eps*w)u
        !           Au      ==> A(u)u
        !           Aw      ==> A(u)w         
        !           Dbw     ==> Db(u)w
        !
        ! This subroutine only allocates the needed memory and uses the 2nd order 
        ! approximation if second_ord_Jac = .true. (see modelparams.f90)
        !
        !====================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nxN, nyN,   &                   ! indices limit - N points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                            ! current time value
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosities - u points
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! needed viscosity derivatives - u points
            P_u, h_u, Cw_u,                 &   ! ice strength, thickness, and water drag coefficient - u points       
            uocn_u, vocn_u                      ! ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosities - v points
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! needed viscosity derivatives - v points 
            P_v, h_v, Cw_v,                 &   ! ice strength, thickness, and water drag coefficient - v points
            uocn_v, vocn_v                      ! ocean velocities at v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(in) :: &
            Au                              ! holds the current matrix vector product of A and u
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(in) :: &
            w                               ! the arbitrary vector

        !==================!
        ! ---- In/out  ----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            Jw                  ! holds the resulting matrix vector product of the jacobian and w

        !==================!
        ! ----- Local  ----!
        !==================!
        integer(kind = int_kind) :: i, j, ij 

        !=================!
        ! Required Arrays !
        !=================!
            real(kind = dbl_kind), dimension(nU_pnts + nV_pnts) :: &
                Aw,         &   ! A(u)w
                Au_peps,    &   ! A(u + eps*w)u
                Dbw             ! Db(u)w
            real(kind = dbl_kind), dimension(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
                ugrid_peps, &   ! (u + eps*w) at U points 
                w_u             ! the portion of w that modifies u points
            real(kind = dbl_kind), dimension(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
                vgrid_peps, &   ! (u + eps*w) at V points 
                w_v             ! the portion of w that modifies v points
            real(kind = dbl_kind), dimension (nxU, nyU) :: &
                zeta_u_peps, eta_u_peps,                        &   ! u point viscosities calc'd with (u + eps*w)
                zeta_gx_u_peps, eta_gx_u_peps, eta_gy_u_peps,   &   ! u point viscosity derivatives calc'd with (u + eps*w)
                Cw_u_peps                                           ! u point NL water drag coef calc'd with (u + eps*w)
            real(kind = dbl_kind), dimension (nxV, nyV) :: &
                zeta_v_peps, eta_v_peps,                        &   ! v point viscosities calc'd with (u + eps*w)
                zeta_gy_v_peps, eta_gx_v_peps, eta_gy_v_peps,   &   ! v point viscosity derivatives calc'd with (u + eps*w)
                Cw_v_peps                                           ! v point NL water drag coef calc'd with (u + eps*w)

        !=============================!
        ! Arrays for 2nd order approx !
        !=============================!
            real(kind = dbl_kind), allocatable, dimension(:) :: &
                Au_meps         ! A(u - eps*w)u
            real(kind = dbl_kind), allocatable, dimension(:,:) :: &
                ugrid_meps, vgrid_meps,                         &   ! (u - eps*w) 
                zeta_u_meps, eta_u_meps,                        &   ! u point viscosities calc'd with (u - eps*w)
                zeta_gx_u_meps, eta_gx_u_meps, eta_gy_u_meps,   &   ! u point viscosity derivatives calc'd with (u - eps*w)
                zeta_v_meps, eta_v_meps,                        &   ! v point viscosities calc'd with (u - eps*w)
                zeta_gy_v_meps, eta_gx_v_meps, eta_gy_v_meps,   &   ! v point viscosity derivatives calc'd with (u - eps*w)
                Cw_u_meps, Cw_v_meps                                ! NL water drag coefs calc'd with (u - eps*w)

        ! Initialize Arrays !
        Aw              = 0.
        Au_peps         = 0.
        Dbw             = 0.
        ugrid_peps      = 0.
        vgrid_peps      = 0.
        w_u             = 0.
        w_v             = 0.
        zeta_u_peps     = 0.
        zeta_v_peps     = 0.
        eta_u_peps      = 0.
        eta_v_peps      = 0.
        zeta_gx_u_peps  = 0.
        zeta_gy_v_peps  = 0.
        eta_gx_u_peps   = 0.
        eta_gx_v_peps   = 0.
        eta_gy_u_peps   = 0.
        eta_gy_v_peps   = 0.
        Cw_u_peps       = 0.
        Cw_v_peps       = 0.

        !==========================================!
        ! Calc (u + eps*w) and place w on the grid !
        !==========================================!
            ! U-points !
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ugrid_peps(i,j)     = ugrid(i,j) + eps*w(ij)
                w_u(i,j)            = w(ij)
            enddo

            ! V-points !
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vgrid_peps(i,j)     = vgrid(i,j) + eps*w(nU_pnts + ij)
                w_v(i,j)            = w(nU_pnts + ij)
            enddo

        !=========!
        ! Calc Aw ! 
        !=========!
            call calc_Au_homogenous_BDF1(Aw, w_u, w_v, time, zeta_u, zeta_v, eta_u, eta_v, &
                                    zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk)
        !==========!
        ! Calc Dbw !
        !==========!
            call calc_Dbw_BDF1(Dbw, w, time, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk)
        !==============!
        ! Calc Au_peps !
        !==============!
            call calc_visc_and_viscgrads(zeta_u_peps, zeta_v_peps, zeta_gx_u_peps, zeta_gy_v_peps, eta_u_peps, eta_v_peps, &
                                        eta_gx_u_peps, eta_gx_v_peps, eta_gy_u_peps, eta_gy_v_peps, P_u, P_v, P_T, P_N, ugrid_peps, vgrid_peps, time, &
                                        vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

            call calc_waterdrag_coef(Cw_u_peps, Cw_v_peps, ugrid_peps, vgrid_peps, uocn_u, vocn_u, uocn_v, vocn_v, &
                                    time, ulmsk, uimsk, vlmsk, vimsk, &
                                    nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                    indxUi, indxUj, indxVi, indxVj)
        
            call calc_Au_r02_BDF1(Au_peps, ugrid, vgrid, time, zeta_u_peps, zeta_v_peps, eta_u_peps, eta_v_peps, &
                            zeta_gx_u_peps, zeta_gy_v_peps, eta_gx_u_peps, eta_gx_v_peps, eta_gy_u_peps, eta_gy_v_peps, Cw_u_peps, Cw_v_peps, h_u, h_v, &
                            nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                            ulmsk, uimsk, vlmsk, vimsk)

        !===============================!
        ! Check for second order approx !
        !===============================!
            if (second_ord_Jac) then 
                !================================!
                ! Allocate and initialize arrays !
                !================================!
                    allocate(Au_meps(nU_pnts + nV_pnts), & 
                             ugrid_meps(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), vgrid_meps(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), &
                             zeta_u_meps(nxU, nyU), eta_u_meps(nxU, nyU), zeta_gx_u_meps(nxU, nyU), &
                             eta_gx_u_meps(nxU, nyU), eta_gy_u_meps(nxU, nyU), Cw_u_meps(nxU, nyU), &
                             zeta_v_meps(nxV, nyV), eta_v_meps(nxV, nyV), zeta_gy_v_meps(nxV, nyV), &
                             eta_gx_v_meps(nxV, nyV), eta_gy_v_meps(nxV, nyV), Cw_v_meps(nxV, nyV))

                    Au_meps = 0.; ugrid_meps = 0.; vgrid_meps = 0.; 
                    zeta_u_meps = 0.; eta_u_meps = 0.; zeta_gx_u_meps = 0.;
                    eta_gx_u_meps = 0.; eta_gy_u_meps = 0.; Cw_u_meps = 0.;
                    zeta_v_meps = 0.; eta_v_meps = 0.; zeta_gy_v_meps = 0.;
                    eta_gx_v_meps = 0.; eta_gy_v_meps = 0.; Cw_v_meps = 0.

                !================!
                ! Calc u - eps*w !
                !================!
                    ! U points !
                    do ij = 1, nU_pnts
                        i = indxUi(ij)
                        j = indxUj(ij)

                        ugrid_meps(i,j) = ugrid(i,j) - eps*w_u(i,j)
                    end do 
                    ! V points !
                    do ij = 1, nV_pnts
                        i = indxVi(ij)
                        j = indxVj(ij)

                        vgrid_meps(i,j) = vgrid(i,j) - eps*w_v(i,j)
                    end do

                !==============!
                ! Calc Au_meps !
                !==============!
                    call calc_visc_and_viscgrads(zeta_u_meps, zeta_v_meps, zeta_gx_u_meps, zeta_gy_v_meps, eta_u_meps, eta_v_meps, &
                                            eta_gx_u_meps, eta_gx_v_meps, eta_gy_u_meps, eta_gy_v_meps, P_u, P_v, P_T, P_N, ugrid_meps, vgrid_meps, time, &
                                            vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                            nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                    call calc_waterdrag_coef(Cw_u_meps, Cw_v_meps, ugrid_meps, vgrid_meps, uocn_u, vocn_u, uocn_v, vocn_v, &
                                            time, ulmsk, uimsk, vlmsk, vimsk, &
                                            nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                            indxUi, indxUj, indxVi, indxVj)
                
                    call calc_Au_r02_BDF1(Au_meps, ugrid, vgrid, time, zeta_u_meps, zeta_v_meps, eta_u_meps, eta_v_meps, &
                                    zeta_gx_u_meps, zeta_gy_v_meps, eta_gx_u_meps, eta_gx_v_meps, eta_gy_u_meps, eta_gy_v_meps, Cw_u_meps, Cw_v_meps, h_u, h_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk)
                !=========!
                ! Calc Jw !
                !=========!
                    Jw = (Au_peps - Au_meps)/(2*eps) + Aw - Dbw
            else
                !=========!
                ! Calc Jw !
                !=========!
                    Jw = (Au_peps - Au)/eps + Aw - Dbw
            end if       

    end subroutine jacobian_multiplication_r02_BDF1

!===========================================================================================!
!                                   GMRES Solver                                            !
!===========================================================================================!
    
!===========================================================================================!
!                                   GMRES Solver r02                                        !
!===========================================================================================!
    subroutine gmres_solve_r02_BDF1(mat_type, iter, u_update, RHS, RHS_norm, tol, size, tmp_size, &
                                time, Au, ugrid, vgrid, P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, &
                                zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v,  &
                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)
    
        !====================================================================
        ! This subroutine calculates the u_update using intel's fgmres
        ! routine. The primary function of this routine is the solve 
        !
        !                       J(u)*u_update = -res_NL,
        !
        ! where J(u) is the jacobian of the system matrix A, w.r.t. u.
        !
        ! But this also has the ability to solve linear systems involving two
        ! other matrix types; this functionality was added to aid in the validation
        ! of the solver. 
        !
        ! For the primary function, this routine should be called with 
        !      
        !                   mat_type = 'Jx'
        !
        ! NOTE: this routine uses the updated formulation of the rheology terms for
        !       the system matrix A(u) and the jacobian, J(u).
        !
        !====================================================================


        !==================!
        ! ----- In --------!
        !==================!
        character(len = 2), intent(in) :: &
            mat_type                        ! identifier of the matrix to be used for the matrix vector multiplication 

        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nxN, nyN,   &                   ! indices limit - N points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts,    &                   ! number of V points in the comp. domain
            size,       &                   ! nU_pnts + nV_pnts
            tmp_size                        ! size of tmp array (defined for gmres routine)
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            tol,            &               ! defines the tolerance of the solver
            RHS_norm,       &               ! holds the 2 norm of RHS vector
            time
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosities - u points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! needed viscosity derivatives - u points
            P_u, h_u, Cw_u,                 &   ! ice strength, thickness and water drag coefficient - u points       
            uocn_u, vocn_u                      ! ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosities - v points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! needed viscosity derivatives - v points
            P_v, h_v, Cw_v,                 &   !ice strength, thickness and water drag coefficient - v points
            uocn_v, vocn_v                      ! ocean velocities at v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(size), intent(in) :: &
            Au,             &               ! current matrix vector product of A and u
            RHS                             ! generally will be -res_NL

        !==================!
        ! ----- In/out ----!
        !==================!
        real(kind = dbl_kind), dimension(size), intent(inout) :: &
            u_update                        ! velocity update vector
        integer(kind = int_kind), intent(inout) :: &
            iter
            
        !==================!
        ! ----- Local  ----!
        !==================!
        integer(kind = int_kind) :: &
            rci_request,            &       ! used in the RCI interface of the gmres routine (essentially it tells the code what operations to do)
            itercount                       ! will hold the number of iterations of the gmres routine 
        integer(kind = int_kind), dimension(128) :: &
            ipar                            ! integer parameters of the gmres routine 

        real(kind = dbl_kind), dimension(128) :: & 
            dpar                            ! double parameters of the gmres routine
        real(kind = dbl_kind), allocatable, dimension(:) :: &
            tmp                             ! temporary storage for vectors produced in the gmres routine 

        ! allocate tmp array !
        allocate(tmp(tmp_size))

        ! Initialize initial guess to zero !
        u_update = 0.

        ! Initialize GMRES Solver !
        call dfgmres_init(size, u_update, RHS, rci_request, ipar, dpar, tmp)
            ! check for error !
            if (rci_request /= 0) then 
                print *, 'Error in the GMRES routine; process aborted.'
                stop
            endif

            ! set integer parameters !
            ! ipar(5)     = gmres_iter_max
            ipar(5)     = size
            ipar(9)     = 1                             ! turns on the residual stopping test                      
            ipar(10)    = 0                             ! turns off user defined stopping test
            if (lin_precond == .true.) ipar(11) = 1     ! turns on preconditioning (lin_precond is in the model parameters module)
            ipar(12)    = 1
            ipar(15)    = gmres_rest                    ! specifies the number of non-restarted gmres iterations

            ! set double precision parameters !
            dpar(1)     = 0.                            ! force the gmres routine to only consider our tolerance
            dpar(2)     = tol*RHS_norm*x_extent/dx   ! specify the absolute tolerance (according to Lemieux 2012)

            ! print *, "****************************************************************"
            ! print *, "GMRES absolute tolerance set to:", dpar(2)
            ! print *, ""

        ! Check for errors !
        call dfgmres_check(size, u_update, RHS, rci_request, ipar, dpar, tmp)
            ! check for error !
            if (rci_request /= 0) then
                print *, 'Error in the GMRES routine; process aborted.'
                stop
            endif

        !=========== Main GMRES Step ==============!
        do
            call dfgmres(size, u_update, RHS, rci_request, ipar, dpar, tmp)
            ! print*, rci_request
            ! print*, dpar(5)
            ! print*, ipar(14)

                if (rci_request == 0) then
                    ! Solution found
                    ! print *, ""
                    ! print *, "**** Stopping Test Passed ****"
                    ! print *, "GMRES final residual:", dpar(5)
                    ! print *, ""
                    exit 
                else if (rci_request == 1) then

                    if (mat_type == 'Jx') then 

                        ! perform Jacobian multiplication !
                        call jacobian_multiplication_r02_BDF1(tmp(ipar(23)), tmp(ipar(22)), Au, time, ugrid, vgrid, &
                                                        zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, &
                                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                                        Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                        nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                                        indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                    else if (mat_type == 'Ax') then 

                        ! perform system matrix multiplication !
                        call calc_Au_r02_BDF1(tmp(ipar(23)), ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                    zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk, tmp(ipar(22)))

                    else if (mat_type == 'Db') then

                        ! perform the matrix mult with the Db matrix !
                        call calc_Dbw_BDF1(tmp(ipar(23)), tmp(ipar(22)), time, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk)

                    end if 

                else if (rci_request == 3) then
                    ! perform the preconditioning !
                    ! This will be implemented once the non-preconditioned solver is working
                else
                    print *, '******************** Warning: GMRES routine failed to converge. ************************************'
                    print *, 'This likely is the maximum iteration count being reached'
                    print *, 'Final Residual = ', dpar(5)
                    exit 
                end if
        enddo

        !============ Populate Solution Vector ======!
        ! Note: mkl_free_buffers frees up unused memory
        call dfgmres_get(size, u_update, RHS, rci_request, ipar, dpar, tmp, itercount)
        call mkl_free_buffers 
        ! print *, 'Linear Solver Terminated in ', itercount, 'iterations.'
        ! print *, "****************************************************************"

        ! set iteration count !
        iter = itercount

    end subroutine gmres_solve_r02_BDF1

!===========================================================================================!
!                                   Non-Linear Solver                                       !
!===========================================================================================!
    
!===========================================================================================!
!                                   Non-Linear Solver r02                                   !
!===========================================================================================!
    subroutine JFNKsolve_r02_BDF1(ugrid, vgrid, time, Au, b, b_0, u_update, res_NL, size, tmp_size, &
                            zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, & 
                            P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                            nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                            indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk, iter_count, res_Final) 
    
        !============================================================================
        ! This subroutine advances ugrid and vgrid to the next time step through the JFNK 
        ! solver. 
        !
        ! NOTE: this routine uses the new formulation of the rheology term in gmres_solve_r02
        !       and the A(u)u routine.
        ! 
        !============================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nxN, nyN,   &                   ! indices limit - N points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts,    &                   ! number of V points in the comp. domain
            size,       &                   ! nU_pnts + nV_pnts
            tmp_size                        ! defines the size of the gmres_tmp array
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                            ! current time value
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            P_u, h_u, uocn_u, vocn_u             ! ice strength, thickness and ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            P_v, h_v, uocn_v, vocn_v             ! ice strength, thickness and ocean velocities at v points
        real(kind = dbl_kind), dimension (size), intent(in) :: &
            b_0                             ! contains the portion of b that isn't affected by the current velocity solution 

        !==================!
        ! ---- In/out  ----!
        !==================!
        integer(kind = int_kind), intent(inout) :: & 
            iter_count                      ! holds the number of non-linear iterations to reach convergence

        real(kind = dbl_kind), intent(inout) :: &
            res_Final                       ! holds the final residual at each time step
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(inout) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(inout) :: &
            vgrid                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(size), intent(inout) :: &
            Au,             &               ! current matrix vector product of A and u
            u_update,       &               ! contains the update values for ugrid and vgrid
            b,              &               ! right hand side of Au = b
            res_NL                          ! current non-linear residual
        real(kind = dbl_kind), dimension (nxU, nyU), intent(inout) :: &
            zeta_u, eta_u,                  &   ! viscosities - u points
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! needed viscosity derivatives - u points
            Cw_u                                ! water drag coefficient - u points   
        real(kind = dbl_kind), dimension (nxV, nyV), intent(inout) :: &
            zeta_v, eta_v,                  &   ! viscosities - v points
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! needed viscosity derivatives - v points 
            Cw_v                                ! water drag coefficient - v points

        !==================!
        ! ---- Local   ----!
        !==================!
        character(len = 8) :: &
            fmt1, fmt2, fmt3, fmt4, &       ! formats for saving files
            dx_f, dt_f, t_lvl,      &       ! strings for saving files 
            eps_str, nltolcoef_str, &   
            strng            

        integer(kind = int_kind) :: &
            i, j, ij, k, n, lin_it          ! local indices and counters

        integer(kind = int_kind), dimension(k_max) :: &
            Linear_Iterations               ! stores the number of iterations used at each linear solve
  
        real(kind = dbl_kind) :: &
            tau,                &           ! damping term for the Newton Iteration
            linear_tol,         &           ! tolerance for the linear solver
            disc_err_tol,       &           ! hold the lower limit tolerance for the non-linear residual. Defined according to the discretization error
            res_NL_norm,        &           ! norm of the current residual
            res_NL_norm_init,   &           ! norm of the initial residual
            res_NL_norm_prev                ! norm of the residual for iteration k - 2
        real(kind = dbl_kind), allocatable, dimension (:) :: &
            RHS                             ! holds the RHS for the linear solver
        real(kind = dbl_kind), dimension(k_max + 1) :: &
            Residual                        ! stores the residual at each step. Note: the extra entry is for the initial residual
        real(kind = dbl_kind), dimension(k_max) :: &
            damp_factors                    ! stores the damping factor used to progress to the next iteration
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
            ugrid_prev                      ! stores the previous u soln
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
            vgrid_prev                      ! stores the previous v soln 

        fmt1 = '(I4.4)'
        fmt2 = '(I8.8)'
        fmt3 = '(ES6.0)'
        fmt4 = '(F4.2)'
        write (dx_f, fmt1) int(dx/1000)                 ! turn dx into a string 
        write (dt_f, fmt1) int(dt)                      ! turn dt into a string
        write (t_lvl, fmt2) int(time)                   ! turn time into a string for file naming
        write (eps_str, fmt3) eps                       ! turn epsilon value in a string
        write (nltolcoef_str, fmt3) disctol_coef_nl     ! turn disctol_coef_nl into a string 
        write (strng, fmt4) ResInc_tol 

        ! allocate the RHS !
        allocate(RHS(size))

        ! calculate discretization error cut off !
        disc_err_tol = (rho*h_init*f*typ_vel)*disctol_coef_nl*(dx**2)/(x_extent**2)                 ! Matching the coriolis term

        ! calculate norm of inital Nonlinear residual !
        call calc_nrm2(res_NL_norm_init, res_NL, size)

        ! initialize variables !
        tau                 = tau_init
        k                   = 0
        res_NL_norm         = res_NL_norm_init
        res_NL_norm_prev    = res_NL_norm_init 
        ugrid_prev          = 0.
        vgrid_prev          = 0.

        ! Main loop !
        ML: do  
            !===================================================================!
            ! Adaptive Damping - if Adaptive_Damp = .true. (in modelparams.f90) !
            !===================================================================!
                if (Adaptive_Damp .and. (k > 0)) then
                    if (res_NL_norm >= res_NL_norm_prev) then 

                        ! Repeat until a decrease is noted !
                        DecrL: do 

                            ! Check for Tau limit !
                                if (tau >= tau_lim) then 
                                    print *, "Unable to decrease residual further past iteration ", k-1
                                    print *, "Maximum tau reached. Solver terminating."
                                    print *, "Final Residual = ", res_NL_norm_prev

                                    iter_count  = k - 1
                                    res_Final   = res_NL_norm_prev

                                    ! Reset solution !
                                        ugrid       = ugrid_prev
                                        vgrid       = vgrid_prev
                                        res_NL_norm = res_NL_norm_prev

                                    ! calc drag coefs and visc coefs ! - I decided to re-calculate these instead of storing to limit storage issues
                                        call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                                                vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                                                nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                                        call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                                time, ulmsk, uimsk, vlmsk, vimsk, &
                                                                nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                                                indxUi, indxUj, indxVi, indxVj)

                                        call calc_Au_r02_BDF1(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                                zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                                nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                                ulmsk, uimsk, vlmsk, vimsk)

                                        call calc_b_BDF1(b, b_0, Cw_u, Cw_v, &
                                                    uocn_u, vocn_u, uocn_v, vocn_v, &
                                                    nxU, nyU, nxV, nyV, &
                                                    nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                                        res_NL = Au - b

                                    if (mod(time, sv_interval) == 0) then
                                        open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                        call csv_write_dble_1d(1, Residual(1:k))
                                        close(1)

                                        ! if k > 0, save linear iteration counts !
                                        if (k > 0) then 
                                            open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                            call csv_write_integer_1d(1, Linear_Iterations(1:k-1))
                                            close(1)

                                            open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                            call csv_write_dble_1d(1, damp_factors(1:k-1))
                                            close(1)
                                        end if
                                    end if

                                    Exit ML
                                end if 

                            ! print *, "****** Increase in residual at iteration ", k, "! ******"
                            ! print *, "Damping factor increased from ", tau, "to ", tau*tau_incfact

                            ! Increase in residual noted, try update with increased tau !
                                tau = tau*tau_incfact

                            ! Update ugrid and vgrid again !
                                do ij = 1,nU_pnts
                                    i = indxUi(ij)
                                    j = indxUj(ij)

                                    ugrid(i,j) = ugrid_prev(i,j) + (1/tau)*u_update(ij)
                                end do 

                                do ij = 1,nV_pnts
                                    i = indxVi(ij)
                                    j = indxVj(ij)

                                    vgrid(i,j) = vgrid_prev(i,j) + (1/tau)*u_update(nU_pnts + ij)
                                end do

                            ! Re-calculate residual !
                                call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                                        eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                                        vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                                call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                        time, ulmsk, uimsk, vlmsk, vimsk, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                                        indxUi, indxUj, indxVi, indxVj)

                                call calc_Au_r02_BDF1(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                        ulmsk, uimsk, vlmsk, vimsk)

                                call calc_b_BDF1(b, b_0, Cw_u, Cw_v, &
                                            uocn_u, vocn_u, uocn_v, vocn_v, &
                                            nxU, nyU, nxV, nyV, &
                                            nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                                res_NL = Au - b

                                call calc_nrm2(res_NL_norm, res_NL, size)

                            ! Check for decrease and exit do loop if noted !
                                if (res_NL_norm < res_NL_norm_prev) then 
                                    ! print *, "Decrease achieved with tau =", tau 
                                    damp_factors(k) = tau
                                    EXIT DecrL
                                end if 
                        end do DecrL

                    else if ((res_NL_norm >= dectau_tol*res_NL_norm_prev) .and. (tau > 1.d0)) then 
                        ! print *, "****** Small decrease in residual noted at iteration ", k, "! ******"
                        ! print *, "Damping factor decreased from ", tau, "to ", tau/tau_decfact 

                        ! store tau !
                            damp_factors(k) = tau

                        ! decrease tau !
                            tau = max(1.d0, tau/tau_decfact)
                    else 
                        ! store tau !
                        damp_factors(k) = tau 
                    end if

                    ! Check for plateau !
                    if (AD_plateau_check .and. (res_NL_norm >= AD_plat_tol*res_NL_norm_prev) .and. (k >= platinc_check)) then
                        print *, "Solver plateaued at iteration", k 
                        print *, "Solver terminating"
                        print *, "Final Residual = ", res_NL_norm

                        ! store values !
                            Residual(k + 1) = res_NL_norm
                            iter_count      = k 
                            res_Final       = res_NL_norm

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            open(unit = 2, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            call csv_write_integer_1d(2, Linear_Iterations(1:k))
                            close(1)
                            close(2)

                            if (Adaptive_Damp) then 
                                open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_dble_1d(1, damp_factors(1:k))
                                close(1)
                            end if 
                        end if 
                        
                        Exit ML
                    end if 
                end if 

            !===================================!
            ! set the RHS for the linear solver !
            !===================================!
                RHS = -1*res_NL

            !========================!
            ! Store norm of Residual !
            !========================! 
                Residual(k + 1) = res_NL_norm

            !=========================!
            ! Check stopping criteria !
            !=========================!

                !====================!
                ! Iteration Criteria !
                !====================!
                    if (k == k_max) then

                        iter_count  = k
                        res_Final   = res_NL_norm

                        print *, "Maximum iteration count reached, solver terminated."
                        print *, "Limit = ", k_max, "iterations"
                        print *, "Final Residual = ", res_NL_norm
                        print *, ""

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            open(unit = 2, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            call csv_write_integer_1d(2, Linear_Iterations(1:k))
                            close(1)
                            close(2)

                            if (Adaptive_Damp) then 
                                open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_dble_1d(1, damp_factors(1:k))
                                close(1)
                            end if 
                        end if 
                        
                        Exit ML
                    end if

                !===================!
                ! Residual Criteria !
                !===================!
                ! Disc Cut-Off !
                    if (res_NL_norm < disc_err_tol) then    ! Check Discretization Error Cut-Off !

                        iter_count  = k
                        res_Final   = res_NL_norm

                        print *, 'Non-linear solver converged in ', k, 'iterations! Residual below discretization error.'
                        print *, ''
                        print *, 'Final Residual:', res_NL_norm

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            close(1)

                            ! if k > 0, save linear iteration counts !
                            if (k > 0) then 
                                open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_integer_1d(1, Linear_Iterations(1:k))
                                close(1)

                                if (Adaptive_Damp) then 
                                    open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                    call csv_write_dble_1d(1, damp_factors(1:k))
                                    close(1)
                                end if
                            end if
                        end if 

                        Exit ML
                        
                ! Plateau Check !
                    else if (plateau_check .and. (k >= platinc_check) .and. &
                         ((res_NL_norm > plat_tol*res_NL_norm_prev) .and. (res_NL_norm <= res_NL_norm_prev))) then !  if enough iterations have happened, check for plateau in residual 

                        iter_count  = k 
                        res_Final   = res_NL_norm 
                        print *, 'Non-linear solver plateaued at iteration', k,'! Solver Terminated.'
                        print *, 'Final Residual:', res_NL_norm 
                        print *, ""

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            close(1)

                            ! if k > 0, save linear iteration counts !
                            if (k > 0) then 
                                open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_integer_1d(1, Linear_Iterations(1:k))
                                close(1)
                            end if
                        end if 

                        Exit ML

                ! Conditional Termination !
                    else if (ResInc_Term .and. &
                        (k >= platinc_check) .and. (res_NL_norm > ResInc_tol*res_NL_norm_prev)) then ! Check for large residual increase !

                            iter_count  = k - 1
                            res_Final   = res_NL_norm_prev

                            print *, '****** Large residual increase at iteration ', k, '! Using previous solution! ******'
                            print *, ''
                            print *, 'Final Residual:', res_NL_norm_prev

                            !================!
                            ! reset solution !
                            !================!
                                ugrid       = ugrid_prev
                                vgrid       = vgrid_prev
                                res_NL_norm = res_NL_norm_prev

                                ! calc drag coefs and visc coefs ! - I decided to re-calculate these instead of storing to limit storage issues
                                call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                                        eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                                        vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                                call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                        time, ulmsk, uimsk, vlmsk, vimsk, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                                        indxUi, indxUj, indxVi, indxVj)

                                call calc_Au_r02_BDF1(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                        ulmsk, uimsk, vlmsk, vimsk)

                                call calc_b_BDF1(b, b_0, Cw_u, Cw_v, &
                                            uocn_u, vocn_u, uocn_v, vocn_v, &
                                            nxU, nyU, nxV, nyV, &
                                            nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                                res_NL = Au - b

                            ! Save !
                                if (mod(time, sv_interval) == 0) then
                                    open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                    call csv_write_dble_1d(1, Residual(1:k))
                                    close(1)

                                    ! if k > 0, save linear iteration counts !
                                    if (k > 0) then 
                                        open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                        call csv_write_integer_1d(1, Linear_Iterations(1:k-1))
                                        close(1)
                                    end if
                                end if

                            Exit ML
                    else 
                        ! Continue with NL iterations !
                    endif

            !======================!
            ! Set Linear Tolerance !
            !======================!
                if (res_NL_norm >= res_t) then ! 
                    linear_tol = gmres_tol_ini
                else
                    linear_tol = max(gmres_tol_min, min(res_NL_norm/res_NL_norm_prev, gmres_tol_ini))
                end if

            !=========================!
            ! Store Previous Solution !
            !=========================!
                ugrid_prev = ugrid 
                vgrid_prev = vgrid

            !==============!
            ! Linear Solve !
            !==============!
                call gmres_solve_r02_BDF1('Jx', lin_it, u_update, RHS, res_NL_norm, linear_tol, size, tmp_size, &
                                time, Au, ugrid, vgrid, P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, &
                                zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v,  &
                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                Linear_Iterations(k+1) = lin_it

            !===================!    
            ! Update velocities !
            !===================!
                ! U-points !
                do ij = 1, nU_pnts
                    ! get indices !
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ugrid(i,j)  = ugrid(i,j) + (1/tau)*u_update(ij)
                enddo

                ! V-points !
                do ij = 1, nV_pnts
                    ! get indices !
                    i = indxVi(ij)
                    j = indxVj(ij)

                    vgrid(i,j)  = vgrid(i,j) + (1/tau)*u_update(nU_pnts + ij)
                enddo

            !============================================!
            ! Update variables dependent on the velocity !
            !============================================!
                call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                            eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                            vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                            nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                        time, ulmsk, uimsk, vlmsk, vimsk, &
                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                        indxUi, indxUj, indxVi, indxVj)

            !==================================!
            ! Calculate and store new residual !
            !==================================!
                call calc_Au_r02_BDF1(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                            zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                            nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                            ulmsk, uimsk, vlmsk, vimsk)

                call calc_b_BDF1(b, b_0, Cw_u, Cw_v, &
                            uocn_u, vocn_u, uocn_v, vocn_v, &
                            nxU, nyU, nxV, nyV, &
                            nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                res_NL = Au - b 

                res_NL_norm_prev = res_NL_norm
                call calc_nrm2(res_NL_norm, res_NL, size)

            !========================!
            ! Update Iteration Count !
            !========================!
                k = k + 1

        end do ML
    end subroutine JFNKsolve_r02_BDF1
    
    
                                             !===============!
!============================================!  End of BDF1  !===============================================!
                                             !===============!
    
                                             !=================!
!============================================!  Start of BDF2  !===============================================!
                                             !=================!
                                             
                                             
!===========================================================================================!
!                           Calc b_0 and update forcing arrays r02                          !
!===========================================================================================!
    subroutine calc_b_0_update_forcing_r02(b_0, ugrid, vgrid, u1_BDF,u2_BDF,v1_BDF,v2_BDF, Cw_u, Cw_v, &
                                    uocn_u, vocn_u, uocn_v, vocn_v, uwnd_u, vwnd_u, uwnd_v, vwnd_v, &
                                    zeta_u, zeta_v, eta_u, eta_v, h_u, h_v, P_T, ulmsk, uimsk, vlmsk, vimsk, &
                                    zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                    nxU, nyU, nxV, nyV, nxT, nyT, nU_pnts, nV_pnts, & 
                                    indxUi, indxUj, indxVi, indxVj, &
                                    time)

        !===================================================================================
        ! NOTE: This is an updated version of the routine which uses the reformulation of the 
        !       rheology term, i.e. where we calculate the viscsity gradients in closed form 
        !       at U and V points
        !
        ! This subroutine calculates the portion of b that is not dependent on the 
        ! current solution. 
        !
        ! This portion of b includes terms resulting from the crank-nicolson scheme and 
        ! those from forcing terms that do not depend on the current solution.
        !
        ! Note 1:   in calculating the crank-nicolson component, numerical masks are used
        !           in order to create the necessary boundary conditions.
        !
        ! Note 2:   in calculating the forcing part of b_0, forcing is required from the 
        !           previous time step and the current one; as a result, this routine also
        !           updates the forcing arrays, uocn, vocn, uwnd, and vwnd 
        !
        ! Note 3:   like the Au routine, when checking for masking this routine only checks
        !           the first 8 masks as these are associated with the velocities needed for 
        !           this operation; the remaining 12 masks are used in the viscosity calculations.
        !===================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain 
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                                ! current time level
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                                 ! ice strength at T points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosity values - U points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! viscosity derivatives - U points 
            h_u, Cw_u                           ! ice thickness and non-linear water drag coefficient - u points         
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosity values - V points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! viscosity derivatives - V points
            h_v, Cw_v                           ! ice thickness and non-linear water drag coefficient - v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                               ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                               ! v velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            u1_BDF, u2_BDF                               ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            v1_BDF, v2_BDF                               ! v velocity on the grid    

        !==================!
        ! ----- In/Out ----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            b_0                             ! linear portion of the b vector
        real(kind = dbl_kind), dimension (nxU, nyU), intent(inout) :: &
            uocn_u, vocn_u, uwnd_u, vwnd_u  ! ocean and wind velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(inout) :: &
            uocn_v, vocn_v, uwnd_v, vwnd_v  ! ocean and wind velocities at v points

        !==================!
        ! ----- Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i,j, ij 

        real(kind = dbl_kind) :: &      
            cnst_1, cnst_2, cnst_3, cnst_4,                 &   ! constants implemented to aid in computation of b_0
            cnst_5, cnst_6, cnst_7, cnst_8,                 &
            cnst_9, cnst_10, cnst_11, cnst_12,              &
            c1, c2, c3, c4, c5, c6, c7,                     &   ! coefficients for the crank-nicolson portion of b_0; implemented to clean up code
            c8, c9,                                         &
            f1, f2, f3, f4, f5, f6,                         &   ! individual forcing components; implemented to clean up code and aid in routine validation
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p1m1, u_p0p0, &   ! masked velocities
            v_m1p0, v_p0p0, v_m1p1, v_p0p1, v_p0m1, v_p1p0, &
            halfdx

            !============================!
            ! Define necessary constants !
            !============================!
                halfdx = dx/2
                
                cnst_3 = rho*f 
                cnst_4 = rho/dt 
                cnst_5 = rho_a*Cd_a
                cnst_6 = rho*f
                cnst_7 = 1./(2*dx)

            !===============================================================================!
            ! Populate portion of b_0 - Crank-Nicolson and forcing from previous time level !
            !===============================================================================! 
                !===============================!
                !   U-component of the SIME     !
                !===============================!
                do ij = 1,nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    
                    ! calculate Crank-Nicolson coefficients !
                    c3 = -2*cnst_4*h_u(i,j)                                                                       ! u(i  ,j  ) !
                    c4 = (1/2)*cnst_4*h_u(i,j)                                                                       ! u(i  ,j  ) !
                    ! calculate forcing components for previous time level !
                    f1 = -cnst_5*sqrt(uwnd_u(i,j)**2 + vwnd_u(i,j)**2)*(uwnd_u(i,j)*cos_a - vwnd_u(i,j)*sin_a)   ! air drag
                  !  f2 = -(Cw_u(i,j)/2)*(uocn_u(i,j)*cos_w - vocn_u(i,j)*sin_w)                                 ! water drag 
                    f3 = cnst_6*h_u(i,j)*vocn_u(i,j)                                                             ! sea surface tilt
                    f4 = cnst_7*(P_T(i,j) - P_T(i-1,j))                                                          ! ice strength gradient
                                                 
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        !============================================================!
                        ! Use masked velocities as at least one of the first 8 masks !
                        ! are activated.                                             !
                        !============================================================! 
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time - dt)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time - dt))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time - dt)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time - dt)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time - dt))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time - dt)

                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time - dt)                           

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time - dt)                             

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time - dt))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time - dt)                         

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time - dt)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt))) &
                                    +   (1- ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time - dt)   

                        !===================================!
                        ! Populate the vector using masking !
                        !===================================!
                        b_0(ij) =    c3*u2_BDF(i,j) + c4*u1_BDF(i,j)   &                                
                                     +   f1 + f3 + f4
                    else
                        !=============================================================!
                        ! Masking isn't required. Populate the vector without masking !
                        !=============================================================!
                        b_0(ij) =  c3*u2_BDF(i,j) + c4*u1_BDF(i,j)                                                &
                                     +   f1 + f3 + f4
                    end if
                end do 

                !===============================!
                !   V-component of the SIME     !
                !===============================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    c7 = -2*cnst_4*h_v(i,j)                                                                          ! u^n(i  ,j  ) 
                    c8= (1/2)*cnst_4*h_v(i,j)                                                                        ! v^n-1(i  ,j  ) 

                  
                                                                                           
                   
                    ! calculate forcing components from the previous time leve !
                    f1 = -cnst_5*sqrt(uwnd_v(i,j)**2 + vwnd_v(i,j)**2)*(vwnd_v(i,j)*cos_a + uwnd_v(i,j)*sin_a)  ! air drag
                  !  f2 = -(Cw_v(i,j)/2)*(vocn_v(i,j)*cos_w + uocn_v(i,j)*sin_w)                                 ! water drag
                    f3 = -cnst_6*h_v(i,j)*uocn_v(i,j)                                                           ! sea surface tilt
                    f4 = cnst_7*(P_T(i,j) - P_T(i,j-1))                                                         ! ice strength gradient

                    if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        !============================================================!
                        ! Use masked velocities as at least one of the first 8 masks !
                        ! are activated.                                             !
                        !============================================================! 
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time - dt)                         
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time - dt)                       
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time - dt))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time - dt)                           
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time - dt)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time - dt) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time - dt)))  &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time - dt)                           

                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time - dt))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time - dt)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time - dt))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time - dt)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time - dt))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time - dt)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time - dt))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time - dt) 

                        b_0(nU_pnts + ij)   =    c7*v2_BDF(i,j) +c8*v1_BDF(i,j)                           &
                                                +   f1 + f3 + f4 
                    else
                        !========================!
                        ! Masking isn't required !
                        !========================!
                        b_0(nU_pnts + ij)   =    c7*v2_BDF(i,j) +c8*v1_BDF(i,j)                         &
                                                 +   f1 + f3 + f4
                    end if
                end do

            !======================================!
            ! Update forcing to current time level !
            !======================================!
                call get_ocn_forcing(uocn_u, uocn_v, vocn_u, vocn_v, time, &          ! located in forcing module
                            nxT, nyT, nxU, nyU, nxV, nyV)
                call get_wnd_forcing(uwnd_u, uwnd_v, vwnd_u, vwnd_v, time, &          ! located in forcing module
                                nxU, nyU, nxV, nyV)

           
    end subroutine calc_b_0_update_forcing_r02
                                             
                                            

!===========================================================================================!
!                                          Calc b                                           !
!===========================================================================================!
    subroutine calc_b(b, b_0, Cw_u, Cw_v, &
                    uocn_u, vocn_u, uocn_v, vocn_v, &
                    nxU, nyU, nxV, nyV, &
                    nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)
        !===================================================================================
        ! This subroutine calculates b by taking adding b_0 and the component of forcing
        ! that depends on the current sea ice velocity (ocean forcing). These two parts of b
        ! were split for computational purposes. When a new non-linear iterate is calculated,
        ! this split allows us to update b by modifying the ocean forcing term and leave
        ! b_0 unchanged; limiting our computations.
        !===================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            uocn_u, vocn_u, Cw_u            ! ocean velocities and non-linear water drag coef. at U points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            uocn_v, vocn_v, Cw_v            ! ocean velocities and non-linear water drag coef. at V points
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(in) :: &
            b_0

        !==================!
        ! ----- Inout -----!
        !==================!
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(inout) :: &
            b

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i,j, ij
        real(kind = dbl_kind) :: &
            f7                              ! holds the updated water drag forcing - called f7 to match the naming convention from the calc_b_0_update_forcing routine

        !====== U - points ======!
        do ij = 1, nU_pnts
            ! get indices
            i = indxUi(ij)
            j = indxUj(ij)

            ! new water drag forcing
            f7 = -(Cw_u(i,j))*(uocn_u(i,j)*cos_w - vocn_u(i,j)*sin_w)
            
            ! calculate b
            b(ij) = b_0(ij) + f7

        enddo

        !====== V - points ======!
        do ij = 1, nV_pnts
            ! get indices
            i = indxVi(ij)
            j = indxVj(ij)

            ! new water drag forcing
            f7 = -(Cw_v(i,j))*(vocn_v(i,j)*cos_w + uocn_v(i,j)*sin_w)
            
            ! calculate b
            b(nU_pnts + ij) = b_0(nU_pnts + ij) + f7

        enddo
    end subroutine calc_b
!===========================================================================================!
!                                   Calc Au - r02                                           !
!===========================================================================================!
    subroutine calc_Au_r02(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk, u_vect)

        !========================================================================================
        ! NOTE: This is the updated version of the routine where the reformulation of the rheology 
        !       term is used; i.e. we calculate the derivatives of the viscosities in closed form.
        !
        ! This subroutine calculates the matrix vector product, Au, where A is our linearized 
        ! coef matrix and u is the velocity vector. When checking for masking, this routine only 
        ! looks at the first 8 masks; the other 12 are only needed in the viscosity calculations.
        !
        ! Note that this routine is also used in the calculation of the jacobian approximation.
        !
        !========================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                                ! current time value
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosity values - U points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! viscosity derivatives - U points 
            h_u, Cw_u                           ! ice thickness and non-linear water drag coefficient - U points      
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosity values - V points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! viscosity derivatives - V points
            h_v, Cw_v                           ! ice thickness and non-linear water drag coefficient - V points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                               ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                               ! v velocity on the grid
        real(kind = dbl_kind), optional, dimension(nU_pnts + nV_pnts), intent (in) :: &
            u_vect                              ! velocity in vector form, this is only used for gmres validation 

        !==================!
        ! ----- Inout------!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            Au 
        
        !==================!
        ! ----- Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij
        real(kind = dbl_kind), allocatable, dimension(:,:) :: &
            ugrid_vect, vgrid_vect              ! if u_vect is present these will hold its values on the grid 
        real(kind = dbl_kind) :: &
            cnst_1, cnst_2, cnst_3, cnst_4,                 &   ! arbitrary constants 
            cnst_5, cnst_6, cnst_7, cnst_8, cnst_9,         &   ! constants used in calculation of matrix coefficients
            c1, c2, c3, c4, c5,                             &   ! coefficients for the crank-nicolson portion of Au; implemented to clean up code
            c6, c7, c8, c9,                                 &   
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p1m1, u_p0p0, &   ! masked velocities
            v_m1p0, v_p0p0, v_m1p1, v_p0p1, v_p0m1, v_p1p0, &
            halfdx 

        !===============================!
        !   Define Necessary Constants  !
        !===============================!
            halfdx = dx/2
            cnst_1 = 1./(dx*dx)
            cnst_2 = 1./(2*dx)
            cnst_3 = rho*f
            cnst_4 = rho/dt

        !==============================================!
        ! Perform Mat-Vec Using the gridded velocities !
        !==============================================!
        if( .not. present(u_vect)) then 

            !===============================!
            !   U-component of the SIME     !
            !===============================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (cnst_3*h_u(i,j) + Cw_u(i,j)*sin_w)/4
                    cnst_6 = eta_gx_u(i,j) + zeta_gx_u(i,j)
                    cnst_7 = eta_u(i,j) + zeta_u(i,j)
                    cnst_8 = zeta_gx_u(i,j) - eta_gx_u(i,j) + eta_gy_u(i,j)
                    cnst_9 = zeta_gx_u(i,j) - eta_gx_u(i,j) - eta_gy_u(i,j)

                    ! calculate coefficients !
                    c1 = -cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                          ! u(i  ,j-1) coef !
                    c2 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! u(i-1,j  ) coef !
                    c3 = -(3/2)*cnst_4*h_u(i,j) - Cw_u(i,j)*cos_w - (2./(dx*dx))*(2*eta_u(i,j) + zeta_u(i,j))     ! u(i  ,j  ) coef !
                    c4 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! u(i+1,j  ) coef !
                    c5 = cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                           ! u(i  ,j+1) coef !
                    c6 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i-1,j  ) coef !
                    c7 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i  ,j  ) coef !
                    c8 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i-1,j+1) coef !
                    c9 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i  ,j+1) coef !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time)

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time) 

                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(ij)  =   c1*u_p0m1 + c2*u_m1p0 + c3*ugrid(i,j) + c4*u_p1p0 + c5*u_p0p1 &
                                    +   c6*v_m1p0 + c7*v_p0p0 + c8*v_m1p1 + c9*v_p0p1 
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(ij)  =   c1*ugrid(i  ,j-1) + c2*ugrid(i-1,j  ) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) + c5*ugrid(i  ,j+1) &
                                    +   c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i-1,j+1) + c9*vgrid(i  ,j+1)
                    end if 
                end do 

            !===============================!
            !   V-component of the SIME     !
            !===============================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (-cnst_3*h_v(i,j) - Cw_v(i,j)*sin_w)/4
                    cnst_6 = eta_gy_v(i,j) + zeta_gy_v(i,j)
                    cnst_7 = eta_v(i,j) + zeta_v(i,j)
                    cnst_8 = zeta_gy_v(i,j) - eta_gy_v(i,j) + eta_gx_v(i,j)
                    cnst_9 = zeta_gy_v(i,j) - eta_gy_v(i,j) - eta_gx_v(i,j)

                    ! calculate coefficients !
                    c1 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i  ,j-1) !
                    c2 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i+1,j-1) !
                    c3 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i  ,j  ) !
                    c4 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i+1,j  ) !
                    c5 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! v(i  ,j-1) !
                    c6 = -cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                          ! v(i-1,j  ) !
                    c7 = -(3/2)*cnst_4*h_v(i,j) - Cw_v(i,j)*cos_w - (2./(dx*dx))*(2*eta_v(i,j) + zeta_v(i,j))     ! v(i  ,j  ) !
                    c8 = cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                           ! v(i+1,j  ) !
                    c9 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! v(i  ,j+1) !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                     if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)                            

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time) 
                            
                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(nU_pnts + ij)    =   c1*u_p0m1 + c2*u_p1m1 + c3*u_p0p0 + c4*u_p1p0 &
                                                +   c5*v_p0m1 + c6*v_m1p0 + c7*vgrid(i,j) + c8*v_p1p0 + c9*v_p0p1
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(nU_pnts + ij)    =   c1*ugrid(i  ,j-1) + c2*ugrid(i+1,j-1) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) &
                                                +   c5*vgrid(i  ,j-1) + c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i+1,j  ) + c9*vgrid(i  ,j+1)
                    end if
                end do

        !=================================================!
        ! Perform Mat-Vec Using vector form of velocities !
        !=================================================!
        else
            allocate(ugrid_vect(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), vgrid_vect(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V))
            do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)
                    ugrid_vect(i,j) = u_vect(ij)
                end do
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)
                    vgrid_vect(i,j) = u_vect(ij + nU_pnts)
            end do

            !===============================!
            !   U-component of the SIME     !
            !===============================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (cnst_3*h_u(i,j) + Cw_u(i,j)*sin_w)/4
                    cnst_6 = eta_gx_u(i,j) + zeta_gx_u(i,j)
                    cnst_7 = eta_u(i,j) + zeta_u(i,j)
                    cnst_8 = zeta_gx_u(i,j) - eta_gx_u(i,j) + eta_gy_u(i,j)
                    cnst_9 = zeta_gx_u(i,j) - eta_gx_u(i,j) - eta_gy_u(i,j)

                    ! calculate coefficients !
                    c1 = -cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                          ! u(i  ,j-1) coef !
                    c2 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! u(i-1,j  ) coef !
                    c3 = -(3/2)*cnst_4*h_u(i,j) - Cw_u(i,j)*cos_w - (2./(dx*dx))*(2*eta_u(i,j) + zeta_u(i,j))     ! u(i  ,j  ) coef !
                    c4 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! u(i+1,j  ) coef !
                    c5 = cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                           ! u(i  ,j+1) coef !
                    c6 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i-1,j  ) coef !
                    c7 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i  ,j  ) coef !
                    c8 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i-1,j+1) coef !
                    c9 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i  ,j+1) coef !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid_vect(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid_vect(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid_vect(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid_vect(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid_vect(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid_vect(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid_vect(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid_vect(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time)

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid_vect(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid_vect(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid_vect(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid_vect(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid_vect(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid_vect(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid_vect(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid_vect(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid_vect(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid_vect(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid_vect(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid_vect(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid_vect(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid_vect(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid_vect(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid_vect(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time) 

                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(ij)  =   c1*u_p0m1 + c2*u_m1p0 + c3*ugrid_vect(i,j) + c4*u_p1p0 + c5*u_p0p1 &
                                    +   c6*v_m1p0 + c7*v_p0p0 + c8*v_m1p1 + c9*v_p0p1 
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(ij)  =   c1*ugrid_vect(i  ,j-1) + c2*ugrid_vect(i-1,j  ) + c3*ugrid_vect(i  ,j  ) + c4*ugrid_vect(i+1,j  ) + c5*ugrid_vect(i  ,j+1) &
                                    +   c6*vgrid_vect(i-1,j  ) + c7*vgrid_vect(i  ,j  ) + c8*vgrid_vect(i-1,j+1) + c9*vgrid_vect(i  ,j+1)
                    end if 
                end do 

            !===============================!
            !   V-component of the SIME     !
            !===============================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (-cnst_3*h_v(i,j) - Cw_v(i,j)*sin_w)/4
                    cnst_6 = eta_gy_v(i,j) + zeta_gy_v(i,j)
                    cnst_7 = eta_v(i,j) + zeta_v(i,j)
                    cnst_8 = zeta_gy_v(i,j) - eta_gy_v(i,j) + eta_gx_v(i,j)
                    cnst_9 = zeta_gy_v(i,j) - eta_gy_v(i,j) - eta_gx_v(i,j)

                    ! calculate coefficients !
                    c1 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i  ,j-1) !
                    c2 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i+1,j-1) !
                    c3 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i  ,j  ) !
                    c4 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i+1,j  ) !
                    c5 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! v(i  ,j-1) !
                    c6 = -cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                          ! v(i-1,j  ) !
                    c7 = -(3/2)*cnst_4*h_v(i,j) - Cw_v(i,j)*cos_w - (2./(dx*dx))*(2*eta_v(i,j) + zeta_v(i,j))     ! v(i  ,j  ) !
                    c8 = cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                           ! v(i+1,j  ) !
                    c9 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! v(i  ,j+1) !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                     if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid_vect(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid_vect(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid_vect(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid_vect(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid_vect(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid_vect(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid_vect(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid_vect(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid_vect(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid_vect(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid_vect(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid_vect(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid_vect(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid_vect(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid_vect(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid_vect(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)                            

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid_vect(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid_vect(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid_vect(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid_vect(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid_vect(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid_vect(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid_vect(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid_vect(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time) 
                            
                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(nU_pnts + ij)    =   c1*u_p0m1 + c2*u_p1m1 + c3*u_p0p0 + c4*u_p1p0 &
                                                +   c5*v_p0m1 + c6*v_m1p0 + c7*vgrid_vect(i,j) + c8*v_p1p0 + c9*v_p0p1
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(nU_pnts + ij)    =   c1*ugrid_vect(i  ,j-1) + c2*ugrid_vect(i+1,j-1) + c3*ugrid_vect(i  ,j  ) + c4*ugrid_vect(i+1,j  ) &
                                                +   c5*vgrid_vect(i  ,j-1) + c6*vgrid_vect(i-1,j  ) + c7*vgrid_vect(i  ,j  ) + c8*vgrid_vect(i+1,j  ) + c9*vgrid_vect(i  ,j+1)
                    end if
                end do
        end if  
    end subroutine calc_Au_r02


!===========================================================================================!
!                                   Calc Au - homogenous BCs                                !
!===========================================================================================!
    subroutine calc_Au_homogenous(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                ulmsk, uimsk, vlmsk, vimsk, u_vect)

        !========================================================================================
        ! This routine uses the new treatement of the rheology term (where the closed form of the 
        ! viscosity gradients are used) to calculate the matrix vector product with homogenous BCs
        ! on u. This was created because when we perform the jacobian multiplication we want to use 
        ! homogenous conditions on delta u.
        !
        ! NOTE: the vector functionality hasn't been incorporated yet.
        !
        !========================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                                ! current time value
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosity values - U points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! viscosity derivatives - U points 
            h_u, Cw_u                           ! ice thickness and non-linear water drag coefficient - U points      
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosity values - V points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! viscosity derivatives - V points
            h_v, Cw_v                           ! ice thickness and non-linear water drag coefficient - V points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                               ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                               ! v velocity on the grid
        real(kind = dbl_kind), optional, dimension(nU_pnts + nV_pnts), intent (in) :: &
            u_vect                              ! velocity in vector form, this is only used for gmres validation 

        !==================!
        ! ----- Inout------!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            Au 
        
        !==================!
        ! ----- Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij
        real(kind = dbl_kind), allocatable, dimension(:,:) :: &
            ugrid_vect, vgrid_vect                              ! if u_vect is present these will hold its values on the grid 
        real(kind = dbl_kind) :: &
            cnst_1, cnst_2, cnst_3, cnst_4,                 &   ! arbitrary constants 
            cnst_5, cnst_6, cnst_7, cnst_8, cnst_9,         &   ! constants used in calculation of matrix coefficients
            c1, c2, c3, c4, c5,                             &   ! coefficients for the crank-nicolson portion of Au; implemented to clean up code
            c6, c7, c8, c9,                                 &   
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p1m1, u_p0p0, &   ! masked velocities
            v_m1p0, v_p0p0, v_m1p1, v_p0p1, v_p0m1, v_p1p0, &
            halfdx 

        !===============================!
        !   Define Necessary Constants  !
        !===============================!
            halfdx = dx/2
            cnst_1 = 1./(dx*dx)
            cnst_2 = 1./(2*dx)
            cnst_3 = rho*f
            cnst_4 = rho/dt

        !==============================================!
        ! Perform Mat-Vec Using the gridded velocities !
        !==============================================!
        if( .not. present(u_vect)) then 

            !===============================!
            !   U-component of the SIME     !
            !===============================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (cnst_3*h_u(i,j) + Cw_u(i,j)*sin_w)/4
                    cnst_6 = eta_gx_u(i,j) + zeta_gx_u(i,j)
                    cnst_7 = eta_u(i,j) + zeta_u(i,j)
                    cnst_8 = zeta_gx_u(i,j) - eta_gx_u(i,j) + eta_gy_u(i,j)
                    cnst_9 = zeta_gx_u(i,j) - eta_gx_u(i,j) - eta_gy_u(i,j)

                    ! calculate coefficients !
                    c1 = -cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                          ! u(i  ,j-1) coef !
                    c2 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! u(i-1,j  ) coef !
                    c3 = -(3/2)*cnst_4*h_u(i,j) - Cw_u(i,j)*cos_w - (2./(dx*dx))*(2*eta_u(i,j) + zeta_u(i,j))     ! u(i  ,j  ) coef !
                    c4 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! u(i+1,j  ) coef !
                    c5 = cnst_2*eta_gy_u(i,j) + cnst_1*eta_u(i,j)                                           ! u(i  ,j+1) coef !
                    c6 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i-1,j  ) coef !
                    c7 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i  ,j  ) coef !
                    c8 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_u(i,j)                                        ! v(i-1,j+1) coef !
                    c9 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_u(i,j)                                        ! v(i  ,j+1) coef !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                    if ((product(ulmsk(i,j,1:8)) == 0) .or. (product(uimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*ugrid(i  ,j  ))
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*ugrid(i  ,j  ))
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*ugrid(i  ,j  ))
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*ugrid(i  ,j  ))

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                    vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            vgrid(i  ,j  ) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         vgrid(i-1,j+1) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   vgrid(i  ,j+1)) 

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                    vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            vgrid(i-1,j  ) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         vgrid(i  ,j+1) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   vgrid(i-1,j+1)) 

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                    vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            vgrid(i  ,j+1) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         vgrid(i-1,j  ) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   vgrid(i  ,j  )) 

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                    vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         vgrid(i  ,j  ) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   vgrid(i-1,j  )) 

                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(ij)  =   c1*u_p0m1 + c2*u_m1p0 + c3*ugrid(i,j) + c4*u_p1p0 + c5*u_p0p1 &
                                    +   c6*v_m1p0 + c7*v_p0p0 + c8*v_m1p1 + c9*v_p0p1 
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(ij)  =   c1*ugrid(i  ,j-1) + c2*ugrid(i-1,j  ) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) + c5*ugrid(i  ,j+1) &
                                    +   c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i-1,j+1) + c9*vgrid(i  ,j+1)
                    end if 
                end do 

            !===============================!
            !   V-component of the SIME     !
            !===============================!
                do ij = 1, nV_pnts

                    i = indxVi(ij)
                    j = indxVj(ij)

                    ! calculate repeated terms in coefs !
                    cnst_5 = (-cnst_3*h_v(i,j) - Cw_v(i,j)*sin_w)/4
                    cnst_6 = eta_gy_v(i,j) + zeta_gy_v(i,j)
                    cnst_7 = eta_v(i,j) + zeta_v(i,j)
                    cnst_8 = zeta_gy_v(i,j) - eta_gy_v(i,j) + eta_gx_v(i,j)
                    cnst_9 = zeta_gy_v(i,j) - eta_gy_v(i,j) - eta_gx_v(i,j)

                    ! calculate coefficients !
                    c1 = cnst_5 - cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i  ,j-1) !
                    c2 = cnst_5 + cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i+1,j-1) !
                    c3 = cnst_5 - cnst_2*cnst_9 - cnst_1*zeta_v(i,j)                                        ! u(i  ,j  ) !
                    c4 = cnst_5 + cnst_2*cnst_8 + cnst_1*zeta_v(i,j)                                        ! u(i+1,j  ) !
                    c5 = -cnst_2*cnst_6 + cnst_1*cnst_7                                                     ! v(i  ,j-1) !
                    c6 = -cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                          ! v(i-1,j  ) !
                    c7 = -(3/2)*cnst_4*h_v(i,j) - Cw_v(i,j)*cos_w - (2./(dx*dx))*(2*eta_v(i,j) + zeta_v(i,j))     ! v(i  ,j  ) !
                    c8 = cnst_2*eta_gx_v(i,j) + cnst_1*eta_v(i,j)                                           ! v(i+1,j  ) !
                    c9 = cnst_2*cnst_6 + cnst_1*cnst_7                                                      ! v(i  ,j+1) !

                    !================================================================!
                    ! Perform the mat-vec product with masked velocities if required !
                    !================================================================!
                     if ((product(vlmsk(i,j,1:8)) == 0) .or. (product(vimsk(i,j,1:8)) == 0)) then
                        ! At least one of the direct neighbours, needed for the mat-vect product isn't in ice !

                        !=================!
                        ! Mask Velocities !
                        !=================!
                            !=====================!
                            ! Direct U Neighbours !
                            !=====================!
                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                ugrid(i  ,j-1) &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          ugrid(i  ,j  ) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       ugrid(i+1,j-1) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* ugrid(i+1,j  )) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                ugrid(i+1,j-1) &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          ugrid(i+1,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       ugrid(i  ,j-1) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* ugrid(i  ,j  )) 
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          ugrid(i  ,j-1) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       ugrid(i+1,j  ) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* ugrid(i+1,j-1)) 
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          ugrid(i+1,j-1) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       ugrid(i  ,j  ) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* ugrid(i  ,j-1))                           

                            !=====================!
                            ! Direct V Neighbours !
                            !=====================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*vgrid(i,j))  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*vgrid(i,j))
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*vgrid(i,j))
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*vgrid(i,j)) 
                            
                        !===============================!
                        ! Perform matrix vector product !
                        !===============================!
                            Au(nU_pnts + ij)    =   c1*u_p0m1 + c2*u_p1m1 + c3*u_p0p0 + c4*u_p1p0 &
                                                +   c5*v_p0m1 + c6*v_m1p0 + c7*vgrid(i,j) + c8*v_p1p0 + c9*v_p0p1
                    else
                        !==========================================!
                        ! Perform mat-vect product without masking !
                        !==========================================!
                            ! All direct neighbours are in ice - no masking required !
                            Au(nU_pnts + ij)    =   c1*ugrid(i  ,j-1) + c2*ugrid(i+1,j-1) + c3*ugrid(i  ,j  ) + c4*ugrid(i+1,j  ) &
                                                +   c5*vgrid(i  ,j-1) + c6*vgrid(i-1,j  ) + c7*vgrid(i  ,j  ) + c8*vgrid(i+1,j  ) + c9*vgrid(i  ,j+1)
                    end if
                end do

        !=================================================!
        ! Perform Mat-Vec Using vector form of velocities !
        !=================================================!
        else

        end if  

    end subroutine calc_Au_homogenous

!===========================================================================================!
!                                      Calc Dbw                                             !
!===========================================================================================!
    subroutine calc_Dbw(Dbw, w, time, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk)

        !====================================================================
        !  This subroutine calculates the Db(u)w portion of the jacobian 
        !  multiplication, where Db(u) is the Jacobian matrix associated with
        !  the vector b, with respect to u.
        !
        ! Note that due to the nature of this multiplication, homogenous BCs 
        ! are always used for the vector w, but non-homogenous BCs may be used 
        ! for u, depending on if the validation problem is being run or not.
        !====================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind) :: & 
            nxU, nyU, &             ! indices limit - U points (not including ghost cells)
            nxV, nyV, &             ! indices limit - V points (not including ghost cells)
            nU_pnts, nV_pnts        ! number of U and V points in the computational domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj          ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj          ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk            ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk            ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                    ! current time value
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            uocn_u, vocn_u
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            uocn_v, vocn_v
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                   ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                   ! v velocity on the grid
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(in) :: &
            w                       ! the arbitrary vector

        !==================!
        ! ---- In/out  ----!
        !==================!
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(inout) :: &
            Dbw                     ! the resulting vector from the multplication 

        !==================!
        ! ----- Local  ----!
        !==================!
        integer(kind = int_kind) :: & 
            i, j, ij

        real(kind = dbl_kind) :: &
            cu_1, cu_2, cu_3, cu_4, &   ! constants used in the calculation of Dbw.
            cv_1, cv_2, cv_3, cv_4, &   
            w1, w2, w3, w4,         &   ! masked velocity deltas for arbitrary w vector
            u1, u2, u3, u4,         &   ! masked u velocities
            v1, v2, v3, v4,         &   ! masked v velocities 
            halfdx
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
            w_u                         ! the vector w placed on the grid at U points 
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
            w_v                         ! the vector w placed on the grid at V points 

        w_u = 0.
        w_v = 0.
        Dbw = 0.

        halfdx = dx/2

        ! place w on the grid !
        do ij = 1, nU_pnts
            i = indxUi(ij)
            j = indxUj(ij)

            w_u(i,j) = w(ij)
        end do 
        do ij = 1, nV_pnts
            i = indxVi(ij)
            j = indxVj(ij)

            w_v(i,j) = w(ij + nU_pnts)
        end do

        !====== Calc Dbw =========!
        ! U-points !
        do ij = 1, nU_pnts
            i = indxUi(ij)
            j = indxUj(ij)

            ! calc constants !
            cu_1 = -rho_w*Cd_w*(uocn_u(i,j)*cos_w - vocn_u(i,j)*sin_w)
            cu_2 = ugrid(i,j) - uocn_u(i,j)

            if ((product(ulmsk(i,j,:)) == 0) .or. (product(uimsk(i,j,:)) == 0)) then
                ! Masking is required as at least of the masks is activated !
                ! mask velocities at the surrounding v points around the u point of interest

                ! v(i-1,j) !
                v1 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                         vgrid(i-1,j  ) &
                        +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                        +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                        +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)
    
                ! v(i,j) !
                v2 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                         vgrid(i  ,j  ) &  
                        +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                        +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                        +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                ! v(i-1,j+1) !
                v3 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                         vgrid(i-1,j+1) &  
                        +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                        +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                        +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                ! v(i,j+1) !
                v4 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                         vgrid(i  ,j+1) &
                        +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                        +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                        +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                        +   (1- ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time) 

                w1 = ulmsk(i,j,5)*(uimsk(i,j,5)*w_v(i-1,j  )  +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*w_v(i  ,j  )  +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*w_v(i-1,j+1) +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*w_v(i  ,j+1)) 
                w2 = ulmsk(i,j,6)*(uimsk(i,j,6)*w_v(i  ,j  )  +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*w_v(i-1,j  )  +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*w_v(i  ,j+1) +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*w_v(i-1,j+1))
                w3 = ulmsk(i,j,7)*(uimsk(i,j,7)*w_v(i-1,j+1)  +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*w_v(i  ,j+1)  +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*w_v(i-1,j  ) +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*w_v(i  ,j  )) 
                w4 = ulmsk(i,j,8)*(uimsk(i,j,8)*w_v(i  ,j+1)  +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*w_v(i-1,j+1)  +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*w_v(i  ,j  ) +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*w_v(i-1,j  ))   

                ! calc remaining constants
                cu_3 = (v1 + v2 + v3 + v4)/4 - vocn_u(i,j)
                cu_4 = sqrt(cu_2**2 + cu_3**2)

                ! calc Dbw
                Dbw(ij) = cu_1*cu_2*w_u(i,j)/cu_4 + (cu_1*cu_3/(4*cu_4))*(w1 + w2 + w3 + w4) 
            else
                ! Masking isn't required

                ! calc remaining constants
                cu_3 = (vgrid(i,j) + vgrid(i-1,j) + vgrid(i-1,j+1) + vgrid(i,j+1))/4 - vocn_u(i,j)
                cu_4 = sqrt(cu_2**2 + cu_3**2)

                ! calc Dbw
                Dbw(ij) = cu_1*cu_2*w_u(i,j)/cu_4 + (cu_1*cu_3/(4*cu_4))*(w_v(i-1,j) + w_v(i,j) + w_v(i-1,j+1) + w_v(i,j+1)) 
            endif
        enddo

        ! V-points !
        do ij = 1, nV_pnts
            i = indxVi(ij)
            j = indxVj(ij)

            ! calc constants !
            cv_1 = -rho_w*Cd_w*(vocn_v(i,j)*cos_w + uocn_v(i,j)*sin_w)
            cv_2 = vgrid(i,j) - vocn_v(i,j)

            if ((product(vlmsk(i,j,:)) == 0) .or. (product(vimsk(i,j,:)) == 0)) then
                ! Masking is required as at least of the masks is activated !
                ! mask velocities at the surounding u points around the v point of interest
               
                ! u(i,j-1) !
                u1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                     ugrid(i  ,j-1)  &
                        + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                        + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                        + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                
                ! u(i+1,j-1) !
                u2 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                     ugrid(i+1,j-1)  &
                        + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                        + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                
                ! u(i,j) !
                u3 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                     ugrid(i  ,j  ) & 
                        + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                        + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                
                ! u(i+1,j) !
                u4 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                     ugrid(i+1,j  ) & 
                        + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                        + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)

                w1 = vlmsk(i,j,1)*(vimsk(i,j,1)*w_u(i  ,j-1) + vimsk(i,j,3)*(1 - vimsk(i,j,1))*w_u(i  ,j  ) + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*w_u(i+1,j-1)    +   (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*w_u(i+1,j  ))
                w2 = vlmsk(i,j,2)*(vimsk(i,j,2)*w_u(i+1,j-1) + vimsk(i,j,4)*(1 - vimsk(i,j,2))*w_u(i+1,j  ) + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*w_u(i  ,j-1)    +   (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*w_u(i  ,j  ))
                w3 = vlmsk(i,j,3)*(vimsk(i,j,3)*w_u(i  ,j  ) + vimsk(i,j,1)*(1 - vimsk(i,j,3))*w_u(i  ,j-1) + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*w_u(i+1,j  )    +   (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*w_u(i+1,j-1))
                w4 = vlmsk(i,j,4)*(vimsk(i,j,4)*w_u(i+1,j  ) + vimsk(i,j,2)*(1 - vimsk(i,j,4))*w_u(i+1,j-1) + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*w_u(i  ,j  )    +   (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*w_u(i  ,j-1))

                ! calc remaining constants
                cv_3 = (u1 + u2 + u3 + u4)/4 - uocn_v(i,j)
                cv_4 = sqrt(cv_3**2 + cv_2**2)

                ! calc Dbw
                Dbw(nU_pnts + ij) = cv_1*cv_2*w_v(i,j)/cv_4 + (cv_1*cv_3/(4*cv_4))*(w1 + w2 + w3 + w4) 
            else
                ! Masking isn't required !

                ! calc remaining constants
                cv_3 = (ugrid(i,j) + ugrid(i+1,j) + ugrid(i+1,j-1) + ugrid(i,j-1))/4 - uocn_v(i,j)
                cv_4 = sqrt(cv_3**2 + cv_2**2)

                ! calc Dbw
                Dbw(nU_pnts + ij) = cv_1*cv_2*w_v(i,j)/cv_4 + (cv_1*cv_3/(4*cv_4))*(w_u(i,j-1) + w_u(i+1,j-1) + w_u(i,j) + w_u(i+1,j)) 
            endif  
        enddo
    end subroutine calc_Dbw
    
!===========================================================================================!
!                            Jacobian Approximation r02                                     !
!===========================================================================================!
    subroutine jacobian_multiplication_r02(Jw, w, Au, time, ugrid, vgrid, &
                                        zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                        Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                        nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                        indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)
        !====================================================================
        ! This subroutine approximates the action of the jacobian on an 
        ! arbitrary vector w. NOTE: This routine uses the new matrix A formulation, 
        ! where we calculate the close forms of the viscosity derivatives.
        !
        ! The first order approximation is as follows:
        !
        !   Jw = (A(u + eps*w)u - A(u)u)/eps + A(u)w - Db(u)w
        !
        !   where Db(u) is the Jacobian Matrix of the vecotr b with respect 
        !   to u.
        !
        ! The second order approximation is:
        !
        !   Jw = (A(u + eps*w)u - A(u - eps*w)u)/2*eps + A(u)w - Db(u)w
        !
        !   Note:   Au_peps ==> A(u + eps*w)u
        !           Au_meps ==> A(u - eps*w)u
        !           Au      ==> A(u)u
        !           Aw      ==> A(u)w         
        !           Dbw     ==> Db(u)w
        !
        ! This subroutine only allocates the needed memory and uses the 2nd order 
        ! approximation if second_ord_Jac = .true. (see modelparams.f90)
        !
        !====================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nxN, nyN,   &                   ! indices limit - N points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                            ! current time value
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosities - u points
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! needed viscosity derivatives - u points
            P_u, h_u, Cw_u,                 &   ! ice strength, thickness, and water drag coefficient - u points       
            uocn_u, vocn_u                      ! ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosities - v points
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! needed viscosity derivatives - v points 
            P_v, h_v, Cw_v,                 &   ! ice strength, thickness, and water drag coefficient - v points
            uocn_v, vocn_v                      ! ocean velocities at v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(in) :: &
            Au                              ! holds the current matrix vector product of A and u
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(in) :: &
            w                               ! the arbitrary vector

        !==================!
        ! ---- In/out  ----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            Jw                  ! holds the resulting matrix vector product of the jacobian and w

        !==================!
        ! ----- Local  ----!
        !==================!
        integer(kind = int_kind) :: i, j, ij 

        !=================!
        ! Required Arrays !
        !=================!
            real(kind = dbl_kind), dimension(nU_pnts + nV_pnts) :: &
                Aw,         &   ! A(u)w
                Au_peps,    &   ! A(u + eps*w)u
                Dbw             ! Db(u)w
            real(kind = dbl_kind), dimension(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
                ugrid_peps, &   ! (u + eps*w) at U points 
                w_u             ! the portion of w that modifies u points
            real(kind = dbl_kind), dimension(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
                vgrid_peps, &   ! (u + eps*w) at V points 
                w_v             ! the portion of w that modifies v points
            real(kind = dbl_kind), dimension (nxU, nyU) :: &
                zeta_u_peps, eta_u_peps,                        &   ! u point viscosities calc'd with (u + eps*w)
                zeta_gx_u_peps, eta_gx_u_peps, eta_gy_u_peps,   &   ! u point viscosity derivatives calc'd with (u + eps*w)
                Cw_u_peps                                           ! u point NL water drag coef calc'd with (u + eps*w)
            real(kind = dbl_kind), dimension (nxV, nyV) :: &
                zeta_v_peps, eta_v_peps,                        &   ! v point viscosities calc'd with (u + eps*w)
                zeta_gy_v_peps, eta_gx_v_peps, eta_gy_v_peps,   &   ! v point viscosity derivatives calc'd with (u + eps*w)
                Cw_v_peps                                           ! v point NL water drag coef calc'd with (u + eps*w)

        !=============================!
        ! Arrays for 2nd order approx !
        !=============================!
            real(kind = dbl_kind), allocatable, dimension(:) :: &
                Au_meps         ! A(u - eps*w)u
            real(kind = dbl_kind), allocatable, dimension(:,:) :: &
                ugrid_meps, vgrid_meps,                         &   ! (u - eps*w) 
                zeta_u_meps, eta_u_meps,                        &   ! u point viscosities calc'd with (u - eps*w)
                zeta_gx_u_meps, eta_gx_u_meps, eta_gy_u_meps,   &   ! u point viscosity derivatives calc'd with (u - eps*w)
                zeta_v_meps, eta_v_meps,                        &   ! v point viscosities calc'd with (u - eps*w)
                zeta_gy_v_meps, eta_gx_v_meps, eta_gy_v_meps,   &   ! v point viscosity derivatives calc'd with (u - eps*w)
                Cw_u_meps, Cw_v_meps                                ! NL water drag coefs calc'd with (u - eps*w)

        ! Initialize Arrays !
        Aw              = 0.
        Au_peps         = 0.
        Dbw             = 0.
        ugrid_peps      = 0.
        vgrid_peps      = 0.
        w_u             = 0.
        w_v             = 0.
        zeta_u_peps     = 0.
        zeta_v_peps     = 0.
        eta_u_peps      = 0.
        eta_v_peps      = 0.
        zeta_gx_u_peps  = 0.
        zeta_gy_v_peps  = 0.
        eta_gx_u_peps   = 0.
        eta_gx_v_peps   = 0.
        eta_gy_u_peps   = 0.
        eta_gy_v_peps   = 0.
        Cw_u_peps       = 0.
        Cw_v_peps       = 0.

        !==========================================!
        ! Calc (u + eps*w) and place w on the grid !
        !==========================================!
            ! U-points !
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ugrid_peps(i,j)     = ugrid(i,j) + eps*w(ij)
                w_u(i,j)            = w(ij)
            enddo

            ! V-points !
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vgrid_peps(i,j)     = vgrid(i,j) + eps*w(nU_pnts + ij)
                w_v(i,j)            = w(nU_pnts + ij)
            enddo

        !=========!
        ! Calc Aw ! 
        !=========!
            call calc_Au_homogenous(Aw, w_u, w_v, time, zeta_u, zeta_v, eta_u, eta_v, &
                                    zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk)
        !==========!
        ! Calc Dbw !
        !==========!
            call calc_Dbw(Dbw, w, time, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk)
        !==============!
        ! Calc Au_peps !
        !==============!
            call calc_visc_and_viscgrads(zeta_u_peps, zeta_v_peps, zeta_gx_u_peps, zeta_gy_v_peps, eta_u_peps, eta_v_peps, &
                                        eta_gx_u_peps, eta_gx_v_peps, eta_gy_u_peps, eta_gy_v_peps, P_u, P_v, P_T, P_N, ugrid_peps, vgrid_peps, time, &
                                        vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

            call calc_waterdrag_coef(Cw_u_peps, Cw_v_peps, ugrid_peps, vgrid_peps, uocn_u, vocn_u, uocn_v, vocn_v, &
                                    time, ulmsk, uimsk, vlmsk, vimsk, &
                                    nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                    indxUi, indxUj, indxVi, indxVj)
        
            call calc_Au_r02(Au_peps, ugrid, vgrid, time, zeta_u_peps, zeta_v_peps, eta_u_peps, eta_v_peps, &
                            zeta_gx_u_peps, zeta_gy_v_peps, eta_gx_u_peps, eta_gx_v_peps, eta_gy_u_peps, eta_gy_v_peps, Cw_u_peps, Cw_v_peps, h_u, h_v, &
                            nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                            ulmsk, uimsk, vlmsk, vimsk)

        !===============================!
        ! Check for second order approx !
        !===============================!
            if (second_ord_Jac) then 
                !================================!
                ! Allocate and initialize arrays !
                !================================!
                    allocate(Au_meps(nU_pnts + nV_pnts), & 
                             ugrid_meps(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), vgrid_meps(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), &
                             zeta_u_meps(nxU, nyU), eta_u_meps(nxU, nyU), zeta_gx_u_meps(nxU, nyU), &
                             eta_gx_u_meps(nxU, nyU), eta_gy_u_meps(nxU, nyU), Cw_u_meps(nxU, nyU), &
                             zeta_v_meps(nxV, nyV), eta_v_meps(nxV, nyV), zeta_gy_v_meps(nxV, nyV), &
                             eta_gx_v_meps(nxV, nyV), eta_gy_v_meps(nxV, nyV), Cw_v_meps(nxV, nyV))

                    Au_meps = 0.; ugrid_meps = 0.; vgrid_meps = 0.; 
                    zeta_u_meps = 0.; eta_u_meps = 0.; zeta_gx_u_meps = 0.;
                    eta_gx_u_meps = 0.; eta_gy_u_meps = 0.; Cw_u_meps = 0.;
                    zeta_v_meps = 0.; eta_v_meps = 0.; zeta_gy_v_meps = 0.;
                    eta_gx_v_meps = 0.; eta_gy_v_meps = 0.; Cw_v_meps = 0.

                !================!
                ! Calc u - eps*w !
                !================!
                    ! U points !
                    do ij = 1, nU_pnts
                        i = indxUi(ij)
                        j = indxUj(ij)

                        ugrid_meps(i,j) = ugrid(i,j) - eps*w_u(i,j)
                    end do 
                    ! V points !
                    do ij = 1, nV_pnts
                        i = indxVi(ij)
                        j = indxVj(ij)

                        vgrid_meps(i,j) = vgrid(i,j) - eps*w_v(i,j)
                    end do

                !==============!
                ! Calc Au_meps !
                !==============!
                    call calc_visc_and_viscgrads(zeta_u_meps, zeta_v_meps, zeta_gx_u_meps, zeta_gy_v_meps, eta_u_meps, eta_v_meps, &
                                            eta_gx_u_meps, eta_gx_v_meps, eta_gy_u_meps, eta_gy_v_meps, P_u, P_v, P_T, P_N, ugrid_meps, vgrid_meps, time, &
                                            vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                            nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                    call calc_waterdrag_coef(Cw_u_meps, Cw_v_meps, ugrid_meps, vgrid_meps, uocn_u, vocn_u, uocn_v, vocn_v, &
                                            time, ulmsk, uimsk, vlmsk, vimsk, &
                                            nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                            indxUi, indxUj, indxVi, indxVj)
                
                    call calc_Au_r02(Au_meps, ugrid, vgrid, time, zeta_u_meps, zeta_v_meps, eta_u_meps, eta_v_meps, &
                                    zeta_gx_u_meps, zeta_gy_v_meps, eta_gx_u_meps, eta_gx_v_meps, eta_gy_u_meps, eta_gy_v_meps, Cw_u_meps, Cw_v_meps, h_u, h_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk)
                !=========!
                ! Calc Jw !
                !=========!
                    Jw = (Au_peps - Au_meps)/(2*eps) + Aw - Dbw
            else
                !=========!
                ! Calc Jw !
                !=========!
                    Jw = (Au_peps - Au)/eps + Aw - Dbw
            end if       

    end subroutine jacobian_multiplication_r02

!===========================================================================================!
!                                   GMRES Solver                                            !
!===========================================================================================!
    
!===========================================================================================!
!                                   GMRES Solver r02                                        !
!===========================================================================================!
    subroutine gmres_solve_r02(mat_type, iter, u_update, RHS, RHS_norm, tol, size, tmp_size, &
                                time, Au, ugrid, vgrid, P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, &
                                zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v,  &
                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)
    
        !====================================================================
        ! This subroutine calculates the u_update using intel's fgmres
        ! routine. The primary function of this routine is the solve 
        !
        !                       J(u)*u_update = -res_NL,
        !
        ! where J(u) is the jacobian of the system matrix A, w.r.t. u.
        !
        ! But this also has the ability to solve linear systems involving two
        ! other matrix types; this functionality was added to aid in the validation
        ! of the solver. 
        !
        ! For the primary function, this routine should be called with 
        !      
        !                   mat_type = 'Jx'
        !
        ! NOTE: this routine uses the updated formulation of the rheology terms for
        !       the system matrix A(u) and the jacobian, J(u).
        !
        !====================================================================


        !==================!
        ! ----- In --------!
        !==================!
        character(len = 2), intent(in) :: &
            mat_type                        ! identifier of the matrix to be used for the matrix vector multiplication 

        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nxN, nyN,   &                   ! indices limit - N points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts,    &                   ! number of V points in the comp. domain
            size,       &                   ! nU_pnts + nV_pnts
            tmp_size                        ! size of tmp array (defined for gmres routine)
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            tol,            &               ! defines the tolerance of the solver
            RHS_norm,       &               ! holds the 2 norm of RHS vector
            time
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosities - u points 
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! needed viscosity derivatives - u points
            P_u, h_u, Cw_u,                 &   ! ice strength, thickness and water drag coefficient - u points       
            uocn_u, vocn_u                      ! ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosities - v points 
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! needed viscosity derivatives - v points
            P_v, h_v, Cw_v,                 &   !ice strength, thickness and water drag coefficient - v points
            uocn_v, vocn_v                      ! ocean velocities at v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(size), intent(in) :: &
            Au,             &               ! current matrix vector product of A and u
            RHS                             ! generally will be -res_NL

        !==================!
        ! ----- In/out ----!
        !==================!
        real(kind = dbl_kind), dimension(size), intent(inout) :: &
            u_update                        ! velocity update vector
        integer(kind = int_kind), intent(inout) :: &
            iter
            
        !==================!
        ! ----- Local  ----!
        !==================!
        integer(kind = int_kind) :: &
            rci_request,            &       ! used in the RCI interface of the gmres routine (essentially it tells the code what operations to do)
            itercount                       ! will hold the number of iterations of the gmres routine 
        integer(kind = int_kind), dimension(128) :: &
            ipar                            ! integer parameters of the gmres routine 

        real(kind = dbl_kind), dimension(128) :: & 
            dpar                            ! double parameters of the gmres routine
        real(kind = dbl_kind), allocatable, dimension(:) :: &
            tmp                             ! temporary storage for vectors produced in the gmres routine 

        ! allocate tmp array !
        allocate(tmp(tmp_size))

        ! Initialize initial guess to zero !
        u_update = 0.

        ! Initialize GMRES Solver !
        call dfgmres_init(size, u_update, RHS, rci_request, ipar, dpar, tmp)
            ! check for error !
            if (rci_request /= 0) then 
                print *, 'Error in the GMRES routine; process aborted.'
                stop
            endif

            ! set integer parameters !
            ! ipar(5)     = gmres_iter_max
            ipar(5)     = size
            ipar(9)     = 1                             ! turns on the residual stopping test                      
            ipar(10)    = 0                             ! turns off user defined stopping test
            if (lin_precond == .true.) ipar(11) = 1     ! turns on preconditioning (lin_precond is in the model parameters module)
            ipar(12)    = 1
            ipar(15)    = gmres_rest                    ! specifies the number of non-restarted gmres iterations

            ! set double precision parameters !
            dpar(1)     = 0.                            ! force the gmres routine to only consider our tolerance
            dpar(2)     = tol*RHS_norm*x_extent/dx   ! specify the absolute tolerance (according to Lemieux 2012)

            ! print *, "****************************************************************"
            ! print *, "GMRES absolute tolerance set to:", dpar(2)
            ! print *, ""

        ! Check for errors !
        call dfgmres_check(size, u_update, RHS, rci_request, ipar, dpar, tmp)
            ! check for error !
            if (rci_request /= 0) then
                print *, 'Error in the GMRES routine; process aborted.'
                stop
            endif

        !=========== Main GMRES Step ==============!
        do
            call dfgmres(size, u_update, RHS, rci_request, ipar, dpar, tmp)
            ! print*, rci_request
            ! print*, dpar(5)
            ! print*, ipar(14)

                if (rci_request == 0) then
                    ! Solution found
                    ! print *, ""
                    ! print *, "**** Stopping Test Passed ****"
                    ! print *, "GMRES final residual:", dpar(5)
                    ! print *, ""
                    exit 
                else if (rci_request == 1) then

                    if (mat_type == 'Jx') then 

                        ! perform Jacobian multiplication !
                        call jacobian_multiplication_r02(tmp(ipar(23)), tmp(ipar(22)), Au, time, ugrid, vgrid, &
                                                        zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, &
                                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                                        Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                        nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                                        indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                    else if (mat_type == 'Ax') then 

                        ! perform system matrix multiplication !
                        call calc_Au_r02(tmp(ipar(23)), ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                    zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk, tmp(ipar(22)))

                    else if (mat_type == 'Db') then

                        ! perform the matrix mult with the Db matrix !
                        call calc_Dbw(tmp(ipar(23)), tmp(ipar(22)), time, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                    ulmsk, uimsk, vlmsk, vimsk)

                    end if 

                else if (rci_request == 3) then
                    ! perform the preconditioning !
                    ! This will be implemented once the non-preconditioned solver is working
                else
                    print *, '******************** Warning: GMRES routine failed to converge. ************************************'
                    print *, 'This likely is the maximum iteration count being reached'
                    print *, 'Final Residual = ', dpar(5)
                    exit 
                end if
        enddo

        !============ Populate Solution Vector ======!
        ! Note: mkl_free_buffers frees up unused memory
        call dfgmres_get(size, u_update, RHS, rci_request, ipar, dpar, tmp, itercount)
        call mkl_free_buffers 
        ! print *, 'Linear Solver Terminated in ', itercount, 'iterations.'
        ! print *, "****************************************************************"

        ! set iteration count !
        iter = itercount

    end subroutine gmres_solve_r02

!===========================================================================================!
!                                   Non-Linear Solver                                       !
!===========================================================================================!
    
!===========================================================================================!
!                                   Non-Linear Solver r02                                   !
!===========================================================================================!
    subroutine JFNKsolve_r02(ugrid, vgrid, time, Au, b, b_0, u_update, res_NL, size, tmp_size, &
                            zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, & 
                            P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                            nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                            indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk, iter_count, res_Final) 
    
        !============================================================================
        ! This subroutine advances ugrid and vgrid to the next time step through the JFNK 
        ! solver. 
        !
        ! NOTE: this routine uses the new formulation of the rheology term in gmres_solve_r02
        !       and the A(u)u routine.
        ! 
        !============================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nxN, nyN,   &                   ! indices limit - N points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts,    &                   ! number of V points in the comp. domain
            size,       &                   ! nU_pnts + nV_pnts
            tmp_size                        ! defines the size of the gmres_tmp array
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                            ! current time value
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            P_u, h_u, uocn_u, vocn_u             ! ice strength, thickness and ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            P_v, h_v, uocn_v, vocn_v             ! ice strength, thickness and ocean velocities at v points
        real(kind = dbl_kind), dimension (size), intent(in) :: &
            b_0                             ! contains the portion of b that isn't affected by the current velocity solution 

        !==================!
        ! ---- In/out  ----!
        !==================!
        integer(kind = int_kind), intent(inout) :: & 
            iter_count                      ! holds the number of non-linear iterations to reach convergence

        real(kind = dbl_kind), intent(inout) :: &
            res_Final                       ! holds the final residual at each time step
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(inout) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(inout) :: &
            vgrid                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(size), intent(inout) :: &
            Au,             &               ! current matrix vector product of A and u
            u_update,       &               ! contains the update values for ugrid and vgrid
            b,              &               ! right hand side of Au = b
            res_NL                          ! current non-linear residual
        real(kind = dbl_kind), dimension (nxU, nyU), intent(inout) :: &
            zeta_u, eta_u,                  &   ! viscosities - u points
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! needed viscosity derivatives - u points
            Cw_u                                ! water drag coefficient - u points   
        real(kind = dbl_kind), dimension (nxV, nyV), intent(inout) :: &
            zeta_v, eta_v,                  &   ! viscosities - v points
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! needed viscosity derivatives - v points 
            Cw_v                                ! water drag coefficient - v points

        !==================!
        ! ---- Local   ----!
        !==================!
        character(len = 8) :: &
            fmt1, fmt2, fmt3, fmt4, &       ! formats for saving files
            dx_f, dt_f, t_lvl,      &       ! strings for saving files 
            eps_str, nltolcoef_str, &   
            strng            

        integer(kind = int_kind) :: &
            i, j, ij, k, n, lin_it          ! local indices and counters

        integer(kind = int_kind), dimension(k_max) :: &
            Linear_Iterations               ! stores the number of iterations used at each linear solve
  
        real(kind = dbl_kind) :: &
            tau,                &           ! damping term for the Newton Iteration
            linear_tol,         &           ! tolerance for the linear solver
            disc_err_tol,       &           ! hold the lower limit tolerance for the non-linear residual. Defined according to the discretization error
            res_NL_norm,        &           ! norm of the current residual
            res_NL_norm_init,   &           ! norm of the initial residual
            res_NL_norm_prev                ! norm of the residual for iteration k - 2
        real(kind = dbl_kind), allocatable, dimension (:) :: &
            RHS                             ! holds the RHS for the linear solver
        real(kind = dbl_kind), dimension(k_max + 1) :: &
            Residual                        ! stores the residual at each step. Note: the extra entry is for the initial residual
        real(kind = dbl_kind), dimension(k_max) :: &
            damp_factors                    ! stores the damping factor used to progress to the next iteration
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
            ugrid_prev                      ! stores the previous u soln
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
            vgrid_prev                      ! stores the previous v soln 

        fmt1 = '(I4.4)'
        fmt2 = '(I8.8)'
        fmt3 = '(ES6.0)'
        fmt4 = '(F4.2)'
        write (dx_f, fmt1) int(dx/1000)                 ! turn dx into a string 
        write (dt_f, fmt1) int(dt)                      ! turn dt into a string
        write (t_lvl, fmt2) int(time)                   ! turn time into a string for file naming
        write (eps_str, fmt3) eps                       ! turn epsilon value in a string
        write (nltolcoef_str, fmt3) disctol_coef_nl     ! turn disctol_coef_nl into a string 
        write (strng, fmt4) ResInc_tol 

        ! allocate the RHS !
        allocate(RHS(size))

        ! calculate discretization error cut off !
        disc_err_tol = (rho*h_init*f*typ_vel)*disctol_coef_nl*(dx**2)/(x_extent**2)                 ! Matching the coriolis term

        ! calculate norm of inital Nonlinear residual !
        call calc_nrm2(res_NL_norm_init, res_NL, size)

        ! initialize variables !
        tau                 = tau_init
        k                   = 0
        res_NL_norm         = res_NL_norm_init
        res_NL_norm_prev    = res_NL_norm_init 
        ugrid_prev          = 0.
        vgrid_prev          = 0.

        ! Main loop !
        ML: do  
            !===================================================================!
            ! Adaptive Damping - if Adaptive_Damp = .true. (in modelparams.f90) !
            !===================================================================!
                if (Adaptive_Damp .and. (k > 0)) then
                    if (res_NL_norm >= res_NL_norm_prev) then 

                        ! Repeat until a decrease is noted !
                        DecrL: do 

                            ! Check for Tau limit !
                                if (tau >= tau_lim) then 
                                    print *, "Unable to decrease residual further past iteration ", k-1
                                    print *, "Maximum tau reached. Solver terminating."
                                    print *, "Final Residual = ", res_NL_norm_prev

                                    iter_count  = k - 1
                                    res_Final   = res_NL_norm_prev

                                    ! Reset solution !
                                        ugrid       = ugrid_prev
                                        vgrid       = vgrid_prev
                                        res_NL_norm = res_NL_norm_prev

                                    ! calc drag coefs and visc coefs ! - I decided to re-calculate these instead of storing to limit storage issues
                                        call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                                                vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                                                nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                                        call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                                time, ulmsk, uimsk, vlmsk, vimsk, &
                                                                nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                                                indxUi, indxUj, indxVi, indxVj)

                                        call calc_Au_r02(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                                zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                                nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                                ulmsk, uimsk, vlmsk, vimsk)

                                        call calc_b(b, b_0, Cw_u, Cw_v, &
                                                    uocn_u, vocn_u, uocn_v, vocn_v, &
                                                    nxU, nyU, nxV, nyV, &
                                                    nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                                        res_NL = Au - b

                                    if (mod(time, sv_interval) == 0) then
                                        open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                        call csv_write_dble_1d(1, Residual(1:k))
                                        close(1)

                                        ! if k > 0, save linear iteration counts !
                                        if (k > 0) then 
                                            open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                            call csv_write_integer_1d(1, Linear_Iterations(1:k-1))
                                            close(1)

                                            open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                            call csv_write_dble_1d(1, damp_factors(1:k-1))
                                            close(1)
                                        end if
                                    end if

                                    Exit ML
                                end if 

                            ! print *, "****** Increase in residual at iteration ", k, "! ******"
                            ! print *, "Damping factor increased from ", tau, "to ", tau*tau_incfact

                            ! Increase in residual noted, try update with increased tau !
                                tau = tau*tau_incfact

                            ! Update ugrid and vgrid again !
                                do ij = 1,nU_pnts
                                    i = indxUi(ij)
                                    j = indxUj(ij)

                                    ugrid(i,j) = ugrid_prev(i,j) + (1/tau)*u_update(ij)
                                end do 

                                do ij = 1,nV_pnts
                                    i = indxVi(ij)
                                    j = indxVj(ij)

                                    vgrid(i,j) = vgrid_prev(i,j) + (1/tau)*u_update(nU_pnts + ij)
                                end do

                            ! Re-calculate residual !
                                call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                                        eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                                        vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                                call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                        time, ulmsk, uimsk, vlmsk, vimsk, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                                        indxUi, indxUj, indxVi, indxVj)

                                call calc_Au_r02(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                        ulmsk, uimsk, vlmsk, vimsk)

                                call calc_b(b, b_0, Cw_u, Cw_v, &
                                            uocn_u, vocn_u, uocn_v, vocn_v, &
                                            nxU, nyU, nxV, nyV, &
                                            nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                                res_NL = Au - b

                                call calc_nrm2(res_NL_norm, res_NL, size)

                            ! Check for decrease and exit do loop if noted !
                                if (res_NL_norm < res_NL_norm_prev) then 
                                    ! print *, "Decrease achieved with tau =", tau 
                                    damp_factors(k) = tau
                                    EXIT DecrL
                                end if 
                        end do DecrL

                    else if ((res_NL_norm >= dectau_tol*res_NL_norm_prev) .and. (tau > 1.d0)) then 
                        ! print *, "****** Small decrease in residual noted at iteration ", k, "! ******"
                        ! print *, "Damping factor decreased from ", tau, "to ", tau/tau_decfact 

                        ! store tau !
                            damp_factors(k) = tau

                        ! decrease tau !
                            tau = max(1.d0, tau/tau_decfact)
                    else 
                        ! store tau !
                        damp_factors(k) = tau 
                    end if

                    ! Check for plateau !
                    if (AD_plateau_check .and. (res_NL_norm >= AD_plat_tol*res_NL_norm_prev) .and. (k >= platinc_check)) then
                        print *, "Solver plateaued at iteration", k 
                        print *, "Solver terminating"
                        print *, "Final Residual = ", res_NL_norm

                        ! store values !
                            Residual(k + 1) = res_NL_norm
                            iter_count      = k 
                            res_Final       = res_NL_norm

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            open(unit = 2, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            call csv_write_integer_1d(2, Linear_Iterations(1:k))
                            close(1)
                            close(2)

                            if (Adaptive_Damp) then 
                                open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_dble_1d(1, damp_factors(1:k))
                                close(1)
                            end if 
                        end if 
                        
                        Exit ML
                    end if 
                end if 

            !===================================!
            ! set the RHS for the linear solver !
            !===================================!
                RHS = -1*res_NL

            !========================!
            ! Store norm of Residual !
            !========================! 
                Residual(k + 1) = res_NL_norm

            !=========================!
            ! Check stopping criteria !
            !=========================!

                !====================!
                ! Iteration Criteria !
                !====================!
                    if (k == k_max) then

                        iter_count  = k
                        res_Final   = res_NL_norm

                        print *, "Maximum iteration count reached, solver terminated."
                        print *, "Limit = ", k_max, "iterations"
                        print *, "Final Residual = ", res_NL_norm
                        print *, ""

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            open(unit = 2, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            call csv_write_integer_1d(2, Linear_Iterations(1:k))
                            close(1)
                            close(2)

                            if (Adaptive_Damp) then 
                                open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_dble_1d(1, damp_factors(1:k))
                                close(1)
                            end if 
                        end if 
                        
                        Exit ML
                    end if

                !===================!
                ! Residual Criteria !
                !===================!
                ! Disc Cut-Off !
                    if (res_NL_norm < disc_err_tol) then    ! Check Discretization Error Cut-Off !

                        iter_count  = k
                        res_Final   = res_NL_norm

                        print *, 'Non-linear solver converged in ', k, 'iterations! Residual below discretization error.'
                        print *, ''
                        print *, 'Final Residual:', res_NL_norm

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            close(1)

                            ! if k > 0, save linear iteration counts !
                            if (k > 0) then 
                                open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_integer_1d(1, Linear_Iterations(1:k))
                                close(1)

                                if (Adaptive_Damp) then 
                                    open(unit = 1, file = './output/DampFacts_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                    call csv_write_dble_1d(1, damp_factors(1:k))
                                    close(1)
                                end if
                            end if
                        end if 

                        Exit ML
                        
                ! Plateau Check !
                    else if (plateau_check .and. (k >= platinc_check) .and. &
                         ((res_NL_norm > plat_tol*res_NL_norm_prev) .and. (res_NL_norm <= res_NL_norm_prev))) then !  if enough iterations have happened, check for plateau in residual 

                        iter_count  = k 
                        res_Final   = res_NL_norm 
                        print *, 'Non-linear solver plateaued at iteration', k,'! Solver Terminated.'
                        print *, 'Final Residual:', res_NL_norm 
                        print *, ""

                        if (mod(time, sv_interval) == 0) then
                            open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                            call csv_write_dble_1d(1, Residual(1:k+1))
                            close(1)

                            ! if k > 0, save linear iteration counts !
                            if (k > 0) then 
                                open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                call csv_write_integer_1d(1, Linear_Iterations(1:k))
                                close(1)
                            end if
                        end if 

                        Exit ML

                ! Conditional Termination !
                    else if (ResInc_Term .and. &
                        (k >= platinc_check) .and. (res_NL_norm > ResInc_tol*res_NL_norm_prev)) then ! Check for large residual increase !

                            iter_count  = k - 1
                            res_Final   = res_NL_norm_prev

                            print *, '****** Large residual increase at iteration ', k, '! Using previous solution! ******'
                            print *, ''
                            print *, 'Final Residual:', res_NL_norm_prev

                            !================!
                            ! reset solution !
                            !================!
                                ugrid       = ugrid_prev
                                vgrid       = vgrid_prev
                                res_NL_norm = res_NL_norm_prev

                                ! calc drag coefs and visc coefs ! - I decided to re-calculate these instead of storing to limit storage issues
                                call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                                        eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                                        vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                                call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                        time, ulmsk, uimsk, vlmsk, vimsk, &
                                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                                        indxUi, indxUj, indxVi, indxVj)

                                call calc_Au_r02(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                        ulmsk, uimsk, vlmsk, vimsk)

                                call calc_b(b, b_0, Cw_u, Cw_v, &
                                            uocn_u, vocn_u, uocn_v, vocn_v, &
                                            nxU, nyU, nxV, nyV, &
                                            nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                                res_NL = Au - b

                            ! Save !
                                if (mod(time, sv_interval) == 0) then
                                    open(unit = 1, file = './output/Nonlin_Res_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                    call csv_write_dble_1d(1, Residual(1:k))
                                    close(1)

                                    ! if k > 0, save linear iteration counts !
                                    if (k > 0) then 
                                        open(unit = 1, file = './output/Lin_Iter_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
                                        call csv_write_integer_1d(1, Linear_Iterations(1:k-1))
                                        close(1)
                                    end if
                                end if

                            Exit ML
                    else 
                        ! Continue with NL iterations !
                    endif

            !======================!
            ! Set Linear Tolerance !
            !======================!
                if (res_NL_norm >= res_t) then ! 
                    linear_tol = gmres_tol_ini
                else
                    linear_tol = max(gmres_tol_min, min(res_NL_norm/res_NL_norm_prev, gmres_tol_ini))
                end if

            !=========================!
            ! Store Previous Solution !
            !=========================!
                ugrid_prev = ugrid 
                vgrid_prev = vgrid

            !==============!
            ! Linear Solve !
            !==============!
                call gmres_solve_r02('Jx', lin_it, u_update, RHS, res_NL_norm, linear_tol, size, tmp_size, &
                                time, Au, ugrid, vgrid, P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, &
                                zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v,  &
                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                Linear_Iterations(k+1) = lin_it

            !===================!    
            ! Update velocities !
            !===================!
                ! U-points !
                do ij = 1, nU_pnts
                    ! get indices !
                    i = indxUi(ij)
                    j = indxUj(ij)

                    ugrid(i,j)  = ugrid(i,j) + (1/tau)*u_update(ij)
                enddo

                ! V-points !
                do ij = 1, nV_pnts
                    ! get indices !
                    i = indxVi(ij)
                    j = indxVj(ij)

                    vgrid(i,j)  = vgrid(i,j) + (1/tau)*u_update(nU_pnts + ij)
                enddo

            !============================================!
            ! Update variables dependent on the velocity !
            !============================================!
                call calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                            eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                            vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                            nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

                call calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                        time, ulmsk, uimsk, vlmsk, vimsk, &
                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                        indxUi, indxUj, indxVi, indxVj)

            !==================================!
            ! Calculate and store new residual !
            !==================================!
                call calc_Au_r02(Au, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                            zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                            nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                            ulmsk, uimsk, vlmsk, vimsk)

                call calc_b(b, b_0, Cw_u, Cw_v, &
                            uocn_u, vocn_u, uocn_v, vocn_v, &
                            nxU, nyU, nxV, nyV, &
                            nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                res_NL = Au - b 

                res_NL_norm_prev = res_NL_norm
                call calc_nrm2(res_NL_norm, res_NL, size)

            !========================!
            ! Update Iteration Count !
            !========================!
                k = k + 1

        end do ML
    end subroutine JFNKsolve_r02

                                             !===============!
!============================================!  End of BDF2  !===============================================!
                                             !===============!
!===========================================================================================!
!                                   Smooth UV                                               !
!===========================================================================================!
    subroutine SmoothUV(time, ugrid, vgrid, uimsk, ulmsk, vimsk, vlmsk, dist_T, dist_gx_T, dist_gy_T, dist_N, & 
                        indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)
        !=============================================================================
        ! This routine smooths the resulting velocities fields from the non-linear 
        ! solver by explicity solving 
        !                   
        !                   u_t = nu*(u_xxxx + u_yyyy)
        !                   v_t = nu*(v_xxxx + v_yyyy)
        !
        ! using centered differences.
        !
        ! Note that this routine checks for masking if the distance function value is 
        ! less than 3*dx. An extra dx was added as we don't correct the distance fnctn 
        ! at every time-step.
        !
        ! Note that this routine hasn't been set up to work on the validation problem.
        !=============================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent (in) :: &
            time                        ! current time level
        integer(kind = int_kind), intent (in) :: &
            nxU, nyU, nxV, nyV,     &   ! index limits for U and V grids 
            nxT, nyT, nxN, nyN,     &   ! index limits for T and N grids 
            nU_pnts, nV_pnts            ! number of U and V points in the computational domain 
        integer(kind = int_kind), dimension (nU_pnts), intent (in) :: &
            indxUi, indxUj              ! locations of U points 
        integer(kind = int_kind), dimension (nV_pnts), intent (in) :: &
            indxVi, indxVj              ! locations of V points 
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent (in) :: &
            uimsk, ulmsk                ! computational masks for U points 
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent (in) :: &
            vimsk, vlmsk                ! computational masks for V points
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (in) :: &
            dist_T, dist_gx_T, dist_gy_T    ! distance function variables - tracer points 
        real(kind = dbl_kind), dimension (nxN, nyN), intent (in) :: &
            dist_N                          ! distance function at node points 

        !=================!
        ! ---- In/Out --- !
        !=================!
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent (inout) :: &
            ugrid                       ! u velocities 
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent (inout) :: &
            vgrid 

        !===============!
        ! --- Local --- !
        !===============!
        character(len = 8) :: &
            fmt, t_lvl                              ! strings for printing the time level 
        integer(kind = int_kind) :: &
            i, j, ij 
        real(kind = dbl_kind) :: &
            u_xxxx, u_yyyy, v_xxxx, v_yyyy,     &   ! discretized 4th derivs
            u_p2p0, u_p1p0, u_m1p0, u_m2p0,     &   ! masked vels for u_xxxx
            u_p0p2, u_p0p1, u_p0m1, u_p0m2,     &   ! masked vels for u_yyyy 
            v_p2p0, v_p1p0, v_m1p0, v_m2p0,     &   ! masked vels for v_xxxx
            v_p0p2, v_p0p1, v_p0m1, v_p0m2,     &   ! masked vels for v_yyyy
            dist, lmbda  
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
            ugrid_tmp                   ! temporary array to hold the new values !
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
            vgrid_tmp

            ugrid_tmp = 0.d0
            vgrid_tmp = 0.d0                           

            ! Calc Lambda !
            lmbda = -visc_smth*dt_smth/(dx**4)

            ! Turn time into string !
            fmt = '(I8.8)'
            write (t_lvl, fmt) int(time)

        !===============================!
        !   Saved Unsmoothed Solution   !
        !===============================!
            ! if (mod(time, sv_interval) == 0) then 
            !     open(unit = 1, file = './output/unsmoothed_u_time'//trim(t_lvl)//'.dat', status = 'unknown')
            !     open(unit = 2, file = './output/unsmoothed_v_time'//trim(t_lvl)//'.dat', status = 'unknown')
            !     call csv_write_dble_2d(1, ugrid)
            !     call csv_write_dble_2d(2, vgrid)
            !     close(1)
            !     close(2)
            ! end if 

        !=======================!
        !   smooth u solution   !
        !=======================!
            do ij = 1,nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ! interpolate distance fnctn !
                dist = (dist_T(i,j) + dist_T(i-1,j))/2

                if (dist < 3*dx) then 
                    ! Check for masking !

                    ! Mask points using pre-built masks !
                        u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*ugrid(i  ,j  ))
                        u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*ugrid(i  ,j  ))
                        u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*ugrid(i  ,j  ))
                        u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*ugrid(i  ,j  ))

                    ! Mask u(i-2,j) !
                        if (lmsk(i-2,j)) then 
                            ! Check for land connection at u(i-1,j) !
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i-1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_m2p0 = 0.d0
                            else
                                ! use Neumann conditions !
                                u_m2p0 = u_m1p0
                            end if 
                        else if (lmsk(i-3,j)) then 
                            ! Check for land connection at u(i-2,j) !
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i-2, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_m2p0 = 0.d0
                            else
                                ! use Neumann conditions !
                                u_m2p0 = u_m1p0
                            end if 
                        else
                            ! Check if u(i-2,j) is in ice !
                            if (.not. inIce('upnt', dist_T, dist_N, i-2, j, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                u_m2p0 = u_m1p0 
                            else
                                u_m2p0 = ugrid(i-2,j)
                            end if 
                        end if 

                    ! Mask u(i+2,j) !
                        if (lmsk(i+1,j)) then 
                            ! Check for land connection at u(i+1,j) !
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_p2p0 = 0.d0
                            else 
                                ! use Neumann conditions !
                                u_p2p0 = u_p1p0 
                            end if 
                        else if (lmsk(i+2,j)) then 
                            ! Check for land connection at u(i+2,j) !
                            if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+2, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_p2p0 = 0.d0
                            else 
                                ! use Neumann conditions !
                                u_p2p0 = u_p1p0 
                            end if
                        else 
                            ! Check if u(i+2,j) is in ice !
                            if (.not. inIce('upnt', dist_T, dist_N, i+2, j, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                u_p2p0 = u_p1p0
                            else 
                                u_p2p0 = ugrid(i+2,j)
                            end if 
                        end if

                    ! Mask u(i,j-2) !
                        if (lmsk(i,j-1) .or. lmsk(i-1,j-1)) then 
                            ! Check for land connection at node (i,j) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_p0m2 = 0.d0
                            else
                                ! use Neumann conditions !
                                u_p0m2 = u_p0m1
                            end if 
                        else if (lmsk(i,j-2) .or. lmsk(i-1,j-2)) then 
                            ! Check for land connection at node (i,j-1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_p0m2 = 0.d0
                            else
                                ! use Neumann conditions !
                                u_p0m2 = u_p0m1
                            end if
                        else
                            ! Check if u(i,j-2) is in ice !
                            if (.not. inIce('upnt', dist_T, dist_N, i, j-2, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                u_p0m2 = u_p0m1
                            else 
                                u_p0m2 = ugrid(i,j-2)
                            end if 
                        end if 

                    ! Mask u(i,j+2) !
                        if (lmsk(i,j+1) .or. lmsk(i-1,j+1)) then 
                            ! Check for land connection at node (i,j+1) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_p0p2 = 0.d0
                            else
                                ! use Neumann conditions !
                                u_p0p2 = u_p0p1
                            end if 
                        else if (lmsk(i,j+2) .or. lmsk(i-1,j+2)) then 
                            ! Check for land connection at node (i,j+2) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+2, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                u_p0p2 = 0.d0
                            else
                                ! use Neumann conditions !
                                u_p0p2 = u_p0p1
                            end if 
                        else
                            ! Check if u(i,j+2) is in ice !
                            if (.not. inIce('upnt', dist_T, dist_N, i, j+2, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                u_p0p2 = u_p0p1
                            else 
                                u_p0p2 = ugrid(i,j+2)
                            end if
                        end if 

                    ! Calc Derivs - Note that the dx**4 is included in lambda above !
                    u_xxxx = u_p2p0 - 4*u_p1p0 + 6*ugrid(i,j) - 4*u_m1p0 + u_m2p0
                    u_yyyy = u_p0p2 - 4*u_p0p1 + 6*ugrid(i,j) - 4*u_p0m1 + u_p0m2

                else

                    ! Calc Derivs - Note that the dx**4 is included in lambda above !
                    u_xxxx = ugrid(i+2,j) - 4*ugrid(i+1,j) + 6*ugrid(i,j) - 4*ugrid(i-1,j) + ugrid(i-2,j)
                    u_yyyy = ugrid(i,j+2) - 4*ugrid(i,j+1) + 6*ugrid(i,j) - 4*ugrid(i,j-1) + ugrid(i,j-2)

                end if 

                !=======================!
                !   Smooth velocity     !
                !=======================!
                    ugrid_tmp(i,j) = ugrid(i,j) + lmbda*(u_xxxx + u_yyyy)
            end do 

        !=======================!
        !   smooth v solution   !
        !=======================!
            do ij = 1,nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                ! interpolate distance fnctn !
                dist = (dist_T(i,j) + dist_T(i,j-1))/2

                if (dist < 3*dx) then 
                    ! Check for masking !

                    ! Masks points with pre-built masks !
                    v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*vgrid(i,j))  
                    v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*vgrid(i,j))
                    v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*vgrid(i,j))
                    v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*vgrid(i,j)) 

                    ! Mask v(i,j-2) !
                        if (lmsk(i,j-2)) then 
                            ! Check for land connection at v(i,j-1) !
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j-1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_p0m2 = 0.d0
                            else 
                                ! use Neumann conditions !
                                v_p0m2 = v_p0m1 
                            end if 
                        else if (lmsk(i,j-3)) then 
                            ! Check for land connection at v(i,j-2) !
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j-2, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_p0m2 = 0.d0
                            else 
                                ! use Neumann conditions !
                                v_p0m2 = v_p0m1 
                            end if
                        else
                            ! Check if v(i,j-2) is in ice !
                            if (.not. inIce('vpnt', dist_T, dist_N, i, j-2, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                v_p0m2 = v_p0m1 
                            else 
                                v_p0m2 = vgrid(i,j-2)
                            end if 
                        end if 

                    ! Mask v(i,j+2) !
                        if (lmsk(i,j+1)) then 
                            ! Check for land connection at v(i,j+1) !
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+1, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_p0p2 = 0.d0
                            else
                                ! use Neumann conditions !
                                v_p0p2 = v_p0p1
                            end if  
                        else if (lmsk(i,j+2)) then 
                            ! Check for land connection at v(i,j+2) !
                            if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j+2, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_p0p2 = 0.d0
                            else
                                ! use Neumann conditions !
                                v_p0p2 = v_p0p1
                            end if 
                        else 
                            ! Check if v(i,j+2) is in ice !
                            if (.not. inIce('vpnt', dist_T, dist_N, i, j+2, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                v_p0p2 = v_p0p1 
                            else 
                                v_p0p2 = vgrid(i,j+2)
                            end if 
                        end if

                    ! Mask v(i-2,j) !
                        if (lmsk(i-1,j) .or. lmsk(i-1,j-1)) then 
                            ! Check for land connection at node (i,j) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_m2p0 = 0.d0
                            else
                                ! use Neumann conditions !
                                v_m2p0 = v_m1p0
                            end if
                        else if (lmsk(i-2,j) .or. lmsk(i-2,j-1)) then 
                            ! Check for land connection at node (i-1,j) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i-1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_m2p0 = 0.d0
                            else
                                ! use Neumann conditions !
                                v_m2p0 = v_m1p0
                            end if
                        else
                            ! Check if v(i-2,j) is in ice !
                            if (.not. inIce('vpnt', dist_T, dist_N, i-2, j, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                v_m2p0 = v_m1p0 
                            else 
                                v_m2p0 = vgrid(i-2,j)
                            end if 
                        end if 

                    ! Mask v(i+2,j) !
                        if (lmsk(i+1,j) .or. lmsk(i+1,j-1)) then 
                            ! Check for land connection at node (i+1,j) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+1, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_p2p0 = 0.d0 
                            else 
                                ! use Neumann conditions !
                                v_p2p0 = v_p1p0 
                            end if 
                        else if (lmsk(i+2,j) .or. lmsk(i+2,j-1)) then 
                            ! Check for land connection at node (i+2,j) !
                            if (uselandBC('node', dist_T, dist_N, dist_gx_T, dist_gy_T, &
                                ugrid, vgrid, i+2, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                                ! use Dirichlet conditions !
                                v_p2p0 = 0.d0 
                            else 
                                ! use Neumann conditions !
                                v_p2p0 = v_p1p0 
                            end if 
                        else
                            ! Check if v(i+2,j) is in ice !
                            if (.not. inIce('vpnt', dist_T, dist_N, i+2, j, nxT, nyT, nxN, nyN)) then 
                                ! use Neumann conditions !
                                v_p2p0 = v_p1p0 
                            else 
                                v_p2p0 = vgrid(i+2,j)
                            end if
                        end if 

                    ! Calc Derivs - Note that the dx**4 is included in lambda above !
                    v_xxxx = v_p2p0 - 4*v_p1p0 + 6*vgrid(i,j) - 4*v_m1p0 + v_m2p0
                    v_yyyy = v_p0p2 - 4*v_p0p1 + 6*vgrid(i,j) - 4*v_p0m1 + v_p0m2

                else

                    ! Calc Derivs - Note that the dx**4 is included in lambda above !
                    v_xxxx = vgrid(i+2,j) - 4*vgrid(i+1,j) + 6*vgrid(i,j) - 4*vgrid(i-1,j) + vgrid(i-2,j)
                    v_yyyy = vgrid(i,j+2) - 4*vgrid(i,j+1) + 6*vgrid(i,j) - 4*vgrid(i,j-1) + vgrid(i,j-2)

                end if 

                !=======================!
                !   Smooth Velocity     !
                !=======================!
                    vgrid_tmp(i,j) = vgrid(i,j) + lmbda*(v_xxxx + v_yyyy)
            end do 

                !===============================!
                !   Store Smoothed Solution     !
                !===============================!
                    ugrid = ugrid_tmp
                    vgrid = vgrid_tmp

        !===============================!
        !   Saved Smoothed Solution     !
        !===============================!
            ! if (mod(time, sv_interval) == 0) then 
            !     open(unit = 1, file = './output/smoothed_u_time'//trim(t_lvl)//'.dat', status = 'unknown')
            !     open(unit = 2, file = './output/smoothed_v_time'//trim(t_lvl)//'.dat', status = 'unknown')
            !     call csv_write_dble_2d(1, ugrid)
            !     call csv_write_dble_2d(2, vgrid)
            !     close(1)
            !     close(2)
            ! end if

    end subroutine SmoothUV

!===========================================================================================!
!                               Advection_AH                                                !
!===========================================================================================!
    subroutine AdvectTracers(time, ugrid, vgrid, dist_T, dist_gx_T, dist_gy_T, dist_N, h_T, A_T, &
                            indxTi, indxTj, indxAdvVi, indxAdvVj, nT_pnts, nAdvVols, &
                            ulmsk, uimsk, vlmsk, vimsk, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)
        !====================================================================
        ! This subroutine advects the following T-point variables using the 
        ! velocities determined by the JFNK solver:
        !   - h     (ice thickness)
        !   - A     (ice area fraction)
        !   - phi   (distance function)
        !
        ! According to:
        !
        !   dh/dt + nabla dot (uh) = S_h 
        !   dA/dt + nabla dot (uA) = S_A 
        !   dphi/dt + (u dot nabla) phi = 0
        !
        ! With homogenous dirichlet boundary conditions on A and h; BC's aren't 
        ! required for phi. For now, the source/sink terms are set to 0.
        !
        ! For A and h, this routine uses the non-oscillatory central differencing scheme 
        ! described in, "Non-oscillatory Central Differencing for Hypberbolic Conservation
        ! Laws - Haim Nessyahu and Eitan Tadmor (1990)" and used in, "A Non-oscillatory 
        ! balanced scheme for an idealized tropical climate model (Pt. II) - Boualem Khouider 
        ! and Andrew Majda (2004)"
        !
        ! For phi, a similar routine is used, but slightly modified as phi is 
        ! not conserved; info on the modification can be found in: Central Schemes for 
        ! Balance Laws - G. Russo"
        !
        ! For this scheme we can't limit the staggered volume to node points in the 
        ! computational domain as some needed volumes will be just outside the ice 
        ! boundary (the locations are stored in indxAdvV* arrays). Additionally
        ! we need an "Advection Halo" of tracer points that aren't in ice. They 
        ! surround the ice and depending on the new velocities may be updated.
        !
        ! Note that this routine also corrects the distance function after it 
        ! is advected; the correct_dist routine is located in the domain_routines module.
        !====================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxU, nyU,   &   ! u-point grid size 
            nxV, nyV,   &   ! v-point grid size
            nxT, nyT,   &   ! T-point grid size
            nxN, nyN,   &   ! N-point grid size 
            nT_pnts,    &   ! number of tracers in the computational domain (inside ice)
            nAdvVols        ! number of Advection Volumes needed (at node points) 
        integer(kind = int_kind), dimension (nT_pnts), intent (in) :: &
            indxTi, indxTj          ! locations of tracer points in comp domain
        integer(kind = int_kind), dimension (nAdvVols), intent (in) :: &
            indxAdvVi, indxAdvVj    ! locations of advection volumes 
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk            ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk            ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                            ! current time level
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                           ! v velocity on the grid

        !====================!
        !------ In/out ------!
        !====================!
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            dist_T, dist_gx_T, dist_gy_T,   &   ! distance function variables
            h_T, A_T                            ! ice thickness and area fraction 
        real(kind = dbl_kind), dimension (nxN, nyN), intent(inout) :: &
            dist_N

        !====================!
        !---- Local Vars ----!
        !====================!
        character(len = 8) :: &
            fmt, t_lvl                  ! for attaching the time level to file names
        integer(kind = int_kind) :: &
            i, j, ii, jj, ij, cnt,  &   ! local indices and counters 
            nAdvHalo                    ! number of tracer points in the Advection Halo 
        integer(kind = int_kind), dimension (nT_pnts) :: &
            indxAdvHi, indxAdvHj        ! will store locations for Advection Halo Tracer points
                                        ! note that nT_pnts is used to initially set the size for these arrays. Unless something is 
                                        ! seriously wrong, these arrays shouldn't get close to filling this amount. 

        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            u_T,    v_T,    &   ! velocities at tracer points 
            ux_T,   vy_T,   &   ! needed velocity derivs at tracer points 
            hx_T,   hy_T,   &   ! slopes for thickness  (NOTE: these slopes haven't been divided by dx, they are just differences.)
            Ax_T,   Ay_T,   &   ! slopes for area fraction 
            dstx_T, dsty_T      ! slopes for distance function  (NOTE: this may not be needed, we could use the existing gradient values, but those weren't calculated
                                !   using the MC limiter and have been divided by dx already)
        real(kind = dbl_kind), dimension (nxN, nyN) :: &
            h_stg, A_stg, dist_stg,     &   ! staggered high resolution averages (Node points)
            h_stgx, h_stgy,             &   ! staggered slopes for h
            A_stgx, A_stgy,             &   ! staggered slopes for A 
            dist_stgx, dist_stgy            ! staggered slopes for the distance function 
        real(kind = dbl_kind) :: &
            lmbd2, mu                       ! useful constants

        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            AdvHalo
        ! logical, dimension (nxN, nyN) :: &
        !     AdvHaloN    

        !=========!
        ! Set fmt !
        !=========!
            fmt = '(I8.8)'
            write (t_lvl, fmt) int(time) 

        !================================!
        ! Initialize necessary variables !
        !================================!
            AdvHalo     = .false.
            nAdvHalo    = 0
            indxAdvHi   = 0
            indxAdvHj   = 0
            ! AdvHaloN    = .false.
            ! nAdvHaloN   = 0
            ! indxAdvHNi  = 0
            ! indxAdvHNj  = 0

            u_T     = 0.d0
            v_T     = 0.d0
            ux_T    = 0.d0
            vy_T    = 0.d0
            hx_T    = 0.d0
            hy_T    = 0.d0
            Ax_T    = 0.d0
            Ay_T    = 0.d0
            dstx_T  = 0.d0
            dsty_T  = 0.d0

            h_stg   = 0.d0
            A_stg   = 0.d0
            dist_stg= 0.d0
            h_stgx  = 0.d0
            h_stgy  = 0.d0
            A_stgx  = 0.d0 
            A_stgy  = 0.d0
            dist_stgx   = 0.d0
            dist_stgy   = 0.d0

            lmbd2   = dt/(2*dx)
            mu      = dt/(4*dx) ! Note that this is divided by dx since the corrector step for phi just uses velocity differences (uxT and vyT)

        !=========================!
        ! Locate "Advection Halo" !
        !=========================!
            ! NOTE: there may be a more efficient method to set this, but I currently don't have the time to fully optimize it 

            ! first flag tracers points by looping over advection volumes !
            do ij = 1, nAdvVols
                i = indxAdvVi(ij)
                j = indxAdvVj(ij)

                ! flag tracers !
                do ii = i-1,i 
                    do jj = j-1,j 
                        if (dist_T(ii,jj) < 0) then 
                            ! tracer isn't in ice but will be needed for calculations -> set flag !
                            AdvHalo(ii,jj) = .true. 
                        end if 
                    end do 
                end do 
            end do 

            ! count activated flags and store locations !
            do i = 0, nxT+1
                do j = 0, nyT+1
                    if (AdvHalo(i,j)) then 
                        nAdvHalo = nAdvHalo + 1
                        indxAdvHi(nAdvHalo) = i 
                        indxAdvHj(nAdvHalo) = j 
                    end if 
                end do 
            end do 

                ! haven't inspected this too much !

        !================================================!
        ! Set Tracer Velocities and Velocity Derivatives !
        !================================================!
            call set_Tvels(time, u_T, v_T, ux_T, vy_T, ugrid, vgrid, dist_T, dist_gx_T, dist_gy_T, dist_N, &
                            ulmsk, uimsk, vlmsk, vimsk, indxTi, indxTj, indxAdvHi, indxAdvHj, &
                            nT_pnts, nAdvHalo, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        !=============================================!
        ! Calculate slopes at tracers at time level t !
        !=============================================!
            call calc_slopes('Tpnt', h_T, hx_T, hy_T)
            call calc_slopes('Tpnt', A_T, Ax_T, Ay_T)
            call calc_slopes('Tpnt', dist_T, dstx_T, dsty_T)

        !==================================================!
        ! Calculate staggered cell averages at node points !
        !==================================================!

            ! Throughout grid !
            do i = 1, nxN
                do j = 1, nyN

                    ! Area Fraction and Thickness !
                    if (lmsk(i-1,j-1) .or. lmsk(i-1,j) .or. lmsk(i,j-1) .or. lmsk(i,j)) then 
                        ! node point is on the corner of a land cell !

                        h_stg(i,j) =        (h_T(i-1,j-1) + h_T(i-1,j) + h_T(i,j-1) + h_T(i,j))/4       &
                                        +   (hx_T(i-1,j-1) + hx_T(i-1,j) - hx_T(i,j-1) - hx_T(i,j))/16  &
                                        +   (hy_T(i-1,j-1) - hy_T(i-1,j) + hy_T(i,j-1) - hy_T(i,j))/16

                        A_stg(i,j) =        (A_T(i-1,j-1) + A_T(i-1,j) + A_T(i,j-1) + A_T(i,j))/4       &
                                        +   (Ax_T(i-1,j-1) + Ax_T(i-1,j) - Ax_T(i,j-1) - Ax_T(i,j))/16  &
                                        +   (Ay_T(i-1,j-1) - Ay_T(i-1,j) + Ay_T(i,j-1) - Ay_T(i,j))/16

                        ! Count how many land cells surround the node point ! 
                            cnt = count([lmsk(i-1,j-1), lmsk(i-1,j), lmsk(i,j-1), lmsk(i,j)])

                        ! Adjust thickness/area fraction values such that land area isn't included !
                            h_stg(i,j) = 4*h_stg(i,j)/(4 - cnt)
                            A_stg(i,j) = 4*A_stg(i,j)/(4 - cnt)
                    else
                        h_stg(i,j) =        (h_T(i-1,j-1) + h_T(i-1,j) + h_T(i,j-1) + h_T(i,j))/4       &
                                        +   (hx_T(i-1,j-1) + hx_T(i-1,j) - hx_T(i,j-1) - hx_T(i,j))/16  &
                                        +   (hy_T(i-1,j-1) - hy_T(i-1,j) + hy_T(i,j-1) - hy_T(i,j))/16

                        A_stg(i,j) =        (A_T(i-1,j-1) + A_T(i-1,j) + A_T(i,j-1) + A_T(i,j))/4       &
                                        +   (Ax_T(i-1,j-1) + Ax_T(i-1,j) - Ax_T(i,j-1) - Ax_T(i,j))/16  &
                                        +   (Ay_T(i-1,j-1) - Ay_T(i-1,j) + Ay_T(i,j-1) - Ay_T(i,j))/16
                    end if

                    ! distance function !
                    dist_stg(i,j) =     (dist_T(i-1,j-1) + dist_T(i-1,j) + dist_T(i,j-1) + dist_T(i,j))/4   &
                                    +   (dstx_T(i-1,j-1) + dstx_T(i-1,j) - dstx_T(i,j-1) - dstx_T(i,j))/16  &
                                    +   (dsty_T(i-1,j-1) - dsty_T(i-1,j) + dsty_T(i,j-1) - dsty_T(i,j))/16
                end do 
            end do 

        !====================================================!
        ! Predictor step -> update tracer values to t + dt/2 !
        !====================================================!
            ! In computational domain (in ice) !
            do ij = 1, nT_pnts
                i = indxTi(ij)
                j = indxTj(ij)

                h_T(i,j) = h_T(i,j) - lmbd2*(u_T(i,j)*hx_T(i,j) + v_T(i,j)*hy_T(i,j) + h_T(i,j)*(ux_T(i,j) + vy_T(i,j)))
                A_T(i,j) = A_T(i,j) - lmbd2*(u_T(i,j)*Ax_T(i,j) + v_T(i,j)*Ay_T(i,j) + A_T(i,j)*(ux_T(i,j) + vy_T(i,j)))

                dist_T(i,j) = dist_T(i,j) - lmbd2*(u_T(i,j)*dstx_T(i,j) + v_T(i,j)*dsty_T(i,j))
            end do

            ! In advection halo !
            do ij = 1, nAdvHalo
                i = indxAdvVi(ij)
                j = indxAdvVj(ij)

                h_T(i,j) = h_T(i,j) - lmbd2*(u_T(i,j)*hx_T(i,j) + v_T(i,j)*hy_T(i,j) + h_T(i,j)*(ux_T(i,j) + vy_T(i,j)))
                A_T(i,j) = A_T(i,j) - lmbd2*(u_T(i,j)*Ax_T(i,j) + v_T(i,j)*Ay_T(i,j) + A_T(i,j)*(ux_T(i,j) + vy_T(i,j)))

                dist_T(i,j) = dist_T(i,j) - lmbd2*(u_T(i,j)*dstx_T(i,j) + v_T(i,j)*dsty_T(i,j))
            end do

        !====================================================!
        ! Calculate slopes at tracers at time level t + dt/2 !
        !====================================================!
            call calc_slopes('Tpnt', h_T, hx_T, hy_T)
            call calc_slopes('Tpnt', A_T, Ax_T, Ay_T)
            call calc_slopes('Tpnt', dist_T, dstx_T, dsty_T)

        !=======================================================!
        ! Update staggered cell averages using centered schemes !
        !=======================================================!

            ! Throughout the grid ! - NOTE: the dist fnctn isn't conserved, which is why this formula is different than the two for thickness and area!
            do i = 1, nxN
                do j = 1, nyN

                    ! Thickness !
                    h_stg(i,j) = h_stg(i,j) &
                                - lmbd2*(   u_T(i  ,j  )*h_T(i  ,j  ) + u_T(i  ,j-1)*h_T(i  ,j-1) &
                                        -   u_T(i-1,j  )*h_T(i-1,j  ) - u_T(i-1,j-1)*h_T(i-1,j-1)) &
                                - lmbd2*(   v_T(i-1,j  )*h_T(i-1,j  ) + v_T(i  ,j  )*h_T(i  ,j  ) &
                                        -   v_T(i-1,j-1)*h_T(i-1,j-1) - v_T(i  ,j-1)*h_T(i  ,j-1))
                    
                    ! Area Fraction !
                    A_stg(i,j) = A_stg(i,j) &
                                - lmbd2*(   u_T(i  ,j  )*A_T(i  ,j  ) + u_T(i  ,j-1)*A_T(i  ,j-1) &
                                        -   u_T(i-1,j  )*A_T(i-1,j  ) - u_T(i-1,j-1)*A_T(i-1,j-1)) &
                                - lmbd2*(   v_T(i-1,j  )*A_T(i-1,j  ) + v_T(i  ,j  )*A_T(i  ,j  ) &
                                        -   v_T(i-1,j-1)*A_T(i-1,j-1) - v_T(i  ,j-1)*A_T(i  ,j-1))

                    dist_stg(i,j) = dist_stg(i,j) &
                            - lmbd2*(   u_T(i  ,j  )*dist_T(i  ,j  ) + u_T(i  ,j-1)*dist_T(i  ,j-1) &
                                    -   u_T(i-1,j  )*dist_T(i-1,j  ) - u_T(i-1,j-1)*dist_T(i-1,j-1)) &
                            - lmbd2*(   v_T(i-1,j  )*dist_T(i-1,j  ) + v_T(i  ,j  )*dist_T(i  ,j  ) &
                                    -   v_T(i-1,j-1)*dist_T(i-1,j-1) - v_T(i  ,j-1)*dist_T(i  ,j-1)) &
                            + mu*(      dist_T(i  ,j  )*(ux_T(i  ,j  ) + vy_T(i  ,j  )) &
                                    +   dist_T(i-1,j  )*(ux_T(i-1,j  ) + vy_T(i-1,j  )) &
                                    +   dist_T(i-1,j-1)*(ux_T(i-1,j-1) + vy_T(i-1,j-1)) &
                                    +   dist_T(i  ,j-1)*(ux_T(i  ,j-1) + vy_T(i  ,j-1)))
                end do 
            end do 

        !=====================================================! 
        ! Calculate slopes at advection volumes - node points !
        !=====================================================!
            call calc_slopes('Npnt', h_stg, h_stgx, h_stgy)
            call calc_slopes('Npnt', A_stg, A_stgx, A_stgy)
            call calc_slopes('Npnt', dist_stg, dist_stgx, dist_stgy)

        !==================================================!
        ! Update distance function values at Tracer points !
        !==================================================!
            ! Across grid !
            do i = 1,nxT
                do j = 1,nyT
                    if (lmsk(i,j)) then 
                        ! don't reconstruct !
                    else
                        dist_T(i,j) =       (dist_stg(i,j) + dist_stg(i,j+1) + dist_stg(i+1,j) + dist_stg(i+1,j+1))/4   &
                                        +   (dist_stgx(i,j) + dist_stgx(i,j+1) - dist_stgx(i+1,j) - dist_stgx(i+1,j+1))/16  &
                                        +   (dist_stgy(i,j) - dist_stgy(i,j+1) + dist_stgy(i+1,j) - dist_stgy(i+1,j+1))/16
                    end if 
                end do 
            end do 

        !=========================================!
        ! Correct distance function or calc grads !
        !=========================================!
            if (dist_correct .and. (mod(time, distcrrctn*dt) == 0)) then 
                call correct_dist(time,dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN)     ! in domain_routines
            else
                call calcDistGrad_DistNpnts(dist_T, dist_gx_T, dist_gy_T, dist_N, nxT, nyT, nxN, nyN) 
            end if 

        !=======================================================================!
        ! Update A and h values in locations specified by the distance function !
        !=======================================================================!
            ! NOTE: we should maybe make it so we update values if dist_T >= -dx/2
            ! In comp domain (from previous time-step) !
            do ij = 1, nT_pnts
                i = indxTi(ij)
                j = indxTj(ij)

                ! Only update if in ice !
                if (dist_T(i,j) < -dx/sqrt(2.)) then 
                    ! T point isn't in ice set h_T, A_T = 0 !
                    h_T(i,j) = 0.d0
                    A_T(i,j) = 0.d0
                else 
                    h_T(i,j) =      (h_stg(i,j) + h_stg(i,j+1) + h_stg(i+1,j) + h_stg(i+1,j+1))/4   &
                                +   (h_stgx(i,j) + h_stgx(i,j+1) - h_stgx(i+1,j) - h_stgx(i+1,j+1))/16  &
                                +   (h_stgy(i,j) - h_stgy(i,j+1) + h_stgy(i+1,j) - h_stgy(i+1,j+1))/16

                    A_T(i,j) =      (A_stg(i,j) + A_stg(i,j+1) + A_stg(i+1,j) + A_stg(i+1,j+1))/4   &
                                +   (A_stgx(i,j) + A_stgx(i,j+1) - A_stgx(i+1,j) - A_stgx(i+1,j+1))/16  &
                                +   (A_stgy(i,j) - A_stgy(i,j+1) + A_stgy(i+1,j) - A_stgy(i+1,j+1))/16

                    A_T(i,j) = min(A_T(i,j),1.d0)
                    A_T(i,j) = max(A_T(i,j),0.d0)
                    h_T(i,j) = max(h_T(i,j),0.d0)
                end if 
            end do 

            ! In advection halo !
            do ij = 1, nAdvHalo
                i = indxAdvHi(ij)
                j = indxAdvHj(ij)

                ! Only update if in ice !
                if (lmsk(i,j)) then 
                    ! T point is a land cell. Set values to 0 !
                    h_T(i,j) = 0.d0
                    A_T(i,j) = 0.d0

                else if (dist_T(i,j) < -dx/sqrt(2.)) then 
                    ! no ice should be in the cell !
                    h_T(i,j) = 0.d0
                    A_T(i,j) = 0.d0
                else 
                    h_T(i,j) =      (h_stg(i,j) + h_stg(i,j+1) + h_stg(i+1,j) + h_stg(i+1,j+1))/4   &
                                +   (h_stgx(i,j) + h_stgx(i,j+1) - h_stgx(i+1,j) - h_stgx(i+1,j+1))/16  &
                                +   (h_stgy(i,j) - h_stgy(i,j+1) + h_stgy(i+1,j) - h_stgy(i+1,j+1))/16

                    A_T(i,j) =      (A_stg(i,j) + A_stg(i,j+1) + A_stg(i+1,j) + A_stg(i+1,j+1))/4   &
                                +   (A_stgx(i,j) + A_stgx(i,j+1) - A_stgx(i+1,j) - A_stgx(i+1,j+1))/16  &
                                +   (A_stgy(i,j) - A_stgy(i,j+1) + A_stgy(i+1,j) - A_stgy(i+1,j+1))/16

                    A_T(i,j) = min(A_T(i,j),1.d0)
                    A_T(i,j) = max(A_T(i,j),0.d0)
                    h_T(i,j) = max(h_T(i,j),0.d0)
                end if 
            end do 

        !===========================================================================================!
        !   Check if there are points with phi > - dx/2 and h or A = 0, and correct dist if needed  ! 
        !===========================================================================================!
            ! Note: this check is done twice as often as the normal distance function correction !
            if (mod(time, int(distcrrctn/2)*dt) == 0) then 
                call correct_boundary(time, h_T, A_T, dist_T, dist_N, dist_gx_T, dist_gy_T, nxT, nyT, nxN, nyN)
            end if 

    end subroutine AdvectTracers

!===========================================================================================!
!                              calcDistGrad_DistNPnts                                       !
!===========================================================================================!
    subroutine calcDistGrad_DistNpnts(dist_T, dist_gx_T, dist_gy_T, dist_N, nxT, nyT, nxN, nyN)
        !====================================================================
        ! This routine calculates the gradients of the distance function 
        ! and populates dist_N through a bi-linear approximation of dist_T.
        !
        ! Note, at a later time it may be better to replace dist_N with the 
        ! high-resolution interpolant; I've avoided this to remain consistent 
        ! with other calculations of dist_N. 
        !====================================================================

        !==================!
        ! ------ In ------ !
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxT, nyT, nxN, nyN      ! index limits for the T and N grid 
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (in) :: &
            dist_T                  ! distance function on the T grid 

        !==================!
        ! ---- In/out ---- !
        !==================!
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (inout) :: &
            dist_gx_T, dist_gy_T    ! gradients of the distance function at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent (inout) :: &
            dist_N                  ! distance function at node points 

        !===============!
        ! --- Local --- !
        !===============!
        integer(kind = int_kind) :: &
            i, j
        real(kind = dbl_kind) :: &
            a, b, c, d              ! will hold the point wise derivatives 

        !===================================!
        !   Calculate Gradient Components   !
        !===================================!
            do i = 1-gc_T, nxT+gc_T
                do j = 1-gc_T, nyT+gc_T
                    !==================================!
                    ! X derivatives - considering BC's !
                    !==================================!
                        if (i == 1-gc_T) then
                            a = (dist_T(i+1, j) - dist_T(i, j))/dx
                        else
                            a = (dist_T(i, j) - dist_T(i-1, j))/dx
                        endif

                        if (i == nxT+gc_T) then 
                            b = (dist_T(i, j) - dist_T(i-1, j))/dx
                        else
                            b = (dist_T(i+1, j) - dist_T(i, j))/dx
                        endif   

                    !==================================!
                    ! Y derivatives - considering BC's !
                    !==================================!
                        if (j == 1-gc_T) then 
                            c = (dist_T(i, j+1) - dist_T(i, j))/dx
                        else
                            c = (dist_T(i, j) - dist_T(i, j-1))/dx
                        endif

                        if (j == nyT+gc_T) then
                            d = (dist_T(i, j) - dist_T(i, j-1))/dx
                        else
                            d = (dist_T(i, j+1) - dist_T(i, j))/dx
                        endif

                    !=============================================================!
                    ! Store Components - Note that these are centered differences !
                    !=============================================================!
                        dist_gx_T(i,j) = (a+b)/2
                        dist_gy_T(i,j) = (c+d)/2
                end do 
            end do 

        !======================================!
        ! Calc distance fnction at node points !
        !======================================!
            do i = 1, nxN
                do j = 1, nyN
                    dist_N(i,j) = (dist_T(i,j) + dist_T(i,j-1) + dist_T(i-1,j-1) + dist_T(i-1,j))/4
                end do 
            end do
    end subroutine calcDistGrad_DistNpnts

!===========================================================================================!
!                                   set_Tvels                                               !
!===========================================================================================!
    subroutine set_Tvels(time, u_T, v_T, ux_T, vy_T, ugrid, vgrid, dist_T, dist_gx_T, dist_gy_T, dist_N, &
                        ulmsk, uimsk, vlmsk, vimsk, indxTi, indxTj, indxAdvHi, indxAdvHj, &
                        nT_pnts, nAdvHalo, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)
        !====================================================================
        ! This subroutine sets the velocities u_T, v_T and their derivatives 
        ! at Tracer points needed for the advection step.
        !
        ! It first loops through the computational domain and sets the values 
        ! at tracer points that are located in ice. To deal with boundaries, it 
        ! uses the inIce() routine (domain_routines.f90) to determine which 
        ! neighbouring points are in ice and then uses the masks associated with 
        ! that velocity point. See create_comp_msk_dist() routine for info on how
        ! the masks are set. It is assumed that at least one neighbouring u point '
        ! and one v point are in ice. 
        !
        ! It then sets the values at tracer points that are located in the 
        ! "Advection Halo". These points are not in ice and it is not garunteed
        ! that at least one u and v point are located in ice; to deal with this 
        ! and properly deal with BCs, it uses the distance function gradients to 
        ! locate the ice front.
        !
        ! Note that this routine may not be fully optimized, but as this is 
        ! only called once per advection step, it is not a problem.
        !====================================================================

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxU, nyU, nxV, nyV,     &   ! index limits for u and v grids 
            nxT, nyT, nxN, nyN,     &   ! index limits for T and N grids 
            nT_pnts,                &   ! number of tracer points in the computational domain 
            nAdvHalo                    ! number of tracer points in the advection halo 
        integer(kind = int_kind), dimension (nT_pnts), intent (in) :: &
            indxTi, indxTj              ! locations of Tracer points in comp. domain 
        integer(kind = int_kind), dimension (nAdvHalo), intent (in) :: &
            indxAdvHi, indxAdvHj        ! locations of Tracer points in advection halo 
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent (in) :: &
            ulmsk, uimsk                ! computational masks for u-points 
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent (in) :: &
            vlmsk, vimsk                ! computational masks for v-points 

        real(kind = dbl_kind), intent (in) :: &
            time 
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent (in) :: &
            ugrid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent (in) :: &
            vgrid
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (in) :: &
            dist_T,                 &   ! distance function values (tracer points)
            dist_gx_T, dist_gy_T        ! distance function gradients (tracer points)
        real(kind = dbl_kind), dimension (nxN, nyN), intent (in) :: &
            dist_N                      ! distance function values (node points)

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (inout) :: &
            u_T,    v_T,    &   ! interpolated velocities at T points
            ux_T,   vy_T        ! centered derivs at T points 

        !==================!
        !------ Local -----!
        !==================!
        character (len = 8) :: &
            fmt1, fmt2, dt_s, dx_s, time_s  ! strings for ouputting files 
        integer(kind = int_kind) :: &
            i, j, ij, ii, jj,   &   ! local indices and counters
            dir, xdir, ydir,    &   ! define various directions when looking at surrounding points 
            velpnt_dir 
        real(kind = dbl_kind) :: &
            u_p0p0, u_p1p0,     &   ! masked velocities
            v_p0p0, v_p0p1 
        logical :: &
            uWice, uEice,   &   ! states if the u points to the east or west of the tracer point are in ice 
            vNice, vSice        ! states if the v points to the north or south of the tracer point are in ice 

        !=======================!
        ! Formats for filenames !
        !=======================!
            fmt1 = '(I4.4)'
            fmt2 = '(I8.8)'
            write (dt_s, fmt1) int(dt)
            write (dx_s, fmt1) int(dx/1000)
            write (time_s, fmt2) int(time) 

        !========================================!
        ! Set values in the computational domain !
        !========================================!
            do ij = 1, nT_pnts
                i = indxTi(ij)
                j = indxTj(ij)

                if (dist_T(i,j) <= dx/2) then 
                ! tracer is close to a boundary - check which surrounding u points are in ice !
                    if (inIce('upnt', dist_T, dist_N, i, j, nxT, nyT, nxN, nyN)) then 
                    ! use masks at u(i,j) !
                        ii = i
                        jj = j

                        u_p0p0 = ugrid(i,j)
                        u_p1p0 = ulmsk(ii,jj,3)*(uimsk(ii,jj,3)*ugrid(i+1,j  ) + (1 - uimsk(ii,jj,3))*ugrid(i  ,j  ))

                        v_p0p0 = ulmsk(ii,jj,6)*(uimsk(ii,jj,6)*vgrid(i  ,j  ) + uimsk(ii,jj,8)*(1 - uimsk(ii,jj,6))*vgrid(i  ,j+1) + &
                                                    (1 - uimsk(ii,jj,8))*(1 - uimsk(ii,jj,6))*vgrid(i-1,j  ))

                        v_p0p1 = ulmsk(ii,jj,8)*(uimsk(ii,jj,8)*vgrid(i  ,j+1) + uimsk(ii,jj,6)*(1 - uimsk(ii,jj,8))*vgrid(i  ,j  ) + &
                                                    (1 - uimsk(ii,jj,6))*(1 - uimsk(ii,jj,8))*vgrid(i-1,j+1))

                    else if (inIce('upnt', dist_T, dist_N, i+1, j, nxT, nyT, nxN, nyN)) then 
                    ! use masks at u(i+1,j) !
                        ii = i+1
                        jj = j 

                        u_p0p0 = ulmsk(ii,jj,2)*(uimsk(ii,jj,2)*ugrid(i  ,j  ) + (1 - uimsk(ii,jj,2))*ugrid(i+1,j  ))                         
                        u_p1p0 = ugrid(i+1,j)

                        v_p0p0 = ulmsk(ii,jj,5)*(uimsk(ii,jj,5)*vgrid(i  ,j  ) + uimsk(ii,jj,7)*(1 - uimsk(ii,jj,5))*vgrid(i  ,j+1) + &
                                                    (1 - uimsk(ii,jj,7))*(1 - uimsk(ii,jj,5))*vgrid(i+1,j  ))

                        v_p0p1 = ulmsk(ii,jj,7)*(uimsk(ii,jj,7)*vgrid(i  ,j+1) + uimsk(ii,jj,5)*(1 - uimsk(ii,jj,7))*vgrid(i  ,j  ) + &
                                                    (1 - uimsk(ii,jj,5))*(1 - uimsk(ii,jj,7))*vgrid(i+1,j+1))

                    else if (inIce('vpnt', dist_T, dist_N, i, j, nxT, nyT, nxN, nyN)) then 
                    ! use masks at v(i,j) !
                        ii = i 
                        jj = j

                        u_p0p0 = vlmsk(ii,jj,3)*(vimsk(ii,jj,3)*ugrid(i  ,j  ) + vimsk(ii,jj,4)*(1 - vimsk(ii,jj,3))*ugrid(i+1,j  ) + &
                                                    (1 - vimsk(ii,jj,4))*(1 - vimsk(ii,jj,3))*ugrid(i  ,j-1))

                        u_p1p0 = vlmsk(ii,jj,4)*(vimsk(ii,jj,4)*ugrid(i+1,j  ) + vimsk(ii,jj,3)*(1 - vimsk(ii,jj,4))*ugrid(i  ,j  ) + &
                                                    (1 - vimsk(ii,jj,3))*(1 - vimsk(ii,jj,4))*ugrid(i+1,j-1)) 

                        v_p0p0 = vgrid(i,j)
                        v_p0p1 = vlmsk(ii,jj,8)*(vimsk(ii,jj,8)*vgrid(i+1,j  ) + (1 - vimsk(ii,jj,8))*vgrid(i  ,j  ))

                    else if (inIce('vpnt', dist_T, dist_N, i, j+1, nxT, nyT, nxN, nyN)) then 
                    ! use masks at v(i,j+1) !
                        ii = i 
                        jj = j+1

                        u_p0p0 = vlmsk(ii,jj,1)*(vimsk(ii,jj,1)*ugrid(i  ,j  ) + vimsk(ii,jj,2)*(1 - vimsk(ii,jj,1))*ugrid(i+1,j  ) + &
                                                    (1 - vimsk(ii,jj,2))*(1 - vimsk(ii,jj,1))*ugrid(i  ,j+1))

                        u_p1p0 = vlmsk(ii,jj,2)*(vimsk(ii,jj,2)*ugrid(i+1,j  ) + vimsk(ii,jj,1)*(1 - vimsk(ii,jj,2))*ugrid(i  ,j  ) + &
                                                    (1 - vimsk(ii,jj,1))*(1 - vimsk(ii,jj,2))*ugrid(i+1,j+1)) 

                        v_p0p0 = vlmsk(ii,jj,5)*(vimsk(ii,jj,5)*vgrid(i  ,j  ) + (1 - vimsk(ii,jj,5))*vgrid(i  ,j+1))
                        v_p0p1 = vgrid(i,j+1)

                    else
                        print *, "In setting tracer velocities, at points in ice, the routine may have failed as masking velocities!"
                        print *, "Look at Tracer", i, j
                        print *, "Additional consideration may be needed in setting velocities for the advection step."
                    end if 

                    ! calculate interpolated velocities and velocity differences ! 
                    u_T(i,j)    = (u_p0p0 + u_p1p0)/2
                    v_T(i,j)    = (v_p0p0 + v_p0p1)/2
                    ux_T(i,j)   = (u_p1p0 - u_p0p0)
                    vy_T(i,j)   = (v_p0p1 - v_p0p0)
                else 
                ! tracer is far from boundaries - masking isn't needed !
                    ! calculate interpolated velocities and velocity differences ! 
                    u_T(i,j)    = (ugrid(i,j) + ugrid(i+1,j))/2
                    v_T(i,j)    = (vgrid(i,j) + vgrid(i,j+1))/2
                    ux_T(i,j)   = ugrid(i+1,j) - ugrid(i,j)
                    vy_T(i,j)   = vgrid(i,j+1) - vgrid(i,j)
                end if 
            end do 

        !===============================!
        ! Save tracer velocities in ice !
        !===============================!
            if (mod(time, sv_interval) == 0) then
                open(unit = 1, file = './output/uT_dx'//trim(dx_s)//'dt'//trim(dt_s)//'time'//trim(time_s)//'.dat', status = 'unknown')
                open(unit = 2, file = './output/vT_dx'//trim(dx_s)//'dt'//trim(dt_s)//'time'//trim(time_s)//'.dat', status = 'unknown')
                call csv_write_dble_2d(1, u_T)
                call csv_write_dble_2d(2, v_T)
                close(1)
                close(2)
            end if 

        !==============================!
        ! Set values in Advection Halo !
        !==============================!
            do ij = 1, nAdvHalo
                i = indxAdvHi(ij)
                j = indxAdvHj(ij)

                ! set x and y directions pointing towards ice !
                xdir = int(sign(1.0, dist_gx_T(i,j)))
                ydir = int(sign(1.0, dist_gy_T(i,j)))

                if (lmsk(i,j)) then 
                ! cell is a land cell !

                    ! set derivatives to zero !
                    ux_T(i,j) = 0.d0
                    vy_T(i,j) = 0.d0

                    if (abs(dist_gx_T(i,j)) > abs(dist_gy_T(i,j))) then 
                        ! ice front is in the x-direction. Check for land connection !

                        if (xdir < 0) then 
                            velpnt_dir = 0 ! if xdir is negative, we look to u(i,j) due to the staggered grid
                        else
                            velpnt_dir = xdir 
                        end if

                        if (uselandBC('upnt', dist_T, dist_N, dist_gx_T, dist_gy_T, ugrid, vgrid, &
                                        i+velpnt_dir, j, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                            ! ice is connected, set velocities to 0 !

                            u_T(i,j) = 0.d0
                            v_T(i,j) = 0.d0 
                        else
                            ! ice isn't connected to land - use Neumann conditions !
                            ! Check for corner condition !
                            if (dist_T(i+xdir, j) < 0) then
                                ! look diagonally -> print warning if diagonal point isn't in ice !
                                if (dist_T(i+xdir,j+ydir) < 0) print *, "set_Tvels warning 1: problem setting velcocities in advection halo at Tracer:", i,j
                                u_T(i,j) = u_T(i+xdir, j+ydir)
                                v_T(i,j) = v_T(i+xdir, j+ydir)
                            else
                                ! look in the direction of xdir !
                                u_T(i,j) = u_T(i+xdir, j)
                                v_T(i,j) = v_T(i+xdir, j)
                            end if 
                        end if 
                    else 
                        ! ice front is in the y-direction. Check for land connection !

                        if (ydir < 0) then 
                            velpnt_dir = 0 ! if ydir is negative, we look to v(i,j) due to the staggered grid
                        else
                            velpnt_dir = ydir
                        end if 

                        if (uselandBC('vpnt', dist_T, dist_N, dist_gx_T, dist_gy_T, ugrid, vgrid, &
                                        i, j+velpnt_dir, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)) then 
                            ! ice is connected, set velocities to 0 !
                            u_T(i,j) = 0.d0
                            v_T(i,j) = 0.d0
                        else 
                            ! ice isn't connected to land - use Neumann conditions !
                            ! Check for corner condition !
                            if (dist_T(i, j+ydir) < 0) then 
                                ! look diagonally -> print warning if diagonal point isn't in ice !
                                if (dist_T(i+xdir,j+ydir) < 0) print *, "set_Tvels warning 2: problem setting velcocities in advection halo at Tracer:", i,j
                                u_T(i,j) = u_T(i+xdir, j+ydir)
                                v_T(i,j) = v_T(i+xdir, j+ydir)
                            else 
                                ! look in the direction of ydir !
                                u_T(i,j) = u_T(i, j+ydir)
                                v_T(i,j) = v_T(i, j+ydir)
                            end if
                        end if 
                    end if 
                else
                ! cell is an ocean cell !

                    ! set derivatives to zero !
                    ux_T(i,j) = 0.d0
                    vy_T(i,j) = 0.d0

                    ! Check if surrounding u and v points are in ice !
                    uEice = inIce('upnt', dist_T, dist_N, i+1, j, nxT, nyT, nxN, nyN)
                    uWice = inIce('upnt', dist_T, dist_N, i  , j, nxT, nyT, nxN, nyN) 
                    vNice = inIce('vpnt', dist_T, dist_N, i, j+1, nxT, nyT, nxN, nyN) 
                    vSice = inIce('vpnt', dist_T, dist_N, i, j  , nxT, nyT, nxN, nyN)

                    if (.not. (uEice .or. uWice)) then 
                        ! neither neighbouring u-points are in ice. set u_T from a neighbouring T point in ice !

                        ! start in x-direction and search for point in ice !
                        if (dist_T(i+xdir,j) >= 0) then 
                            u_T(i,j) = u_T(i+xdir,j)
                        else if (dist_T(i,j+ydir) >= 0) then
                            u_T(i,j) = u_T(i,j+ydir)
                        else if (dist_T(i-xdir,j) >= 0) then 
                            u_T(i,j) = u_T(i-xdir,j)
                        else if (dist_T(i,j-ydir) >= 0) then
                            u_T(i,j) = u_T(i,j-ydir)
                        else if (dist_T(i+xdir,j+ydir) >= 0) then 
                            u_T(i,j) = u_T(i+xdir,j+ydir)
                        else if (dist_T(i-xdir,j-ydir) >= 0) then
                            u_T(i,j) = u_T(i-xdir,j-ydir)
                        else if (dist_T(i+xdir,j-ydir) >= 0) then
                            u_T(i,j) = u_T(i+xdir,j-ydir)
                        else if (dist_T(i-xdir,j+ydir) >= 0) then
                            u_T(i,j) = u_T(i-xdir,j+ydir)
                        else
                            print *, "set_Tvels warning 3: problem setting velocities in advection halo at Tracer:", i,j
                        end if          
                    else
                        ! one point is in ice, set velocity according to that point !
                        ! Note there is a chance that both points will be in ice. If that is the case, the velocity at this tracer point 
                        ! should depend on what control volume we are considering. Nevertheless, this should have minimal affect but would 
                        ! require a fair amount of coding work to properly handle. For now we will just set it once and will output a warning 
                        ! if this situation occurs. 
                        if (uWice .and. uEice) print *, "WARNING: when setting velocities at tracer points in the advection halo, a situation occured when", &
                                                        " both neighbouring u points are in ice. This means that the Tracer point",i,j," is in a narrow lead.", &
                                                        " This may lead some interesting behaviour at this point."
                        if (uEice) u_T(i,j) = ugrid(i+1,j)
                        if (uWice) u_T(i,j) = ugrid(i,j)
                    end if 

                    if (.not. (vNice .or. vSice)) then 
                        ! neither neighbouring v-points are in ice. set v_T from the nearest T point in ice !

                        ! start in x-direction and search for point in ice !
                        if (dist_T(i+xdir,j) >= 0) then 
                            v_T(i,j) = v_T(i+xdir,j)
                        else if (dist_T(i,j+ydir) >= 0) then
                            v_T(i,j) = v_T(i,j+ydir)
                        else if (dist_T(i-xdir,j) >= 0) then 
                            v_T(i,j) = v_T(i-xdir,j)
                        else if (dist_T(i,j-ydir) >= 0) then
                            v_T(i,j) = v_T(i,j-ydir)
                        else if (dist_T(i+xdir,j+ydir) >= 0) then 
                            v_T(i,j) = v_T(i+xdir,j+ydir)
                        else if (dist_T(i-xdir,j-ydir) >= 0) then
                            v_T(i,j) = v_T(i-xdir,j-ydir)
                        else if (dist_T(i+xdir,j-ydir) >= 0) then
                            v_T(i,j) = v_T(i+xdir,j-ydir)
                        else if (dist_T(i-xdir,j+ydir) >= 0) then
                            v_T(i,j) = v_T(i-xdir,j+ydir)
                        else
                            print *, "set_Tvels warning 4: problem setting velocities in advection halo at Tracer:", i,j
                        end if
                              
                    else
                        ! one point is in ice
                        if (vNice .and. vSice) print *, "Warning, when setting velocities at tracer points in the advection halo, a situation occured when", &
                                                        " both neighbouring v points are in ice. This means that the Tracer point",i,j," is in a narrow lead.", &
                                                        " This may lead some interesting behaviour at this point."
                        if (vNice) v_T(i,j) = vgrid(i,j+1)
                        if (vSice) v_T(i,j) = vgrid(i,j)
                    end if 
                end if
            end do 

    end subroutine set_Tvels

!===========================================================================================!
!                                    calc_slopes                                            !
!===========================================================================================!
    subroutine calc_slopes(grid_type, var, varx, vary)
        !====================================================================
        ! This subroutine calculates slopes according to the MC limiter.
        ! This routine was made to be used during the central advection 
        ! schemes for A, h, and phi. 
        !
        ! This routine does not divide the differences by dx! This is done 
        ! in further calculations.
        !
        ! Note that no boundary consideration is required for these variables 
        ! as the homogenous dirichlet conditions are automatically enforced on 
        ! h and A. Also, since phi is calculated everywhere in the domain  
        ! (including ghost cells surrounding it) we won't encounter boundaries 
        ! for this variable. 
        !
        ! Additionally, note that this routine uses assumed shape. Although
        ! I've typically not used this Fortran feature, I want this routine 
        ! to be as flexible as possible. This way it can take in different
        ! grids, which is needed as we need the high resolution interpolants
        ! at both node points and tracer points, the type is specified by 
        ! 'grid_type'.
        !====================================================================

        !==================!
        !--------In--------!
        !==================!
        character(len = 4), intent(in) :: & 
            grid_type   ! defines what grid type we are using: either Tpnt or Npnt 
        real(kind = dbl_kind), intent(in) :: &
            var(:,:)    ! variable for which slopes need be calculated

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), intent(inout) :: &
            varx(:,:), vary(:,:)    ! resulting slope in the x and y directions, respectively

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j,   &   ! counters for loops 
            N, M,   &   ! used to define size of arrays
            dl, ul, &   ! masking variables 
            dr, ur, &   ! masking variables 
            msk         ! actual mask value
        real(kind = dbl_kind) :: &
            var_p1p0, var_m1p0,     &   ! masked variables in the x-direction
            var_p0p1, var_p0m1          ! masked variables in the y-direction

        ! Determine Grid Sizes !
            N = size(var,1)
            M = size(var,2)

            ! Terminate program and output warning if array size mis-match 
            if ((size(varx,1) /= N) .or. (size(varx,2) /= M)) then 
                print *, "Array size mis-match in slope calculation (for varx). Slope arrays must match variable array in calc_slopes."
                print *, "See routine in solver_routines module"
                print *, ""
                print *, "Terminating...."
                stop 
            end if 
            if ((size(vary,1) /= N) .or. (size(varx,2) /= M)) then 
                print *, "Array size mis-match in slope calculation (for vary). Slope arrays must match variable array in calc_slopes."
                print *, "See routine in solver_routines module"
                print *, ""
                print *, "Terminating...."
                stop 
            end if 

        if (grid_type == 'Tpnt') then 
            ! Avoid all ghost points surrounding domain except one row !
            do i = 1+(gc_T-1), N-(gc_T-1) 
                do j = 1+(gc_T-1), M-(gc_T-1)
                    varx(i,j) = minmod((var(i+1,j) - var(i-1,j))/2,     &   ! centered slope
                                slope_theta*(var(i+1,j) - var(i,j)),    &   ! increased forward slope 
                                slope_theta*(var(i,j) - var(i-1,j)))        ! increased backward slope

                    vary(i,j) = minmod((var(i,j+1) - var(i,j-1))/2,     &   ! centered slope
                                slope_theta*(var(i,j+1) - var(i,j)),    &   ! increased forward slope
                                slope_theta*(var(i,j) - var(i,j-1)))        ! increased backward slope
                end do 
            end do 

        else if (grid_type == 'Npnt') then 
            ! No ghost points on Node grid; no consideration required !

            ! Masked version 
            do i = 1, N 
                do j = 1, M 
                    ! Check for land points 
                    if (lmsk(i-1,j-1) .or. lmsk(i,j-1) .or. lmsk(i-1,j) .or. lmsk(i,j)) then 
                        ! node point is on a land cell - mask as necessary !

                        ! initialize masks to 0 !
                        dl = 0; ul = 0; dr = 0; ur = 0

                        ! set masks !
                        if (lmsk(i-1,j-1)) dl = 1   ! check tracer "down" and "left" of node point 
                        if (lmsk(i-1,j  )) ul = 1   ! check tracer "up" and "left" of node point 
                        if (lmsk(i  ,j-1)) dr = 1   ! check tracer "down" and "right" of node point 
                        if (lmsk(i  ,j  )) ur = 1   ! check tracer "up" and "right" of node point 

                        ! mask var(i+1,j) !
                            msk = int((ur + dr)/2)  ! -> 0 if only one is activated, 1 if both are 
                            var_p1p0 = var(i+1,j)*(1 - msk) + (2*var(i,j) - var(i-1,j))*msk 

                        ! mask var(i-1,j) !
                            msk = int((ul + dl)/2)
                            var_m1p0 = var(i-1,j)*(1 - msk) + (2*var(i,j) - var(i+1,j))*msk

                        ! mask var(i,j+1) !
                            msk = int((ul + ur)/2)
                            var_p0p1 = var(i,j+1)*(1 - msk) + (2*var(i,j) - var(i,j-1))*msk

                        ! mask var(i,j-1) !
                            msk = int((dl + dr)/2)
                            var_p0m1 = var(i,j-1)*(1 - msk) + (2*var(i,j) - var(i,j+1))*msk

                        varx(i,j) = minmod((var_p1p0 - var_m1p0)/2,     &   ! centered slope
                                    slope_theta*(var_p1p0 - var(i,j)),  &   ! increased forward slope 
                                    slope_theta*(var(i,j) - var_m1p0))      ! increased backward slope

                        vary(i,j) = minmod((var_p0p1 - var_p0m1)/2,     &   ! centered slope
                                    slope_theta*(var_p0p1 - var(i,j)),  &   ! increased forward slope
                                    slope_theta*(var(i,j) - var_p0m1))      ! increased backward slope

                    else
                        ! no masking required !
                        varx(i,j) = minmod((var(i+1,j) - var(i-1,j))/2,     &   ! centered slope
                                    slope_theta*(var(i+1,j) - var(i,j)),    &   ! increased forward slope 
                                    slope_theta*(var(i,j) - var(i-1,j)))        ! increased backward slope

                        vary(i,j) = minmod((var(i,j+1) - var(i,j-1))/2,     &   ! centered slope
                                    slope_theta*(var(i,j+1) - var(i,j)),    &   ! increased forward slope
                                    slope_theta*(var(i,j) - var(i,j-1)))        ! increased backward slope
                    end if 
                end do 
            end do
        else 
            print *, "Improper grid type sent to calc_slopes; see routine in solver_routines"
            print *, ""
            print *, "Terminating...."
            stop
        end if
    end subroutine calc_slopes

!===========================================================================================!
!                                      minmod                                               !
!===========================================================================================!
    real(kind = dbl_kind) function minmod(a,b,c)
        !====================================================================
        ! This function was written to quickly apply the minmod function when 
        ! using the MC limiter in the high-resolution slope calculations.
        !
        ! if (a > 0 and b > 0 and c > 0)
        !   minmod = min(a,b,c)
        ! elseif (a < 0 and b < 0 and c < 0)
        !   minmod = max(a,b,c)
        ! else 
        !   minmod = 0
        ! end 
        !====================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            a, b, c     ! the three slopes to be tested 

        if ((a > 0.d0) .and. (b > 0.d0) .and. (c > 0.d0)) then 
            minmod = min(a,b,c)
        else if ((a < 0.d0) .and. (b < 0.d0) .and. (c < 0.d0)) then 
            minmod = max(a,b,c)
        else
            minmod = 0.d0
        end if 
    end function minmod

!===========================================================================================!
!                                      Calc 2 Norm                                          !
!===========================================================================================!
    subroutine calc_nrm2(Norm2, vect, size)
        !====================================================================
        ! This subroutine calculates the 2 norm for a given vector.
        !
        ! I wrote this because this norm2 will be useful for this solver and 
        ! I didn't want to go through the trouble of linking the blas95 routine 
        !====================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            size            ! size of the vector
        real(kind = dbl_kind), dimension(size), intent(in) :: &
            vect            ! actual vector
        !==================!
        ! ----- Out   -----!
        !==================!
        real(kind = dbl_kind), intent(out) :: &
            Norm2           ! where the 2norm will be stored
        !==================!
        !---- Local -------!
        !==================!
        integer(kind = int_kind) :: &
            i
        real(kind = dbl_kind) :: &
            Sum

        ! initialize Sum !
        Sum = 0

        do i = 1,size
            Sum = Sum + vect(i)**2
        enddo

        Norm2 = sqrt(Sum)*dx/(x_extent)
    end subroutine calc_nrm2
end module solver_routines
