! This module contains routines that are used to validate major routines.
! It was create in order to aid in reproducibility of validation tests.
! Original Author: Clinton Seinen
!
! The contained routines are:
!               - gmres_validation
!               - store_Db 
!               - store_MatAorJ
!               - calc_validprb_frcng
!               - vpbcalc_exactdragcoef
!               - vpbcalc_exactvisc
!               - vpbcalc_exactvisc_viscgrads
!               - vpbadd_time_deriv
!               - vpbadd_coriolis_term
!               - vpbadd_airdrag_term
!               - vpbadd_waterdrag_term
!               - vpbadd_seasurf_term
!               - vpbadd_rheo_term

module validation_routines

! -----------------------------------------------------------------------------!
    !                       Module References                           !
    use modelparams         ! includes physical, numerical, and grid parameters
    use forcing             ! includes routines for updating forcing data
    use initialization      ! includes init. routines and grid parameters that are defined according to dx and will remain unchanged
    use var_routines        ! includes routines for calculating variables needed in the solver (for example, viscosity)
    use csv_file            ! module for writing data to csv format
    use solver_routines     ! module with the main solver routines 
    use domain_routines     ! module with routines for domain limiting and bc calcs
    use validsoln_routines  ! module with functions for calculating the exact solution to the validation problem and its derivatives
! -----------------------------------------------------------------------------!  

implicit none

! -----------------------------------------------------------------------------!
    !                           Contains                                !
! -----------------------------------------------------------------------------!

contains
!===========================================================================================!
!                           gmres validation                                                !
!===========================================================================================!
    subroutine gmres_validation(test_type, ugrid, vgrid, Au, time, si, tmp_size, &
                            zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, h_u, h_v, &
                            zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                            Cw_u, Cw_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                            nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                            indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

        !============================================================================================
        ! This subroutine is used to test and validate the gmres routine. 
        !
        ! This routine has the ability to check the gmres' ability to solve the following linear systems
        !
        !                           J(u)x = y; A(u)x = y; Db(u)x = y
        ! 
        ! where y is produced by multiplying the matrices J,A, and Db by a known vector x (normally ones).
        ! If the gmres routine and the matrices have been produced correctly, starting with an initial 
        ! guess of x = 0, the gmres_solve routine should produce the actual x once it is converged. 
        ! This routine places the produced vector on the grid and saves it for manual inspection.
        !============================================================================================

        !==================!
        !--------In--------!
        !==================!
        character(len = 2), intent(in) :: &
            test_type                                       ! string that contains the type of test to be done

        integer(kind = int_kind), intent (in) :: &
            nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN,     &   ! limit of indices for all point types (not including ghost cells)
            nU_pnts, nV_pnts,                           &   ! number of points in the computational domain
            si, tmp_size                                    ! si = size of vectors in the comp domain, tmp_size = size of temporary array needed for gmres routine
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj                                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj                                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                                            ! current time level
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                              &   ! viscosities - u points 
            zeta_gx_u, eta_gx_u, eta_gy_u,              &   ! needed viscosity derivatives - u points
            P_u, h_u, Cw_u,                             &   ! ice strength, thickness and water drag coefficient - u points       
            uocn_u, vocn_u                                  ! ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                              &   ! viscosities - v points 
            zeta_gy_v, eta_gx_v, eta_gy_v,              &   ! needed viscosity derivatives - v points
            P_v, h_v, Cw_v,                             &   ! ice strength, thickness and water drag coefficient - v points
            uocn_v, vocn_v                                  ! ocean velocities at v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(si), intent(in) :: &
            Au                                              ! the already computed mat-vect product

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij, iter                                  ! local indices and counters

        real(kind = dbl_kind) :: &
            norm, tol                                       ! variables needed for the gmres routine's tolerance                                            
        real(kind = dbl_kind), allocatable, dimension (:) :: &
            y_Jx, y_Ax, y_Dbx, &                            ! RHS vectors for each test type
            x_vect, &                                       ! The known solution
            soln_vect, &                                    ! The solution produced via the GMRES routine
            lin_res
        real(kind = dbl_kind), allocatable, dimension (:,:) :: &
            x_gridu, x_gridv, &                             ! the known solution to the linear system
            soln_gridu, soln_gridv, &                       ! gridded version of solution
            y_Dbx_gridu, y_Dbx_gridv                        ! gridded version of y_Dbx for debugging of Dbx calc  

            allocate(lin_res(si), x_vect(si), soln_vect(si),    x_gridu(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U),     x_gridv(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), &
                                                             soln_gridu(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U),  soln_gridv(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), &
                                                            y_Dbx_gridu(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), y_Dbx_gridv(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V))

            tol         = 0.0000000001_dbl_kind 
            norm        = 0.
            x_gridu     = 0.
            x_gridv     = 0.
            x_vect      = 0.
            soln_vect   = 0.
            soln_gridu  = 0.
            soln_gridv  = 0.
            y_Dbx_gridu = 0.
            y_Dbx_gridv = 0.

            ! set known solution used to produce the RHS vectors !
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                x_vect(ij)      = 1.
                ! x_vect(ij)      = ugrid(i,j)
                x_gridu(i,j)    = 1.
                ! x_gridu(i,j)    = ugrid(i,j)
            end do 
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                x_vect(ij + nU_pnts)    = 1.
                ! x_vect(ij + nU_pnts)    = vgrid(i,j)
                x_gridv(i,j)            = 1.
                ! x_gridv(i,j)            = vgrid(i,j)
            end do 

                ! open(unit = 1, file = '../Validation_Tests/gmres_tests/x_gridu.dat', status = 'unknown')
                open(unit = 1, file = '../Validation_Tests/gmres_tests/2017_05_02/x_gridu.dat', status = 'unknown')
                ! open(unit = 2, file = '../Validation_Tests/gmres_tests/x_gridv.dat', status = 'unknown')
                open(unit = 2, file = '../Validation_Tests/gmres_tests/2017_05_02/x_gridv.dat', status = 'unknown')
                call csv_write_dble_2d(1, x_gridu)
                call csv_write_dble_2d(2, x_gridv)
                close(1)
                close(2)  

                ! open(unit = 1, file = '../Validation_Tests/gmres_tests/x_vect.dat', status = 'unknown')
                open(unit = 1, file = '../Validation_Tests/gmres_tests/2017_05_02/x_vect.dat', status = 'unknown')
                call csv_write_dble_1d(1, x_vect)
                close(1)

            if (test_type == 'Jx') then
                ! run test on full jacobian matrix !
                print *, ""
                print *, "Testing linear solver with the Jacobian Matrix with tol =", tol
                print *, ""
                allocate(y_Jx(si))  
                y_Jx = 0.   

                ! perform the multiplication
                call jacobian_multiplication_r02(y_Jx, x_vect, Au, time, ugrid, vgrid, &
                                                zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, &
                                                zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                                Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                                indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                ! calculate norm in order to set the tolerance !
                call calc_nrm2(norm, y_Jx, si)

                print *, "Norm of RHS:", norm 
                print *, ""

                ! solve the system !
                call gmres_solve_r02('Jx', iter, soln_vect, y_Jx, norm, tol, si, tmp_size, &
                                time, Au, ugrid, vgrid, P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, &
                                zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v,  &
                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                ! put the solution on the grid for inspection and save !
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    soln_gridu(i,j) = soln_vect(ij) 
                end do 
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    soln_gridv(i,j) = soln_vect(nU_pnts + ij)
                end do

                ! open(unit = 1, file = '../Validation_Tests/gmres_tests/Jx_solnu.dat', status = 'unknown')
                open(unit = 1, file = '../Validation_Tests/gmres_tests/2017_05_02/Jx_solnu.dat', status = 'unknown')
                ! open(unit = 2, file = '../Validation_Tests/gmres_tests/Jx_solnv.dat', status = 'unknown')
                open(unit = 2, file = '../Validation_Tests/gmres_tests/2017_05_02/Jx_solnv.dat', status = 'unknown')
                call csv_write_dble_2d(1, soln_gridu)
                call csv_write_dble_2d(2, soln_gridv)
                close(1)
                close(2)

                ! calculate the linear residual !
                lin_res = soln_vect - x_vect
                call calc_nrm2(norm, lin_res, si)
                print *, "Final error L2 norm   = ", norm
                print *, "Final error inf norm  = ", maxval(abs(lin_res))

            else if (test_type == 'Ax') then 
                ! run test on full A matrix !
                print *, ""
                print *, "Testing linear solver with the System Matrix A with tol =", tol
                print *, ""
                allocate(y_Ax(si))
                y_Ax = 0.

                ! perform the multiplication ! 
                call calc_Au_r02(y_Ax, x_gridu, x_gridv, time, zeta_u, zeta_v, eta_u, eta_v, &
                                zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                ulmsk, uimsk, vlmsk, vimsk)

                ! calculate norm in order to set the tolerance !
                call calc_nrm2(norm, y_Ax, si)

                print *, "Norm of RHS:", norm 
                print *, ""

                ! solve the system !
                call gmres_solve_r02('Ax', iter, soln_vect, y_Ax, norm, tol, si, tmp_size, &
                                    time, Au, ugrid, vgrid, P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, &
                                    zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v,  &
                                    eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                    nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                    indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                ! put the solution on the grid for inspection and save !
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    soln_gridu(i,j) = soln_vect(ij) 
                end do 
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    soln_gridv(i,j) = soln_vect(nU_pnts + ij)
                end do

                open(unit = 1, file = '../Validation_Tests/gmres_tests/Ax_solnu.dat', status = 'unknown')
                open(unit = 2, file = '../Validation_Tests/gmres_tests/Ax_solnv.dat', status = 'unknown')
                call csv_write_dble_2d(1, soln_gridu)
                call csv_write_dble_2d(2, soln_gridv)
                close(1)
                close(2)

                ! calculate the linear residual !
                lin_res = soln_vect - x_vect
                call calc_nrm2(norm, lin_res, si)
                print *, "Final error norm = ", norm

            else if (test_type == 'Db') then 
                ! run test on Db matrix !
                print *, ""
                print *, "Testing linear solver with Db with tol =", tol
                print *, ""
                allocate(y_Dbx(si))
                y_Dbx = 0.

                ! perform the multiplication ! 
                call calc_Dbw(y_Dbx, x_vect, time, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk)

                ! place y_Dbx on the grid !
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)
                    y_Dbx_gridu(i,j) = y_Dbx(ij)
                end do 
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)
                    y_Dbx_gridv(i,j) = y_Dbx(ij + nU_pnts)
                end do 
                open(unit = 1, file = '../Validation_Tests/gmres_tests/y_Dbx_gridu.dat', status = 'unknown')
                open(unit = 2, file = '../Validation_Tests/gmres_tests/y_Dbx_gridv.dat', status = 'unknown')
                call csv_write_dble_2d(1, y_Dbx_gridu)
                call csv_write_dble_2d(2, y_Dbx_gridv)
                close(1)
                close(2)

                ! calculate norm in order to set the tolerance !
                call calc_nrm2(norm, y_Dbx, si)

                print *, "Norm of RHS:", norm 
                print *, ""

                ! solve the system !
                call gmres_solve_r02('Db', iter, soln_vect, y_Dbx, norm, tol, si, tmp_size, &
                                time, Au, ugrid, vgrid, P_u, P_v, P_T, P_N, Cw_u, Cw_v, h_u, h_v, &
                                zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v,  &
                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                ! put the solution on the grid for inspection and save !
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    soln_gridu(i,j) = soln_vect(ij) 
                end do 
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    soln_gridv(i,j) = soln_vect(nU_pnts + ij)
                end do

                open(unit = 1, file = '../Validation_Tests/gmres_tests/Dbx_solnu.dat', status = 'unknown')
                open(unit = 2, file = '../Validation_Tests/gmres_tests/Dbx_solnv.dat', status = 'unknown')
                call csv_write_dble_2d(1, soln_gridu)
                call csv_write_dble_2d(2, soln_gridv)
                close(1)
                close(2)

                ! calculate the linear residual !
                lin_res = soln_vect - x_vect
                call calc_nrm2(norm, lin_res, si)
                print *, "Final error norm = ", norm

            endif
    end subroutine gmres_validation

!===========================================================================================!
!                                      Store Db                                             !
!===========================================================================================!
    subroutine store_Db(ugrid, vgrid, time, uocn_u, vocn_u, uocn_v, vocn_v, &
                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                        ulmsk, uimsk, vlmsk, vimsk)

        !============================================================================================
        ! This subroutine was created to store the matrix Db for debugging and testing purposes.
        !
        ! Note that Db is limited to derivatives caused by the water drag forcing term, at the current
        ! time level, for b.
        !============================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind) :: & 
            nxU, nyU, &             ! indices limit - U points (not including ghost cells)
            nxV, nyV, &             ! indices limit - V points (not including ghost cells)
            nU_pnts, nV_pnts        ! number of U and V points in the computational domain
        integer(kind = int_kind), dimension ((nxU+2*gc_U)*(nyU+2*gc_U)), intent(in) :: &
            indxUi, indxUj          ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension ((nxV+2*gc_V)*(nyV+2*gc_V)), intent(in) :: &
            indxVi, indxVj          ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk            ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk            ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                    ! current time level
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            uocn_u, vocn_u
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            uocn_v, vocn_v
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                   ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                   ! v velocity on the grid

        !==================!
        ! ----- Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij, kk,           &   ! local indices
            cnt1, cnt2, cnt3, cnt4, &   ! counters used to set the off diagonal terms 
            ofd1, ofd2, ofd3, ofd4      ! will save the locations of the off diagonal terms in the matrix
        real(kind = dbl_kind) :: &
            cu_1, cu_2, cu_3, cu_4, &   ! constants used in the calculation of Dbw.
            cv_1, cv_2, cv_3, cv_4, &   ! 
            u1, u2, u3, u4,         &   ! masked u velocities
            v1, v2, v3, v4,         &   ! masked v velocities 
            halfdx
        real(kind = dbl_kind), allocatable, dimension (:,:) :: &
            Db                          ! matrix 

            halfdx = dx/2
            
            ! save variables needed in calculation for validation purposes - Note file names need to be manually changed !
                ! open(unit = 1, file = "../Validation_Tests/Db1_tests/ugrid40", status = "unknown")
                ! open(unit = 2, file = "../Validation_Tests/Db1_tests/vgrid40", status = "unknown")
                ! open(unit = 3, file = "../Validation_Tests/Db1_tests/uocnu40", status = "unknown")
                ! open(unit = 4, file = "../Validation_Tests/Db1_tests/vocnu40", status = "unknown")
                ! open(unit = 5, file = "../Validation_Tests/Db1_tests/uocnv40", status = "unknown")
                ! open(unit = 6, file = "../Validation_Tests/Db1_tests/vocnv40", status = "unknown")
                ! call csv_write_dble_2d(1, ugrid)
                ! call csv_write_dble_2d(2, vgrid)
                ! call csv_write_dble_2d(3, uocn_u)
                ! call csv_write_dble_2d(4, vocn_u)
                ! call csv_write_dble_2d(5, uocn_v)
                ! call csv_write_dble_2d(6, vocn_v)
                ! close(1)
                ! close(2)
                ! close(3)
                ! close(4)
                ! close(5)
                ! close(6)

            ! initialize matrix !
            allocate(Db(nU_pnts + nV_pnts, nU_pnts + nV_pnts))
            Db = 0.

            print *, "Storing Matrix Db1"
            print *, "Size is ", nU_pnts + nV_pnts, "by", nU_pnts + nV_pnts
            print *, "This could take a moment....."

            !=== Set U half of the matrix ===!
            do ij = 1, nU_pnts

                ! set initial off-diag points
                ofd1 = 999999
                ofd2 = 999999
                ofd3 = 999999
                ofd4 = 999999

                ! get u-point indices !
                i = indxUi(ij)
                j = indxUj(ij)

                ! calc constants !
                cu_1 = -rho_w*Cd_w*(uocn_u(i,j)*cos_w - vocn_u(i,j)*sin_w)/2 
                cu_2 = ugrid(i,j) - uocn_u(i,j)

                if ((product(ulmsk(i,j,:)) == 0) .or. (product(uimsk(i,j,:)) == 0)) then
                    ! Masking is required as at least of the masks is activated !
                    ! mask velocities at the surrounding v points around the u point of interest
                    ! v(i-1,j) !
                    v1 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                         vgrid(i-1,j  ) &
                            +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                            +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                            +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                            +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)
        
                    ! v(i,j) !
                    v2 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                         vgrid(i  ,j  ) &  
                            +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                            +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                            +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                            +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                    ! v(i-1,j+1) !
                    v3 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                         vgrid(i-1,j+1) &  
                            +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                            +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                            +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                            +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                    ! v(i,j+1) !
                    v4 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                         vgrid(i  ,j+1) &
                            +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                            +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                            +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                            +   (1- ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time)

                    ! calc remaining constants
                    cu_3 = (v1 + v2 + v3 + v4)/4 - vocn_u(i,j)
                    cu_4 = sqrt(cu_2**2 + cu_3**2)

                    ! Set coefficients for off-diagonal terms !
                    cnt1 = 0
                    cnt2 = 0
                    cnt3 = 0
                    cnt4 = 0

                    ! v(i-1,j) !
                    if (ulmsk(i,j,5) == 1) then
                        if (uimsk(i,j,5) == 1) then 
                            cnt1 = cnt1 + 1
                        else if (uimsk(i,j,6) == 1) then
                            cnt2 = cnt2 + 1
                        else if (uimsk(i,j,7) == 1) then 
                            cnt3 = cnt3 + 1
                        else if (uimsk(i,j,8) == 1) then 
                            cnt4 = cnt4 + 1
                        end if
                    end if 

                    ! v(i,j) !
                    if (ulmsk(i,j,6) == 1) then
                        if (uimsk(i,j,6) == 1) then
                            cnt2 = cnt2 + 1
                        else if (uimsk(i,j,5) == 1) then
                            cnt1 = cnt1 + 1
                        else if (uimsk(i,j,8) == 1) then
                            cnt4 = cnt4 + 1
                        else if (uimsk(i,j,7) == 1) then 
                            cnt3 = cnt3 + 1
                        end if 
                    end if 

                    ! v(i-1,j+1) !
                    if (ulmsk(i,j,7) == 1) then
                        if (uimsk(i,j,7) == 1) then
                            cnt3 = cnt3 + 1
                        else if (uimsk(i,j,8) == 1) then
                            cnt4 = cnt4 + 1
                        else if (uimsk(i,j,5) == 1) then
                            cnt1 = cnt1 + 1
                        else if (uimsk(i,j,6) == 1) then 
                            cnt2 = cnt2 + 1
                        end if 
                    end if 

                    ! v(i,j+1) !
                    if (ulmsk(i,j,8) == 1) then
                        if (uimsk(i,j,8) == 1) then
                            cnt4 = cnt4 + 1
                        else if (uimsk(i,j,7) == 1) then
                            cnt3 = cnt3 + 1
                        else if (uimsk(i,j,6) == 1) then
                            cnt2 = cnt2 + 1
                        else if (uimsk(i,j,5) == 1) then 
                            cnt1 = cnt1 + 1
                        end if 
                    end if

                else
                    ! Masking isn't required
                    ! calc remaining constants
                    cu_3 = (vgrid(i,j) + vgrid(i-1,j) + vgrid(i-1,j+1) + vgrid(i,j+1))/4 - vocn_u(i,j)
                    cu_4 = sqrt(cu_2**2 + cu_3**2)

                    ! Set coefficients for off-diagonal terms !
                    cnt1 = 1
                    cnt2 = 1
                    cnt3 = 1
                    cnt4 = 1
                end if

                ! Set diagonal !
                Db(ij, ij) = cu_1*cu_2/cu_4

                ! Set off diagonals !
                do kk = 1, nV_pnts
                    if ((indxVi(kk) == i-1) .and. (indxVj(kk) == j  )) then
                       Db(ij, nU_pnts + kk) = cnt1*cu_1*cu_3/(4*cu_4); ofd1 = nU_pnts + kk
                    end if 
                    if ((indxVi(kk) == i  ) .and. (indxVj(kk) == j  )) then
                       Db(ij, nU_pnts + kk) = cnt2*cu_1*cu_3/(4*cu_4); ofd2 = nU_pnts + kk
                    end if 
                    if ((indxVi(kk) == i-1) .and. (indxVj(kk) == j+1)) then
                       Db(ij, nU_pnts + kk) = cnt3*cu_1*cu_3/(4*cu_4); ofd3 = nU_pnts + kk
                    end if 
                    if ((indxVi(kk) == i  ) .and. (indxVj(kk) == j+1)) then
                       Db(ij, nU_pnts + kk) = cnt4*cu_1*cu_3/(4*cu_4); ofd4 = nU_pnts + kk
                    end if 
                end do 

                ! For identifying the corresponding u point !
                ! if ((ij == 1076)) then
                !     print *, " "
                !     print *, "Row:", ij
                !     print *, "corresponds to u-point", i, j
                ! end if 

                ! Print values for validation !
                ! if ((i == 45) .and. (j == 49)) then 
                !     print *, " "
                !     print *, "U-comp validations for u-point:", i, j
                !     print *, "Row number    = ", ij
                !     print *, "Diag          = ", Db(ij,ij)
                !     print *, "cu_1          = ", cu_1
                !     print *, "cu_2          = ", cu_2
                !     print *, "cu_3          = ", cu_3
                !     print *, "cu_4          = ", cu_4
                !     print *, "Count 1       = ", cnt1
                !     print *, "Count 2       = ", cnt2
                !     print *, "Count 3       = ", cnt3
                !     print *, "Count 4       = ", cnt4
                !     print *, "Off diag base = ", cu_1*cu_3/(4*cu_4)
                !     print *, "v(i-1,j)      loc = ", ofd1
                !     print *, "v(i,j)        loc = ", ofd2
                !     print *, "v(i-1,j+1)    loc = ", ofd3
                !     print *, "v(i,j+1)      loc = ", ofd4
                !     print *, " "
                !     print *, "Boundary Condition Masks"
                !     print *, "******* Land Masks **********"
                !     print *, ulmsk(i,j,5), ulmsk(i,j,6), ulmsk(i,j,7), ulmsk(i,j,8)
                !     print *, "******* Ice Extent Masks ****"
                !     print *, uimsk(i,j,5), uimsk(i,j,6), uimsk(i,j,7), uimsk(i,j,8)
                ! end if
            end do 

            !=== Set V half of the matrix ===!
            do ij = 1, nV_pnts

                ! set original of diag terms !
                ofd1 = 999999
                ofd2 = 999999
                ofd3 = 999999
                ofd4 = 999999

                ! get indices of v-point !
                i = indxVi(ij)
                j = indxVj(ij)

                ! calc constants !
                cv_1 = -rho_w*Cd_w*(vocn_v(i,j)*cos_w + uocn_v(i,j)*sin_w)/2
                cv_2 = vgrid(i,j) - vocn_v(i,j)

                if ((product(vlmsk(i,j,:)) == 0) .or. (product(vimsk(i,j,:)) == 0)) then
                    ! Masking is required as at least of the masks is activated !
                    ! mask velocities at the surounding u points around the v point of interest
                    u1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                     ugrid(i  ,j-1)  &
                        + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                        + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                        + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                
                    ! u(i+1,j-1) !
                    u2 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                     ugrid(i+1,j-1)  &
                            + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                            + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                            + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                            + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                    
                    ! u(i,j) !
                    u3 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                     ugrid(i  ,j  ) & 
                            + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                            + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                            + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                            + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                    
                    ! u(i+1,j) !
                    u4 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                     ugrid(i+1,j  ) & 
                            + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                            + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                            + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                            + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)

                    ! calc remaining constants
                    cv_3 = (u1 + u2 + u3 + u4)/4 - uocn_v(i,j)
                    cv_4 = sqrt(cv_3**2 + cv_2**2)

                    ! Set coefficients for off-diagonal terms !
                    cnt1 = 0
                    cnt2 = 0
                    cnt3 = 0
                    cnt4 = 0

                    ! u(i,j-1) !
                    if (vlmsk(i,j,1) == 1) then
                        if (vimsk(i,j,1) == 1) then 
                            cnt1 = cnt1 + 1
                        else if (vimsk(i,j,3) == 1) then
                            cnt3 = cnt3 + 1
                        else if (vimsk(i,j,2) == 1) then 
                            cnt2 = cnt2 + 1
                        else if (vimsk(i,j,4) == 1) then
                            cnt4 = cnt4 + 1
                        end if 
                    end if

                    ! u(i+1,j-1) !
                    if (vlmsk(i,j,2) == 1) then 
                        if (vimsk(i,j,2) == 1) then 
                            cnt2 = cnt2 + 1
                        else if (vimsk(i,j,4) == 1) then
                            cnt4 = cnt4 + 1
                        else if (vimsk(i,j,1) == 1) then 
                            cnt1 = cnt1 + 1
                        else if (vimsk(i,j,3) == 1) then
                            cnt3 = cnt3 + 1
                        end if 
                    end if 

                    ! u(i,j) !
                    if (vlmsk(i,j,3) == 1) then 
                        if (vimsk(i,j,3) == 1) then 
                            cnt3 = cnt3 + 1
                        else if (vimsk(i,j,1) == 1) then
                            cnt1 = cnt1 + 1
                        else if (vimsk(i,j,4) == 1) then 
                            cnt4 = cnt4 + 1
                        else if (vimsk(i,j,2) == 1) then
                            cnt2 = cnt2 + 1
                        end if 
                    end if

                    ! u(i+1,j) !
                    if (vlmsk(i,j,4) == 1) then
                        if (vimsk(i,j,4) == 1) then 
                            cnt4 = cnt4 + 1
                        else if (vimsk(i,j,2) == 1) then
                            cnt2 = cnt2 + 1
                        else if (vimsk(i,j,3) == 1) then 
                            cnt3 = cnt3 + 1
                        else if (vimsk(i,j,1) == 1) then
                            cnt1 = cnt1 + 1
                        end if 
                    end if 

                else 
                    ! calc remaining constants
                    cv_3 = (ugrid(i,j) + ugrid(i+1,j) + ugrid(i+1,j-1) + ugrid(i,j-1))/4 - uocn_v(i,j)
                    cv_4 = sqrt(cv_3**2 + cv_2**2)

                    ! Set coefficients for off-diagonal terms !
                    cnt1 = 1
                    cnt2 = 1
                    cnt3 = 1
                    cnt4 = 1
                end if

                ! Set diagonal !
                Db(ij + nU_pnts, ij + nU_pnts) = cv_1*cv_2/cv_4

                ! Set off diagonals !
                do kk = 1, nU_pnts
                    if ((indxUi(kk) == i  ) .and. (indxUj(kk) == j-1)) then 
                        Db(ij + nU_pnts, kk) = cnt1*cv_1*cv_3/(4*cv_4); ofd1 = kk
                    end if 
                    if ((indxUi(kk) == i+1) .and. (indxUj(kk) == j-1)) then 
                        Db(ij + nU_pnts, kk) = cnt2*cv_1*cv_3/(4*cv_4); ofd2 = kk
                    end if 
                    if ((indxUi(kk) == i  ) .and. (indxUj(kk) == j  )) then 
                        Db(ij + nU_pnts, kk) = cnt3*cv_1*cv_3/(4*cv_4); ofd3 = kk
                    end if 
                    if ((indxUi(kk) == i+1) .and. (indxUj(kk) == j  )) then 
                        Db(ij + nU_pnts, kk) = cnt4*cv_1*cv_3/(4*cv_4); ofd4 = kk
                    end if 
                end do 
            end do 

            ! Save Matrix !
            open(unit = 1, file = "../Validation_Tests/Db1_tests/Db1_Mat_40km", status = "unknown")
            call csv_write_dble_2d(1, Db)
            close(1)

            print *, " "
            print *, "Matrix Saved"
            print *, " "

    end subroutine store_Db

!===========================================================================================!
!                                Store Matrix A or J                                        !
!===========================================================================================!
    subroutine store_MatAorJ(mat_type, Au, ugrid, vgrid, time, &
                            zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, h_u, h_v, &
                            zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                            Cw_u, Cw_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                            nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                            indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

        !============================================================================================
        ! This subroutine was made in order to store the matrices A or J. This routine accomplishes 
        ! this by multiplying the matrix by the standard basis vectors and storing the resulting 
        ! vectors as the columns of the matrices.
        !
        !============================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        character(len = 1), intent(in) :: &
            mat_type                        ! string that contains the type of matrix we wish to use

        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nxN, nyN,   &                   ! indices limit - N points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension ((nxU+2*gc_U)*(nyU+2*gc_U)), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension ((nxV+2*gc_V)*(nyV+2*gc_V)), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                    ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                    ! computational masks for V points

        real(kind = dbl_kind), intent(in) :: &
            time                            ! current time level
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                             ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                             ! ice strength at N points
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            zeta_u, eta_u,                  &   ! viscosities - u points
            zeta_gx_u, eta_gx_u, eta_gy_u,  &   ! needed viscosity derivatives - u points
            P_u, h_u, Cw_u,                 &   ! ice strength, thickness and water drag coefficient - u points       
            uocn_u, vocn_u                      ! ocean velocities at u points
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosities - v points
            zeta_gy_v, eta_gx_v, eta_gy_v,  &   ! needed viscosity derivatives - v points 
            P_v, h_v, Cw_v,                 &   ! ice strength, thickness and water drag coefficient - v points
            uocn_v, vocn_v                      ! ocean velocities at v points
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                           ! u velocity on the grid
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                           ! v velocity on the grid
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(in) :: &
            Au                              ! holds the current matrix vector product of A and u 

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            si, ij

        real(kind = dbl_kind), allocatable, dimension(:) :: &
            Me, e_vect                      ! holds the vector that will result from the multiplications
        real(kind = dbl_kind), allocatable, dimension(:,:) :: &
            Mat                             ! will hold the actual matrix 

        ! initalize arrays !
        si = nU_pnts + nV_pnts

        allocate(Me(si), e_vect(si), Mat(si,si))
        Me      = 0.
        e_vect  = 0.
        Mat     = 0.

        print *, "Storing a", si, "by", si, "matrix. This may take a while......"

        if (mat_type == 'A') then
            ! Store A !
            print *, " "
            print *, "Storing the system matrix A"
            do ij = 1, si 
                e_vect(ij) = 1.

                call calc_Au_r02(Me, ugrid, vgrid, time, zeta_u, zeta_v, eta_u, eta_v, &
                                zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, Cw_u, Cw_v, h_u, h_v, &
                                nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj, &
                                ulmsk, uimsk, vlmsk, vimsk, e_vect)

                Mat(:,ij)   = Me 
                Me          = 0.
                e_vect(ij)  = 0.
            end do 

            ! save matrix !
            open(unit = 1, file = "../Validation_Tests/A_J_tests/A_Mat.dat", status = "unknown")
            call csv_write_dble_2d(1, Mat)
            close(1)

        else if (mat_type == 'J') then
            ! Store the Jacobian !
            print *, " "
            print *, "Storing the jacobian matrix J(u)"
            do ij = 1, si 
                e_vect(ij) = 1.

                call jacobian_multiplication_r02(Me, e_vect, Au, time, ugrid, vgrid, &
                                        zeta_u, zeta_v, eta_u, eta_v, P_u, P_v, P_T, P_N, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                        Cw_u, Cw_v, h_u, h_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                        nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, &
                                        indxUi, indxUj, indxVi, indxVj, ulmsk, uimsk, vlmsk, vimsk)

                Mat(:,ij)   = Me 
                Me          = 0.
                e_vect(ij)  = 0.
            end do

            ! save matrix !
            ! open(unit = 1, file = "../Validation_Tests/A_J_tests/J_Mat.dat", status = "unknown")
            ! open(unit = 1, file = "../Validation_Tests/gmres_tests/2017_05_02/J_Mat.dat", status = "unknown")
            open(unit = 1, file = "./output/J_Mat.dat",status = "unknown")
            call csv_write_dble_2d(1, Mat)
            close(1)

        else
            print *, "See routine store_MatAorJ in validation_routines.f90: unknown matrix type encountered."
        end if 

    end subroutine store_MatAorJ

!===========================================================================================!
!                           calc validation prblm forcing term                              !
!===========================================================================================!
    subroutine calc_validprb_frcng(vpb_forc, time, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN, &
                                    nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj,      &
                                    P_u, P_v, P_T, P_N, h_u, h_v)

        !====================================================================================
        ! This subroutine calculates the known forcing term, vpb_forc, for the validation problem.
        ! Note that in this routine, discrete viscosity gradients are used; the exact versions 
        ! could be derived, but they will be quite messy and there is a decent likelihood for 
        ! errors; centered differences are currently used.
        !
        ! Note: the routine that adds the rheology term doesn't add the pressure term. This 
        ! is simple to code in and as P doesn't change between time steps, I add it once.
        !====================================================================================

        !==================!
        !------- In -------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN,     &   ! index limits for all point types
            nU_pnts, nV_pnts                                ! number of points in the computational domain 
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                                  ! location of U points in the comp domain 
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                                  ! location of V points in the comp domain 

        real(kind = dbl_kind), intent(in) :: &
            time                                            ! current time level
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                                             ! ice strength at T points 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            P_N                                             ! ice strength at N points
        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            P_u, h_u                                        ! strength and thickness at U points
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            P_v, h_v                                        ! strength and thickness at V points

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc

        !==================!
        !----- Local ------!
        !==================!
        character(len = 8) :: &
            fmt1, dx_f                                  ! strings used for file outputting 

        integer(kind = int_kind) :: &
            i, j, ij                                    ! local indices    

        real(kind = dbl_kind), dimension(nxU, nyU) :: &
            uocn_u, vocn_u, uwnd_u, vwnd_u,                 &   ! needed forcing arrays 
            eta_u, zeta_u, zeta_gx_u, eta_gx_u, eta_gy_u,   &   ! known viscosities and derivatives at U points 
            Cw_u, w1_u, w2_u,                               &   ! known drag coef and solution at U poins
            uforce                                              ! gridded u forcing 
        real(kind = dbl_kind), dimension(nxV, nyV) :: &
            uocn_v, vocn_v, uwnd_v, vwnd_v,                 &   ! needed forcing arrays 
            eta_v, zeta_v, zeta_gy_v, eta_gx_v, eta_gy_v,   &   ! known viscosities and derivatives at V points
            Cw_v, w1_v, w2_v,                               &   ! known drag coef and solution at V points
            vforce                                              ! gridded v forcing

        ! initialize local arrays !
        uocn_u = 0.; vocn_u = 0.; uwnd_u = 0.; vwnd_u = 0.; Cw_u = 0.; w1_u = 0.; w2_u = 0.
        uocn_v = 0.; vocn_v = 0.; uwnd_v = 0.; vwnd_v = 0.; Cw_v = 0.; w1_v = 0.; w2_v = 0.
        eta_u = 0.; zeta_u = 0.; zeta_gx_u = 0.; eta_gx_u = 0.; eta_gy_u = 0.
        eta_v = 0.; zeta_v = 0.; zeta_gy_v = 0.; eta_gx_v = 0.; eta_gy_v = 0.
        uforce = 0.; vforce = 0.

        fmt1 = '(I2.2)'
        write (dx_f, fmt1) int(dx/1000)

        !=========================!
        !      Time level n-1     !
        !=========================!
            ! calc solution at the previous time level !
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                w1_u(i,j) = toysoln_w1(x_u(i), y_u(j), time - dt)
                w2_u(i,j) = toysoln_w2(x_u(i), y_u(j), time - dt)
            end do 
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                w1_v(i,j) = toysoln_w1(x_v(i), y_v(j), time - dt)
                w2_v(i,j) = toysoln_w2(x_v(i), y_v(j), time - dt)
            end do

            ! calc forcing vectors at previous time level !
                call get_ocn_forcing(uocn_u, uocn_v, vocn_u, vocn_v, time - dt, nxT, nyT, nxU, nyU, nxV, nyV)
                call get_wnd_forcing(uwnd_u, uwnd_v, vwnd_u, vwnd_v, time - dt, nxU, nyU, nxV, nyV)

            ! calc exact drag coefficients !
                call vpbcalc_exactdragcoef(Cw_u, Cw_v, w1_u, w2_u, w1_v, w2_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

            ! calc exact viscosities and gradients !
                call vpbcalc_exactvisc_viscgrads(zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v, & 
                                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, & 
                                                indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts, &
                                                nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN, time - dt)

            ! add time deriv component !
            !    call vpbadd_time_deriv(vpb_forc, w2_u, w1_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add coriolis component !
          !      call vpbadd_coriolis_term(vpb_forc, w2_u, w1_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add air drag term !
           !     call vpbadd_airdrag_term(vpb_forc, uwnd_u, vwnd_u, uwnd_v, vwnd_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add water drag term !
            !    call vpbadd_waterdrag_term(vpb_forc, w1_u, w2_u, w1_v, w2_v, uocn_u, vocn_u, uocn_v, vocn_v, Cw_u, Cw_v, &
            !                                nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add sea surface tilt term !
             !   call vpbadd_seasurf_term(vpb_forc, vocn_u, uocn_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)

            ! add reformulated rheology term !
              !  call vpbadd_rheo_term_r02(vpb_forc, time - dt, zeta_u, eta_u, zeta_v, eta_v, &
              !                      zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
              !                      nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

            ! add pressure term !
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    vpb_forc(ij) = vpb_forc(ij) - (P_T(i,j) - P_T(i-1,j))/(2*dx)
                end do 
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    vpb_forc(ij + nU_pnts) = vpb_forc(ij + nU_pnts) - (P_T(i,j) - P_T(i,j-1))/(2*dx)
                end do 

        !=========================!
        !      Time level n       !
        !=========================!
            ! calc solution at the current time level
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                w1_u(i,j) = toysoln_w1(x_u(i), y_u(j), time)
                w2_u(i,j) = toysoln_w2(x_u(i), y_u(j), time)
            end do 
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                w1_v(i,j) = toysoln_w1(x_v(i), y_v(j), time)
                w2_v(i,j) = toysoln_w2(x_v(i), y_v(j), time)
            end do

            ! calc forcing vectors at current time level !
                call get_ocn_forcing(uocn_u, uocn_v, vocn_u, vocn_v, time, nxT, nyT, nxU, nyU, nxV, nyV)
                call get_wnd_forcing(uwnd_u, uwnd_v, vwnd_u, vwnd_v, time, nxU, nyU, nxV, nyV)

            ! calc exact drag coefficients !
                call vpbcalc_exactdragcoef(Cw_u, Cw_v, w1_u, w2_u, w1_v, w2_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

            ! calc exact viscosities and gradients !
                call vpbcalc_exactvisc_viscgrads(zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v, & 
                                                eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, & 
                                                indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts, &
                                                nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN, time)

            ! add time deriv component !
                call vpbadd_time_deriv(vpb_forc, w2_u, w1_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add coriolis component !
                call vpbadd_coriolis_term(vpb_forc, w2_u, w1_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add air drag term !
                call vpbadd_airdrag_term(vpb_forc, uwnd_u, vwnd_u, uwnd_v, vwnd_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add water drag term !
                call vpbadd_waterdrag_term(vpb_forc, w1_u, w2_u, w1_v, w2_v, uocn_u, vocn_u, uocn_v, vocn_v, Cw_u, Cw_v, &
                                            nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)
            ! add sea surface tilt term !
                call vpbadd_seasurf_term(vpb_forc, vocn_u, uocn_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)

            ! add reformulated rheology term !
                call vpbadd_rheo_term_r02(vpb_forc, time, zeta_u, eta_u, zeta_v, eta_v, &
                                        zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    uforce(i,j) = vpb_forc(ij)                    
                end do 
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    vforce(i,j) = vpb_forc(ij + nU_pnts)
                end do 

                ! open(unit = 1, file = "../Valid_Pblm/Forcing/uforcing.dat", status = "unknown")
                ! open(unit = 2, file = "../Valid_Pblm/Forcing/vforcing.dat", status = "unknown")
                ! call csv_write_dble_2d(1, uforce)
                ! call csv_write_dble_2d(2, vforce)
                ! close(1)
                ! close(2)

                ! open(unit = 1, file = '../Valid_Pblm/Cw_Tests/CwU_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! open(unit = 2, file = '../Valid_Pblm/Cw_Tests/CwV_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, Cw_u)
                ! call csv_write_dble_2d(2, Cw_v)
                ! close(1)
                ! close(2)

                ! open(unit = 1, file = '../Valid_Pblm/Visc_Tests/zU_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! open(unit = 2, file = '../Valid_Pblm/Visc_Tests/zV_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, zeta_u)
                ! call csv_write_dble_2d(2, zeta_v)
                ! close(1)
                ! close(2)

                ! open(unit = 1, file = '../Valid_Pblm/ViscGrads_Tests/dedxU_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! open(unit = 2, file = '../Valid_Pblm/ViscGrads_Tests/dedxV_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, eta_gx_u)
                ! call csv_write_dble_2d(2, eta_gx_v)
                ! close(1)
                ! close(2)

                ! open(unit = 1, file = '../Valid_Pblm/ViscGrads_Tests/dedyU_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! open(unit = 2, file = '../Valid_Pblm/ViscGrads_Tests/dedyV_exact'//trim(dx_f)//'.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, eta_gy_u)
                ! call csv_write_dble_2d(2, eta_gy_v)
                ! close(1)
                ! close(2)

    end subroutine calc_validprb_frcng

!===========================================================================================!
!                               Calc Exact Drag Coeff                                       !
!===========================================================================================!
    subroutine vpbcalc_exactdragcoef(Cw_u, Cw_v, w1_u, w2_u, w1_v, w2_v, uocn_u, vocn_u, uocn_v, vocn_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

        !====================================================================================
        ! This subroutine calculates the exact water drag coefficients to be used in the forcing
        ! term for the validation problem. 
        !====================================================================================  

        !==================!
        !------- In -------!
        !==================! 
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &               ! index limits 
            nU_pnts, nV_pnts                        ! number of points in the computation domain (U and V points)
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                          ! location of U points in the comp domain 
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                          ! location of V points in the comp domain

        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            w1_u, w2_u, uocn_u, vocn_u              ! known solution (w1 and w2) and ocean forcing at U points 
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            w1_v, w2_v, uocn_v, vocn_v              ! known solution (w1 and w2) and ocean forcing at V points 

        !==================!
        !------ In/out ----!
        !==================!
        real(kind = dbl_kind), dimension(nxU, nyU), intent(inout) :: &
            Cw_u 
        real(kind = dbl_kind), dimension(nxV, nyV), intent(inout) :: &
            Cw_v 

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij 

        !===============================!
        !       Calc at U points        !
        !===============================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                Cw_u(i,j) = rho_w*Cd_w*sqrt( (w1_u(i,j) - uocn_u(i,j))**2 + (w2_u(i,j) - vocn_u(i,j))**2 )
            end do 

        !===============================!
        !       Calc at V points        !
        !===============================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                Cw_v(i,j) = rho_w*Cd_w*sqrt( (w1_v(i,j) - uocn_v(i,j))**2 + (w2_v(i,j) - vocn_v(i,j))**2 )
            end do     
    end subroutine vpbcalc_exactdragcoef

!===========================================================================================!
!                               Calc Exact Viscosities                                      !
!===========================================================================================!
    subroutine vpbcalc_exactvisc(eta_u, zeta_u, eta_v, zeta_v, eta_T, zeta_T, eta_N, zeta_N, P_u, P_v, P_T, P_N,   &
                                nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN, nU_pnts, nV_pnts, nT_pnts, nN_pnts,     &
                                indxUi, indxUj, indxVi, indxVj, indxTi, indxTj, indxNi, indxNj,                 &
                                dist_gx_T, dist_gy_T, nHT_pnts, nHN_pnts, haloTi, haloTj, haloNi, haloNj,       &
                                time)

        !===================================================================================
        ! NOTE THIS ROUTINE HAS BEEN REPLACED BY vpbcalc_exactvisc_viscgrads, where the viscosity
        ! derivatives are now calculated in closed form.
        !
        ! This subroutine calculates the exact viscosity values according to the known solution
        ! and populates the halo points accordingly. 
        !===================================================================================

        !==================!
        !------- In -------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN,     &   ! index limits for all points types 
            nU_pnts, nV_pnts, nT_pnts, nN_pnts,         &   ! number of points in the comp domain 
            nHT_pnts, nHN_pnts                              ! number of halo points around the comp domain 
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                                  ! locations of U points in the comp domain
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                                  ! locations of V points in the comp domain
        integer(kind = int_kind), dimension(nT_pnts), intent(in) :: &
            indxTi, indxTj                                  ! locations of T points in the comp domain
        integer(kind = int_kind), dimension(nN_pnts), intent(in) :: &
            indxNi, indxNj                                  ! locations of N points in the comp domain
        integer(kind = int_kind), dimension(nHT_pnts), intent(in) :: &
            haloTi, haloTj                                  ! location of T halo points 
        integer(kind = int_kind), dimension(nHN_pnts), intent(in) :: &
            haloNi, haloNj                                  ! location of N halo points 

        real(kind = dbl_kind), intent(in) :: &
            time                                            ! time level of interest 
        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            P_u                                             ! ice strength at u points 
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            P_v                                             ! ice strength at v points 
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            dist_gx_T, dist_gy_T,   &                       ! distance function gradients, needed for halo points  
            P_T                                             ! ice strength at T points 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            P_N                                             ! ice strength at N points 

        !====================!
        !------ In/Out ------!
        !====================!
        real(kind = dbl_kind), dimension(nxU, nyU), intent(inout) :: &
            eta_u, zeta_u
        real(kind = dbl_kind), dimension(nxV, nyV), intent(inout) :: &
            eta_v, zeta_v
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            eta_T, zeta_T
        real(kind = dbl_kind), dimension(nxN, nyN), intent(inout) :: &
            eta_N, zeta_N

        !====================!
        !------ Local -------!
        !====================!
        integer(kind = int_kind) :: &
            i, j, ij 

        real(kind = dbl_kind) :: &
            strain_11,  &       ! epsilon_11 in delta calc (dudx)
            strain_22,  &       ! epsilon_22 in delta calc (dvdy)
            strain_12,  &       ! epsilon_12 in delta calc 0.5(dudy + dvdx)
            delta,      &       ! contains the exact delta value 
            zeta_max            ! contains the maximum zeta value

        !===========================!
        !       U - points          !
        !===========================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ! determine strain rates !
                strain_11 = toysoln_dw1dx(x_u(i), y_u(j), time)
                strain_22 = toysoln_dw2dy(x_u(i), y_u(j), time)
                strain_12 = 0.5*(toysoln_dw1dy(x_u(i), y_u(j), time) + toysoln_dw2dx(x_u(i), y_u(j), time))

                ! calc delta !
                delta = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))   &
                                + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                ! calc viscosities !
                zeta_max = maxvisc_cf*P_u(i,j)

                ! calc bulk visc according to non-smoothed capping method !
                ! zeta_u(i,j) = min(P_u(i,j)/(2*delta + machine_eps*P_u(i,j)/zeta_max), zeta_max)

                ! calc bulk visc according to smoothed capping method !
                zeta_u(i,j) = zeta_max*tanh(P_u(i,j)/(2*delta*zeta_max + machine_eps))
                eta_u(i,j)  = zeta_u(i,j)*(ellip_rat**(-2))
            end do 

        !===========================!
        !       V - points          !
        !===========================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                ! determine strain rates !
                strain_11 = toysoln_dw1dx(x_v(i), y_v(j), time)
                strain_22 = toysoln_dw2dy(x_v(i), y_v(j), time)
                strain_12 = 0.5*(toysoln_dw1dy(x_v(i), y_v(j), time) + toysoln_dw2dx(x_v(i), y_v(j), time))

                ! calc delta !
                delta = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))   &
                                + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                ! calc viscosities !
                zeta_max = maxvisc_cf*P_v(i,j)

                ! calc bulk visc according to non-smoothed capping method !
                ! zeta_v(i,j) = min(P_v(i,j)/(2*delta + machine_eps*P_v(i,j)/zeta_max), zeta_max)

                ! calc bulk visc according to smoothed capping method !
                zeta_v(i,j) = zeta_max*tanh(P_v(i,j)/(2*delta*zeta_max + machine_eps))
                eta_v(i,j)  = zeta_v(i,j)*(ellip_rat**(-2))
            end do 

        !===========================!
        !       T - points          !
        !===========================!
            do ij = 1, nT_pnts
                i = indxTi(ij)
                j = indxTj(ij)

                ! determine strain rates !
                strain_11 = toysoln_dw1dx(x_T(i), y_T(j), time)
                strain_22 = toysoln_dw2dy(x_T(i), y_T(j), time)
                strain_12 = 0.5*(toysoln_dw1dy(x_T(i), y_T(j), time) + toysoln_dw2dx(x_T(i), y_T(j), time))

                ! calc delta !
                delta = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))   &
                                + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                ! calc viscosities !
                zeta_max = maxvisc_cf*P_T(i,j)

                ! calc bulk visc according to non-smoothed capping method !
                ! zeta_T(i,j) = min(P_T(i,j)/(2*delta + machine_eps*P_T(i,j)/zeta_max), zeta_max)

                ! calc bulk visc according to smoothed capping method !
                zeta_T(i,j) = zeta_max*tanh(P_T(i,j)/(2*delta*zeta_max + machine_eps))
                eta_T(i,j)  = zeta_T(i,j)*(ellip_rat**(-2))
            end do 

        !===========================!
        !       N - points          !
        !===========================!
            do ij = 1, nN_pnts
                i = indxNi(ij)
                j = indxNj(ij)

                ! determine strain rates !
                strain_11 = toysoln_dw1dx(x_N(i), y_N(j), time)
                strain_22 = toysoln_dw2dy(x_N(i), y_N(j), time)
                strain_12 = 0.5*(toysoln_dw1dy(x_N(i), y_N(j), time) + toysoln_dw2dx(x_N(i), y_N(j), time))

                ! calc delta !
                delta = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))   &
                                + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                ! calc viscosities !
                zeta_max = maxvisc_cf*P_N(i,j)

                ! calc bulk visc according to non-smoothed capping method !
                ! zeta_N(i,j) = min(P_N(i,j)/(2*delta + machine_eps*P_N(i,j)/zeta_max), zeta_max)

                ! calc bulk visc according to smoothed capping method !
                zeta_N(i,j) = zeta_max*tanh(P_N(i,j)/(2*delta*zeta_max + machine_eps))
                eta_N(i,j)  = zeta_N(i,j)*(ellip_rat**(-2))
            end do 

        !===============================!
        !       Update Halo Points      !
        !===============================!
            call update_halo_points(zeta_T, eta_T, zeta_N, eta_N, dist_gx_T, dist_gy_T, &
                                    haloTi, haloTj, haloNi, haloNj, nHN_pnts, nHT_pnts, &
                                    nxT, nyT, nxN, nyN)

    end subroutine vpbcalc_exactvisc

!===========================================================================================!
!                   Calc Exact Viscosities and the Needed Grads                             !
!===========================================================================================!
    subroutine vpbcalc_exactvisc_viscgrads(zeta_u, zeta_v, eta_u, eta_v, zeta_gx_u, zeta_gy_v, & 
                                            eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, & 
                                            indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts, &
                                            nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN, time)

        !===================================================================================
        ! According to time, this routine calculates the exact viscosities and the their 
        ! required gradients. Note that this routine calculates the viscosities according to
        ! Lemiuex's continuously differentiable formulation.
        !===================================================================================

        !==================!
        !------ In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &   ! index limit for U and V points
            nxT, nyT, nxN, nyN,     &   ! index limit for T and N points
            nU_pnts, nV_pnts            ! number of U and V points in the computational domain 
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj              ! locations of U points with-in computational domain 
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj              ! locations of V points with-in computational domain

        real(kind = dbl_kind), intent(in) :: &
            time 
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: &
            P_u
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            P_v
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                         ! ice strength at T points
        real(kind = dbl_kind), dimension (nxN, nyN), intent(in) :: &
            P_N                         ! ice strength at N points

        !==================!
        !----- In/Out -----!
        !==================!
        real(kind = dbl_kind), dimension (nxU, nyU), intent(inout) :: &
            zeta_u, eta_u,                  &   ! u point viscosities
            zeta_gx_u, eta_gx_u, eta_gy_u       ! u point visc grads
        real(kind = dbl_kind), dimension (nxV, nyV), intent(inout) :: &
            zeta_v, eta_v,                  &   ! v point viscosities
            zeta_gy_v, eta_gx_v, eta_gy_v       ! v point visc grads

        !==================!
        !------ Local -----!
        !==================!
        character(len=8) :: &
            fmt, dx_f      ! local format descriptor for outputting files and a string to hold dx in kilometers for easy outputting
        integer(kind = int_kind) :: &
            i, j, ij       ! local indexes
        real(kind = dbl_kind) :: &
            dw1dx, dw1dy, d2w1dx2, d2w1dy2, d2w1dxdy,       &   ! needed spatial derivatives in u 
            dw2dx, dw2dy, d2w2dx2, d2w2dy2, d2w2dxdy,       &   ! needed spatial derivatives in v
            strain_11, strain_22, strain_12,                &   ! needed strains 
            delta, ddelta2_dx, ddelta2_dy,                  &   ! delta needed in calculations and the derivatives of its square
            dPdx, dPdy,                                     &   ! necessary ice strength derivatives
            cnst1, cnst2                                        ! constants to clean up code
        ! real(kind = dbl_kind), dimension (nxU, nyU) :: &
        !     ddelta2_dx_U,ddelta2_dy_U, delta_U, deriv_U,   &   ! These arrays are only needed to test the rate of convergence. 
        !     dPdx_U, dPdy_U
        ! real(kind = dbl_kind), dimension (nxV, nyV) :: &        ! Comment out or delete once routines have been validated.
        !     ddelta2_dx_V, ddelta2_dy_V, delta_V, deriv_V,   &
        !     dPdx_V, dPdy_V

            fmt = '(I2.2)'
            write (dx_f, fmt) int(dx/1000) ! turn dx into a string  

            ! ddelta2_dx_U = 0.; ddelta2_dy_U = 0.; ddelta2_dx_V = 0.; ddelta2_dy_V = 0.
            ! delta_U = 0.; delta_V = 0.; deriv_U = 0.; deriv_V = 0.;
            ! dPdx_U = 0.; dPdx_V = 0.; dPdy_U = 0.; dPdy_V = 0.

            !=======================!
            !       U-points        !
            !=======================!
                do ij = 1, nU_pnts
                    i = indxUi(ij)
                    j = indxUj(ij)

                    !=======================================!
                    !   Get derivatives of known solution   !
                    !=======================================!
                        dw1dx       = toysoln_dw1dx(x_u(i), y_u(j), time)
                        dw1dy       = toysoln_dw1dy(x_u(i), y_u(j), time)
                        d2w1dx2     = toysoln_d2w1dx2(x_u(i), y_u(j), time)
                        d2w1dy2     = toysoln_d2w1dy2(x_u(i), y_u(j), time)
                        d2w1dxdy    = toysoln_d2w1dxdy(x_u(i), y_u(j), time)

                        dw2dx       = toysoln_dw2dx(x_u(i), y_u(j), time)
                        dw2dy       = toysoln_dw2dy(x_u(i), y_u(j), time)
                        d2w2dx2     = toysoln_d2w2dx2(x_u(i), y_u(j), time)
                        d2w2dy2     = toysoln_d2w2dy2(x_u(i), y_u(j), time)
                        d2w2dxdy    = toysoln_d2w2dxdy(x_u(i), y_u(j), time)

                            ! Store for test purposes !
                            ! deriv_U(i,j) = d2w2dxdy

                    !===============================!
                    !       Calculate Delta         !
                    !===============================!
                        strain_11   = dw1dx
                        strain_22   = dw2dy 
                        strain_12   = 0.5*(dw1dy + dw2dx)

                        delta       = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2)) &
                                            + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                            + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                            ! Store for test purposes !
                            ! delta_U(i,j) = delta 
                    !===================================!
                    !  Calculate derivatives of Delta^2 !
                    !===================================!
                        ddelta2_dx  = (1 + ellip_rat**(-2))*(2*strain_11*d2w1dx2 + 2*strain_22*d2w2dxdy)  &
                                        + 4*(ellip_rat**(-2))*strain_12*(d2w1dxdy + d2w2dx2)              &
                                        + 2*(1 - ellip_rat**(-2))*(strain_22*d2w1dx2 + strain_11*d2w2dxdy)

                        ddelta2_dy  = (1 + ellip_rat**(-2))*(2*strain_11*d2w1dxdy + 2*strain_22*d2w2dy2) &
                                        + 4*(ellip_rat**(-2))*strain_12*(d2w1dy2 + d2w2dxdy) &
                                        + 2*(1 - ellip_rat**(-2))*(strain_22*d2w1dxdy + strain_11*d2w2dy2)

                            ! Store for test purposes !
                            ! ddelta2_dx_U(i,j) = ddelta2_dx
                            ! ddelta2_dy_U(i,j) = ddelta2_dy

                    !===================================================!
                    !    Calculate Viscosities and Necessary Gradients  !
                    !===================================================!
                        cnst1   = 2*delta*maxvisc_cf + machine_eps
                        cnst2   = (maxvisc_cf**2)*P_u(i,j)/(delta*(cnst1**2) + machine_eps)

                        dPdx    = (1./dx)*(P_T(i  ,j  ) - P_T(i-1,j  ))
                        dPdy    = (1./dx)*(P_N(i  ,j+1) - P_N(i  ,j  ))

                            ! Store for testing !
                            ! dPdx_U(i,j) = dPdx  
                            ! dPdy_U(i,j) = dPdy

                        zeta_u(i,j) = maxvisc_cf*P_u(i,j)*tanh(1./cnst1)
                        eta_u(i,j)  = zeta_u(i,j)*(ellip_rat**(-2))

                        zeta_gx_u(i,j)  = maxvisc_cf*tanh(1./cnst1)*dPdx - cnst2*(1 - (tanh(1./cnst1))**2)*ddelta2_dx
                        eta_gx_u(i,j)   = zeta_gx_u(i,j)*(ellip_rat**(-2))
                        eta_gy_u(i,j)   = (ellip_rat**(-2))*maxvisc_cf*tanh(1./cnst1)*dPdy - (ellip_rat**(-2))*cnst2*(1 - (tanh(1./cnst1))**2)*ddelta2_dy
                end do 

            !=======================!
            !       V-points        !
            !=======================!
                do ij = 1, nV_pnts
                    i = indxVi(ij)
                    j = indxVj(ij)

                    !=======================================!
                    !   Get derivatives of known solution   !
                    !=======================================!
                        dw1dx       = toysoln_dw1dx(x_v(i), y_v(j), time)
                        dw1dy       = toysoln_dw1dy(x_v(i), y_v(j), time)
                        d2w1dx2     = toysoln_d2w1dx2(x_v(i), y_v(j), time)
                        d2w1dy2     = toysoln_d2w1dy2(x_v(i), y_v(j), time)
                        d2w1dxdy    = toysoln_d2w1dxdy(x_v(i), y_v(j), time)

                        dw2dx       = toysoln_dw2dx(x_v(i), y_v(j), time)
                        dw2dy       = toysoln_dw2dy(x_v(i), y_v(j), time)
                        d2w2dx2     = toysoln_d2w2dx2(x_v(i), y_v(j), time)
                        d2w2dy2     = toysoln_d2w2dy2(x_v(i), y_v(j), time)
                        d2w2dxdy    = toysoln_d2w2dxdy(x_v(i), y_v(j), time)

                            ! Store for test purposes !
                            ! deriv_V(i,j) = dw2dx

                    !===============================!
                    !       Calculate Delta         !
                    !===============================!
                        strain_11   = dw1dx
                        strain_22   = dw2dy 
                        strain_12   = 0.5*(dw1dy + dw2dx)

                        delta       = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2)) &
                                            + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                            + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                            ! store for test purposes !
                            ! delta_V(i,j) = delta

                    !===================================!
                    !  Calculate derivatives of Delta^2 !
                    !===================================!
                        ddelta2_dx  = (1 + ellip_rat**(-2))*(2*strain_11*d2w1dx2 + 2*strain_22*d2w2dxdy)  &
                                        + 4*(ellip_rat**(-2))*strain_12*(d2w1dxdy + d2w2dx2)              &
                                        + 2*(1 - ellip_rat**(-2))*(strain_22*d2w1dx2 + strain_11*d2w2dxdy)

                        ddelta2_dy  = (1 + ellip_rat**(-2))*(2*strain_11*d2w1dxdy + 2*strain_22*d2w2dy2) &
                                        + 4*(ellip_rat**(-2))*strain_12*(d2w1dy2 + d2w2dxdy) &
                                        + 2*(1 - ellip_rat**(-2))*(strain_22*d2w1dxdy + strain_11*d2w2dy2)

                            ! Store for testing !
                            ! ddelta2_dx_V(i,j) = ddelta2_dx
                            ! ddelta2_dy_V(i,j) = ddelta2_dy

                    !===================================================!
                    !    Calculate Viscosities and Necessary Gradients  !
                    !===================================================!

                        cnst1   = 2*delta*maxvisc_cf + machine_eps   
                        cnst2   = (maxvisc_cf**2)*P_v(i,j)/(delta*(cnst1**2) + machine_eps)

                        dPdx    = (1./dx)*(P_N(i+1,j  ) - P_N(i  ,j  ))
                        dPdy    = (1./dx)*(P_T(i  ,j  ) - P_T(i  ,j-1))

                            ! store for testing !
                            ! dPdx_V(i,j) = dPdx
                            ! dPdy_V(i,j) = dPdy

                        zeta_v(i,j) = maxvisc_cf*P_v(i,j)*tanh(1./cnst1)
                        eta_v(i,j)  = zeta_v(i,j)*(ellip_rat**(-2))

                        eta_gx_v(i,j)   = (ellip_rat**(-2))*maxvisc_cf*tanh(1./cnst1)*dPdx - (ellip_rat**(-2))*cnst2*(1 - (tanh(1./cnst1))**2)*ddelta2_dx
                        zeta_gy_v(i,j)  = maxvisc_cf*tanh(1./cnst1)*dPdy - cnst2*(1 - (tanh(1./cnst1))**2)*ddelta2_dy
                        eta_gy_v(i,j)   = (ellip_rat**(-2))*zeta_gy_v(i,j)
                end do 

                    ! open(unit = 1, file = "../Valid_Pblm/ViscGrads_Tests/SolnDeriv_Tests/derivU_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 2, file = "../Valid_Pblm/ViscGrads_Tests/SolnDeriv_Tests/derivV_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! call csv_write_dble_2d(1, deriv_U)
                    ! call csv_write_dble_2d(2, deriv_V)
                    ! close(1)
                    ! close(2)

                    ! open(unit = 1, file = "../Valid_Pblm/StrDeriv_Tests/dPdxU"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 2, file = "../Valid_Pblm/StrDeriv_Tests/dPdyU"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 3, file = "../Valid_Pblm/StrDeriv_Tests/dPdxV"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 4, file = "../Valid_Pblm/StrDeriv_Tests/dPdyV"//trim(dx_f)//".dat", status = "unknown")
                    ! call csv_write_dble_2d(1, dPdx_U)
                    ! call csv_write_dble_2d(2, dPdy_U)
                    ! call csv_write_dble_2d(3, dPdx_V)
                    ! call csv_write_dble_2d(4, dPdy_V)
                    ! close(1)
                    ! close(2)
                    ! close(3)
                    ! close(4)

                    ! open(unit = 1, file = "../Valid_Pblm/Visc_Tests/delta_tests/deltaU_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 2, file = "../Valid_Pblm/Visc_Tests/delta_tests/deltaV_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! call csv_write_dble_2d(1, delta_U)
                    ! call csv_write_dble_2d(2, delta_V)
                    ! close(1)
                    ! close(2)

                    ! open(unit = 1, file = "../Valid_Pblm/ViscGrads_Tests/delta2_Tests/ddelta2_dx_U_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 2, file = "../Valid_Pblm/ViscGrads_Tests/delta2_Tests/ddelta2_dy_U_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 3, file = "../Valid_Pblm/ViscGrads_Tests/delta2_Tests/ddelta2_dx_V_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 4, file = "../Valid_Pblm/ViscGrads_Tests/delta2_Tests/ddelta2_dy_V_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! call csv_write_dble_2d(1, ddelta2_dx_U)
                    ! call csv_write_dble_2d(2, ddelta2_dy_U)
                    ! call csv_write_dble_2d(3, ddelta2_dx_V)
                    ! call csv_write_dble_2d(4, ddelta2_dy_V)
                    ! close(1)
                    ! close(2)
                    ! close(3)
                    ! close(4)

                    ! open(unit = 1, file = "../Valid_Pblm/ViscGrads_Tests/dzdxU_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 2, file = "../Valid_Pblm/ViscGrads_Tests/dedxU_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 3, file = "../Valid_Pblm/ViscGrads_Tests/dedyU_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! call csv_write_dble_2d(1, zeta_gx_u)
                    ! call csv_write_dble_2d(2, eta_gx_u)
                    ! call csv_write_dble_2d(3, eta_gy_u)
                    ! close(1)
                    ! close(2)
                    ! close(3)

                    ! open(unit = 1, file = "../Valid_Pblm/ViscGrads_Tests/dzdyV_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 2, file = "../Valid_Pblm/ViscGrads_Tests/dedxV_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 3, file = "../Valid_Pblm/ViscGrads_Tests/dedyV_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! call csv_write_dble_2d(1, zeta_gy_v)
                    ! call csv_write_dble_2d(2, eta_gx_v)
                    ! call csv_write_dble_2d(3, eta_gy_v)
                    ! close(1)
                    ! close(2)
                    ! close(3)

                    ! open(unit = 1, file = "../Valid_Pblm/Visc_Tests/zU_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 2, file = "../Valid_Pblm/Visc_Tests/eU_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 3, file = "../Valid_Pblm/Visc_Tests/zV_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! open(unit = 4, file = "../Valid_Pblm/Visc_Tests/eV_exact"//trim(dx_f)//".dat", status = "unknown")
                    ! call csv_write_dble_2d(1, zeta_u)
                    ! call csv_write_dble_2d(2, eta_u)
                    ! call csv_write_dble_2d(3, zeta_v)
                    ! call csv_write_dble_2d(4, eta_v)
                    ! close(1)
                    ! close(2)
                    ! close(3)
                    ! close(4)
                    
    end subroutine vpbcalc_exactvisc_viscgrads

!===========================================================================================!
!                         Validation Pblm Forcing: add time deriv. component                !
!===========================================================================================!
    subroutine vpbadd_time_deriv(vpb_forc, w2_u, w1_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)

        !===================================================================================
        ! According to the time level w1 and w2 are currently defined at, this routine adds 
        ! the portion of the time derivative from that time level to vp_forc.
        !===================================================================================

        !==================!
        !------- In -------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &
            nU_pnts, nV_pnts
        integer(kind = int_kind), dimension(nU_pnts) :: &
            indxUi, indxUj 
        integer(kind = int_kind), dimension(nV_pnts) :: &
            indxVi, indxVj

        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            h_u, w2_u
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            h_v, w1_v

        !==================!
        !------ Inout -----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij 

        !===============================!
        !           U points            !
        !===============================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                vpb_forc(ij) = vpb_forc(ij) - (rho*h_u(i,j)*valid_pblm_vel/2)*w2_u(i,j)
            end do 
        !===============================!
        !           V points            !
        !===============================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vpb_forc(ij + nU_pnts) = vpb_forc(ij + nU_pnts) + (rho*h_v(i,j)*valid_pblm_vel/2)*w1_v(i,j)
            end do 
    end subroutine vpbadd_time_deriv

!===========================================================================================!
!                         Validation Pblm Forcing: add coriolis term                        !
!===========================================================================================!
    subroutine vpbadd_coriolis_term(vpb_forc, w2_u, w1_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)

        !==================================================================================
        ! According to the time level w1 and w2 are currently defined at, this routine adds 
        ! the portion of the time derivative from that time level to vp_forc.
        !==================================================================================

        !==================!
        !------- In -------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &
            nU_pnts, nV_pnts
        integer(kind = int_kind), dimension(nU_pnts) :: &
            indxUi, indxUj 
        integer(kind = int_kind), dimension(nV_pnts) :: &
            indxVi, indxVj

        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            h_u, w2_u
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            h_v, w1_v

        !==================!
        !------ Inout -----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij 

        !===============================!
        !           U points            !
        !===============================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                vpb_forc(ij) = vpb_forc(ij) + (rho*h_u(i,j)*f)*w2_u(i,j)
            end do 
        !===============================!
        !           V points            !
        !===============================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vpb_forc(ij + nU_pnts) = vpb_forc(ij + nU_pnts) - (rho*h_v(i,j)*f)*w1_v(i,j)
            end do
    end subroutine vpbadd_coriolis_term

!===========================================================================================!
!                         Validation Pblm Forcing: add air drag term                        !
!===========================================================================================!
    subroutine vpbadd_airdrag_term(vpb_forc, uwnd_u, vwnd_u, uwnd_v, vwnd_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)

        !===================================================================================
        ! For time level where w1 and w2 are currently defined, this routine updates vpb_forc
        ! to include the contribution from the air drag term at that time level
        !===================================================================================

        !==================!
        !------- In -------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &
            nU_pnts, nV_pnts
        integer(kind = int_kind), dimension(nU_pnts) :: &
            indxUi, indxUj 
        integer(kind = int_kind), dimension(nV_pnts) :: &
            indxVi, indxVj

        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            uwnd_u, vwnd_u
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            uwnd_v, vwnd_v

        !==================!
        !------ Inout -----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij
        real(kind = dbl_kind) :: &
            c                       ! just a constant to clean up the calculations

        !===========================!
        !          U points         !
        !===========================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                c               = rho_a*Cd_a*sqrt( uwnd_u(i,j)**2 + vwnd_u(i,j)**2 )
                vpb_forc(ij)    = vpb_forc(ij) + c*(uwnd_u(i,j)*cos_a - vwnd_u(i,j)*sin_a) 
            end do 

        !===========================!
        !          V points         !
        !===========================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                c                       = rho_a*Cd_a*sqrt( uwnd_v(i,j)**2 + vwnd_v(i,j)**2 )
                vpb_forc(ij + nU_pnts)  = vpb_forc(ij + nU_pnts) + c*(vwnd_v(i,j)*cos_a + uwnd_v(i,j)*sin_a)
            end do 
    end subroutine vpbadd_airdrag_term

!===========================================================================================!
!                         Validation Pblm Forcing: add water drag term                      !
!===========================================================================================!
    subroutine vpbadd_waterdrag_term(vpb_forc, w1_u, w2_u, w1_v, w2_v, uocn_u, vocn_u, uocn_v, vocn_v, Cw_u, Cw_v, &
                                        nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)

        !==================!
        !------- In -------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &
            nU_pnts, nV_pnts
        integer(kind = int_kind), dimension(nU_pnts) :: &
            indxUi, indxUj 
        integer(kind = int_kind), dimension(nV_pnts) :: &
            indxVi, indxVj

        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            w1_u, w2_u, uocn_u, vocn_u, Cw_u
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            w1_v, w2_v, uocn_v, vocn_v, Cw_v

        !==================!
        !------ Inout -----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij 

        !===========================!
        !       U points            !
        !===========================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                vpb_forc(ij) = vpb_forc(ij) - (Cw_u(i,j))*( (w1_u(i,j) - uocn_u(i,j))*cos_w - (w2_u(i,j) - vocn_u(i,j))*sin_w )                
            end do 
        !===========================!
        !       V points            !
        !===========================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vpb_forc(ij + nU_pnts) = vpb_forc(ij + nU_pnts) - (Cw_v(i,j))*( (w2_v(i,j) - vocn_v(i,j))*cos_w + (w1_v(i,j) - uocn_v(i,j))*sin_w )
            end do 
    end subroutine vpbadd_waterdrag_term

!===========================================================================================!
!                         Validation Pblm Forcing: add sea surface tilt term                !
!===========================================================================================!
    subroutine vpbadd_seasurf_term(vpb_forc, vocn_u, uocn_v, h_u, h_v, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi ,indxVj)

        !==================!
        !------- In -------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &
            nU_pnts, nV_pnts
        integer(kind = int_kind), dimension(nU_pnts) :: &
            indxUi, indxUj 
        integer(kind = int_kind), dimension(nV_pnts) :: &
            indxVi, indxVj

        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            h_u, vocn_u
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            h_v, uocn_v

        !==================!
        !------ Inout -----!
        !==================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc

        !==================!
        !------ Local -----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij 

        !===============================!
        !           U points            !
        !===============================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                vpb_forc(ij) = vpb_forc(ij) - (rho*h_u(i,j)*f)*vocn_u(i,j)
            end do 
        !===============================!
        !           V points            !
        !===============================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                vpb_forc(ij + nU_pnts) = vpb_forc(ij + nU_pnts) + (rho*h_v(i,j)*f)*uocn_v(i,j)
            end do

    end subroutine vpbadd_seasurf_term

!===========================================================================================!
!                         Validation Pblm Forcing: add rheology term                        !
!===========================================================================================!
    subroutine vpbadd_rheo_term(vpb_forc, time, eta_T, zeta_T, eta_N, zeta_N, eta_u, zeta_u, eta_v, zeta_v, &
                                nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

        !====================================================================================
        ! NOTE: THIS ROUTINE IS OUT OF DATE AND HAS BEEN REPLACED BY vpbadd_rheo_term_r02. THE 
        ! NEW ROUTINE CALCULATES THE RHEOLOGY TERM WITH THE VISCOSITY DERIVATIVES IN THEIR CLOSED 
        ! FORM.
        !
        ! Using the exact viscosities at the time level defined by time, this routine calculates the 
        ! rheology term's contribution to the validation problem forcing term. Note that although 
        ! exact forms of the validation solution are known, we do not have closed forms for the 
        ! derivatives of the viscosities and thus centered differences are still used to calculate 
        ! these derivatives.
        !
        ! NOTE: This routine does not add the pressure term!
        !====================================================================================

        !===================!
        !------- In --------! 
        !===================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN,     &   ! index limits
            nU_pnts, nV_pnts                                ! number of U and V points in the comp domain 
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                                  ! locations of U points in the comp domain 
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                                  ! locations of V points in the comp domain 

        real(kind = dbl_kind), intent(in) :: &
            time                                            ! time level of interest 
        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            eta_u, zeta_u 
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            eta_v, zeta_v
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            eta_T, zeta_T 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            eta_N, zeta_N 

        !===================!
        !----- In/out ------! 
        !===================!
        real(kind = dbl_kind), dimension(nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc 

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij                ! local indices

        real(kind = dbl_kind) :: &
            c1, c2, c3, &           ! local constants to clean up calculations
            rheo 

        !===================================!
        !           U points                !
        !===================================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ! calc visc gradients !
                c1 = (1./dx)*( (eta_T(i,j) + zeta_T(i,j)) - (eta_T(i-1,j) + zeta_T(i-1,j)) )    ! d(eta + zeta)dx 
                c2 = (1./dx)*( (zeta_T(i,j) - eta_T(i,j)) - (zeta_T(i-1,j) - eta_T(i-1,j)) )    ! d(zeta - eta)dx 
                c3 = (1./dx)*(eta_N(i,j+1) - eta_N(i,j))                                        ! d(eta)dy 

                ! calc rheology term (less the pressure term) !
                rheo =      c1*toysoln_dw1dx(x_u(i),y_u(j),time) + (eta_u(i,j) + zeta_u(i,j))*toysoln_d2w1dx2(x_u(i),y_u(j),time)   & 
                        +   c2*toysoln_dw2dy(x_u(i),y_u(j),time) + (zeta_u(i,j) - eta_u(i,j))*toysoln_d2w2dxdy(x_u(i),y_u(j),time)  &
                        +   c3*toysoln_dw1dy(x_u(i),y_u(j),time) + eta_u(i,j)*toysoln_d2w1dy2(x_u(i),y_u(j),time)                   &
                        +   c3*toysoln_dw2dx(x_u(i),y_u(j),time) + eta_u(i,j)*toysoln_d2w2dxdy(x_u(i),y_u(j),time)

                ! add half the rheology term to vpb_forc !
                vpb_forc(ij) = vpb_forc(ij) + 0.5*rheo 
            end do 

        !===================================!
        !           V points                !
        !===================================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                ! calc visc gradients !
                c1 = (1./dx)*(eta_N(i+1,j) - eta_N(i,j))                                        ! d(eta)dx
                c2 = (1./dx)*( (eta_T(i,j) + zeta_T(i,j)) - (eta_T(i,j-1) + zeta_T(i,j-1)) )    ! d(eta + zeta)dy 
                c3 = (1./dx)*( (zeta_T(i,j) - eta_T(i,j)) - (zeta_T(i,j-1) - eta_T(i,j-1)) )    ! d(zeta - eta)dy

                ! calc rheology term (less the pressure term) !
                rheo =      c1*toysoln_dw1dy(x_v(i),y_v(j),time) + eta_v(i,j)*toysoln_d2w1dxdy(x_v(i),y_v(j),time)                  &
                        +   c1*toysoln_dw2dx(x_v(i),y_v(j),time) + eta_v(i,j)*toysoln_d2w2dx2(x_v(i),y_v(j),time)                   &
                        +   c2*toysoln_dw2dy(x_v(i),y_v(j),time) + (eta_v(i,j) + zeta_v(i,j))*toysoln_d2w2dy2(x_v(i),y_v(j),time)   &
                        +   c3*toysoln_dw1dx(x_v(i),y_v(j),time) + (zeta_v(i,j) - eta_v(i,j))*toysoln_d2w1dxdy(x_v(i),y_v(j),time)

                ! add half the rheology term to vpb_forc !
                vpb_forc(ij + nU_pnts) = vpb_forc(ij + nU_pnts) + 0.5*rheo 
            end do 
    end subroutine vpbadd_rheo_term

!===========================================================================================!
!             Validation Pblm Forcing: add rheology term (new formulation)                  !
!===========================================================================================!
    subroutine vpbadd_rheo_term_r02(vpb_forc, time, zeta_u, eta_u, zeta_v, eta_v, &
                                    zeta_gx_u, zeta_gy_v, eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, &
                                    nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

        !====================================================================================
        ! Using the exact viscosities and their derivatives, this routine calculates the rheology
        ! term at the time level defined by the "time" variable. Note that this routine uses our 
        ! new formulation of the rheology term, where we calculate the viscosity derivatives in 
        ! a closed form. 
        !
        ! Only half of rheo is added to vpb_forc at each point due to the Crank-Nicolson 
        ! discretization.
        !
        ! NOTE: This routine does not add the pressure term!
        !====================================================================================

        !===================!
        !------- In --------! 
        !===================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &   ! index limits for U and V points
            nU_pnts, nV_pnts            ! number of U and V points in the computational domain 
        integer(kind = int_kind), dimension (nU_pnts), intent(in) :: &
            indxUi, indxUj              ! locations of U points in the computational domain 
        integer(kind = int_kind), dimension (nV_pnts), intent(in) :: &
            indxVi, indxVj              ! locations of V points in the computational domain

        real(kind = dbl_kind), intent(in) :: &
            time                        ! defines what time level we are currently at 
        real(kind = dbl_kind), dimension (nxU, nyU), intent(in) :: & 
            zeta_u, eta_u,                  &   ! viscosities - U points 
            zeta_gx_u, eta_gx_u, eta_gy_u       ! needed viscosity derivatives - U points 
        real(kind = dbl_kind), dimension (nxV, nyV), intent(in) :: &
            zeta_v, eta_v,                  &   ! viscosities - V points
            zeta_gy_v, eta_gx_v, eta_gy_v       ! needed viscosity derivatives - V points 

        !===================!
        !----- In/out ------! 
        !===================!
        real(kind = dbl_kind), dimension (nU_pnts + nV_pnts), intent(inout) :: &
            vpb_forc                ! current forcing vector to be updated 

        !==================!
        !----- Local ------!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij                ! local indices

        real(kind = dbl_kind) :: &
            rheo                    ! rheology term

        !=========================!
        ! U component of the SIME !
        !=========================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij) 

                rheo    = (eta_gx_u(i,j) + zeta_gx_u(i,j))* toysoln_dw1dx(x_u(i), y_u(j), time)     &
                        + (eta_u(i,j) + zeta_u(i,j))*       toysoln_d2w1dx2(x_u(i), y_u(j), time)   &
                        + (zeta_gx_u(i,j) - eta_gx_u(i,j))* toysoln_dw2dy(x_u(i), y_u(j), time)     &
                        + (zeta_u(i,j) - eta_u(i,j))*       toysoln_d2w2dxdy(x_u(i), y_u(j), time)  &
                        + eta_gy_u(i,j)*toysoln_dw1dy(x_u(i), y_u(j), time)     &
                        + eta_u(i,j)*   toysoln_d2w1dy2(x_u(i), y_u(j), time)   &
                        + eta_gy_u(i,j)*toysoln_dw2dx(x_u(i), y_u(j), time)     &
                        + eta_u(i,j)*   toysoln_d2w2dxdy(x_u(i), y_u(j), time)

                ! Update vpb_forc !
                vpb_forc(ij) = vpb_forc(ij) + rheo
            end do 

        !=========================!
        ! V component of the SIME !
        !=========================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                rheo    = eta_gx_v(i,j)*toysoln_dw2dx(x_v(i), y_v(j), time)     &
                        + eta_v(i,j)*   toysoln_d2w2dx2(x_v(i), y_v(j), time)   &
                        + eta_gx_v(i,j)*toysoln_dw1dy(x_v(i), y_v(j), time)     &
                        + eta_v(i,j)*   toysoln_d2w1dxdy(x_v(i), y_v(j), time)  &
                        + (eta_gy_v(i,j) + zeta_gy_v(i,j))* toysoln_dw2dy(x_v(i), y_v(j), time)     &
                        + (eta_v(i,j) + zeta_v(i,j))*       toysoln_d2w2dy2(x_v(i), y_v(j), time)   &
                        + (zeta_gy_v(i,j) - eta_gy_v(i,j))* toysoln_dw1dx(x_v(i), y_v(j), time)     &
                        + (zeta_v(i,j) - eta_v(i,j))*       toysoln_d2w1dxdy(x_v(i), y_v(j), time)

                ! Update vpb_force !
                vpb_forc(nU_pnts + ij) = vpb_forc(nU_pnts + ij) + rheo
            end do 
    end subroutine vpbadd_rheo_term_r02
end module validation_routines
