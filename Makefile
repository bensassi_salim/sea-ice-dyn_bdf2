# Makefile for the JFNK Solver

##### NOTE #####
# If the compiler is changed, there may be portability issues due to line length, at a later date, I should clean up the code #
comp 	= ifort
flags 	= -mkl

objects	= 	csv_file.o modelparams.o initialization.o forcing.o\
			domain_routines.o var_routines.o solver_routines.o validation_routines.o\
			validsoln_routines.o JFNKsolver.o 

# Possible debugging flags:
# 	-g -C -ftrapuv - fp-model strict -fpe-all=0 -no-ftz -traceback -warn

# If running on valgrind to check for memory errors, use valgrind --track-origins=yes ./JFNKsolver

JFNKsolver : 	$(objects)
	$(comp) -g -traceback -O $(flags) -o JFNKsolver $(objects)

csv_file.o : csv_file.f90
	$(comp) -g -traceback -O -c csv_file.f90

modelparams.o : modelparams.f90
	$(comp) -g -traceback -O -c modelparams.f90

initialization.o : initialization.f90 modelparams.o 
	$(comp) -g -traceback -O -c initialization.f90

forcing.o : forcing.f90 modelparams.o initialization.o 
	$(comp) -g -traceback -O -c forcing.f90 

validsoln_routines.o : validsoln_routines.f90 modelparams.o initialization.o csv_file.o
	$(comp) -g -traceback -O -c validsoln_routines.f90

domain_routines.o : domain_routines.f90 csv_file.o modelparams.o initialization.o validsoln_routines.o
	$(comp) -g -traceback -O -c domain_routines.f90 

var_routines.o : var_routines.f90 csv_file.o modelparams.o initialization.o domain_routines.o
	$(comp) -g -traceback -O -c var_routines.f90 

solver_routines.o : solver_routines.f90 csv_file.o modelparams.o initialization.o\
					forcing.o var_routines.o domain_routines.o
	$(comp) -g -traceback -O $(flags)  -c solver_routines.f90

validation_routines.o : validation_routines.f90 csv_file.o modelparams.o initialization.o\
						forcing.o var_routines.o solver_routines.o domain_routines.o validsoln_routines.o
	$(comp) -g -traceback -O -c validation_routines.f90

JFNKsolver.o : JFNKsolver.f90 csv_file.o modelparams.o initialization.o forcing.o\
				domain_routines.o solver_routines.o var_routines.o validation_routines.o validsoln_routines.o 
	$(comp) -g -traceback -O -c JFNKsolver.f90
