! This module contains routines that are used to calculate the exact solution to 
! the validation problem and its derivatives.
! Original Author: Clinton Seinen
!
! The contained routines are:

module validsoln_routines

! -----------------------------------------------------------------------------!
    !                       Module References                           !
    use modelparams         ! includes physical, numerical, and grid parameters
    use initialization      ! includes grid parameters that are defined according to dx and will remain unchanged
    use csv_file            ! module for writing data to csv format
! -----------------------------------------------------------------------------!

implicit none

! -----------------------------------------------------------------------------!
    !                           Contains                                !
! -----------------------------------------------------------------------------!

contains

!===========================================================================================!
!                               toy solution - w1                                           !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_w1(x,y,t)

        !====================================================================================
        ! This routine was written in order to quickly evaluate first component of the toy 
        ! solution.
        !
        ! Note: our toy solution is w = [w1, w2]^T
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        toysoln_w1 = (1./10)*sin( (4*x/x_extent - 2)**2 + (4*y/y_extent - 2)**2 + valid_pblm_vel*t )
    end function toysoln_w1

!===========================================================================================!
!                               toy solution - dw1/dx                                       !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_dw1dx(x,y,t)

        !====================================================================================
        ! This routine evaluates the x derivatives for w1 for the toy solution for the validation 
        ! problem 
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_dw1dx = (16./(x_extent**2))*(2*x - x_extent)*toysoln_w2(x,y,t)
    end function toysoln_dw1dx

!===========================================================================================!
!                               toy solution - dw1/dy                                       !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_dw1dy(x,y,t)

        !====================================================================================
        ! This routine evaluates the y derivatives for w1 for the toy solution for the validation 
        ! problem 
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_dw1dy = (16./(y_extent**2))*(2*y - y_extent)*toysoln_w2(x,y,t)
    end function toysoln_dw1dy

!===========================================================================================!
!                               toy solution - d2w1/dx2                                     !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_d2w1dx2(x,y,t)

        !====================================================================================
        ! This routine evaluates the 2nd x derivatives for w1 for the toy solution for the validation 
        ! problem 
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_d2w1dx2 = (32./(x_extent**2))*toysoln_w2(x,y,t) - (256./(x_extent**4))*((2*x - x_extent)**2)*toysoln_w1(x,y,t)           
    end function toysoln_d2w1dx2

!===========================================================================================!
!                               toy solution - d2w1/dy2                                     !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_d2w1dy2(x,y,t)

        !====================================================================================
        ! This routine evaluates the 2nd y derivatives for w1 for the toy solution for the validation 
        ! problem 
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time
        
            toysoln_d2w1dy2 = (32./(y_extent**2))*toysoln_w2(x,y,t) - (256./(y_extent**4))*((2*y - y_extent)**2)*toysoln_w1(x,y,t)
    end function toysoln_d2w1dy2

!===========================================================================================!
!                               toy solution - d2w1/dxdy                                    !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_d2w1dxdy(x,y,t)

        !====================================================================================
        ! This routine evaluates the mixed derivatives for w1 for the toy solution for the validation 
        ! problem 
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_d2w1dxdy = (-256./((x_extent**2)*(y_extent**2)))*(2*x - x_extent)*(2*y - y_extent)*toysoln_w1(x,y,t)        
    end function toysoln_d2w1dxdy

!===========================================================================================!
!                               toy solution - w2                                           !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_w2(x,y,t)

        !====================================================================================
        ! This routine was written in order to quickly evaluate second component of the toy 
        ! solution.
        !
        ! Note: our toy solution is w = [w1, w2]^T
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

        toysoln_w2 = (1./10)*cos( (4*x/x_extent - 2)**2 + (4*y/y_extent - 2)**2 + valid_pblm_vel*t )
    end function toysoln_w2

!===========================================================================================!
!                               toy solution - dw2/dx                                       !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_dw2dx(x,y,t)

        !====================================================================================
        ! This function evaluates the x derivatives of w2 for the solution to the validation problem
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_dw2dx = (-16./(x_extent**2))*(2*x - x_extent)*toysoln_w1(x,y,t)
    end function toysoln_dw2dx

!===========================================================================================!
!                               toy solution - dw2/dy                                       !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_dw2dy(x,y,t)

        !====================================================================================
        ! This function evaluates the y derivatives of w2 for the solution to the validation problem
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_dw2dy = (-16./(y_extent**2))*(2*y - y_extent)*toysoln_w1(x,y,t)
    end function toysoln_dw2dy

!===========================================================================================!
!                               toy solution - d2w2/dx2                                     !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_d2w2dx2(x,y,t)

        !====================================================================================
        ! This function evaulates the 2nd derivatives for w2 of the solution to the validation
        ! problem, in the x direction
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_d2w2dx2 = (-32./(x_extent**2))*toysoln_w1(x,y,t) - (256./(x_extent**4))*((2*x - x_extent)**2)*toysoln_w2(x,y,t)
    end function toysoln_d2w2dx2

!===========================================================================================!
!                               toy solution - d2w2/dy2                                     !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_d2w2dy2(x,y,t)

        !====================================================================================
        ! This function evaulates the 2nd derivatives for w2 of the solution to the validation
        ! problem, in the y direction
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_d2w2dy2 = (-32./(y_extent**2))*toysoln_w1(x,y,t) - (256./(y_extent**4))*((2*y - y_extent)**2)*toysoln_w2(x,y,t)
    end function toysoln_d2w2dy2

!===========================================================================================!
!                               toy solution - d2w2/dxdy                                    !
!===========================================================================================!
    real(kind = dbl_kind) function toysoln_d2w2dxdy(x,y,t)

        !====================================================================================
        ! This function evaulates the mixed derivatives for w2 of the solution to the validation
        ! problem
        !====================================================================================

        !==================!
        !--------In--------!
        !==================!
        real(kind = dbl_kind), intent(in) :: &
            x, y, t                             ! x and y position and time

            toysoln_d2w2dxdy = (-256./((x_extent**2)*(y_extent**2)))*(2*x - x_extent)*(2*y - y_extent)*toysoln_w2(x,y,t)            
        end function toysoln_d2w2dxdy

!===============================================================================!
!                               Save Exact Solutions                            !
!===============================================================================!
    subroutine save_exact(time, nxU, nyU, nxV, nyV, nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)

        !====================================================================================
        ! This subroutine saves the exact solutions to the validation problem at with t = time.
        !
        ! The following inputs are optional:
        !       - indxUi, indxUj
        !       - indxVi, indxVj 
        !
        ! If they are not present, this routine saves the solution over the entire grid. If they 
        ! are, it only populates the exact solution to where ice is present.
        ! Note that regardless if they are used or not, nU_pnts and nV_pnts must be sent to this 
        ! routine!
        !====================================================================================

        !==================!
        !------ In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &   ! index limit - U points 
            nxV, nyV        ! index limit - V points

        integer(kind = int_kind), intent(in) :: &
            nU_pnts,    &   ! number of U points in the computational domain 
            nV_pnts         ! number of V points in the computational domain
        integer(kind = int_kind), optional, dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj  ! indices of U points in computational domain.
        integer(kind = int_kind), optional, dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj  ! indices of V points in computational domain

        real(kind = dbl_kind), intent(in) :: &
            time            ! defines the time level of the exact solution 

        !==================!
        !------ Local -----!
        !==================!
        character(len = 8) :: &
            fmt1, fmt2,     &   ! formats for output filenames
            dx_f, dt_f,     &   ! strings for dx and dt 
            t_lvl               ! time string 

        integer(kind = int_kind) :: &
            i,j,ij 

        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U) :: &
            u_soln          
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V) :: &
            v_soln

        ! Define strings for output !
            fmt1 = '(I4.4)'
            fmt2 = '(I8.8)'
            write(dx_f, fmt1) int(dx/1000)  ! turn dx into a string (km)
            write(dt_f, fmt1) int(dt)       ! turd dt into a string (s)
            write(t_lvl,fmt2) int(time)     ! turn time into a string (s)

        ! Initialize Arrays !
            u_soln = 0.
            v_soln = 0.

        if (present(indxUi) .and. present(indxUj) &
                .and. present(indxVi) .and. present(indxVj)) then
            ! Populate solution only where ice is present !

            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                u_soln(i,j) = toysoln_w1(x_u(i), y_u(j), time)
            end do 
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                v_soln(i,j) = toysoln_w2(x_v(i), y_v(j), time)
            end do
        else 
            ! Populate solution over entire domain !
            do i = 1, nxU
                do j = 1, nyU
                    u_soln(i,j) = toysoln_w1(x_u(i), y_u(j), time)
                end do 
            end do 
            do i = 1, nxV 
                do j = 1, nyV
                    v_soln(i,j) = toysoln_w2(x_v(i), y_v(j), time)
                end do 
            end do 
        end if 

        ! Save Values !
            open(unit = 1, file = './output/usoln_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
            open(unit = 2, file = './output/vsoln_dx'//trim(dx_f)//'dt'//trim(dt_f)//'time'//trim(t_lvl)//'.dat', status = 'unknown')
            call csv_write_dble_2d(1, u_soln)
            call csv_write_dble_2d(2, v_soln)
            close(1)
            close(2)

    end subroutine save_exact
end module validsoln_routines
