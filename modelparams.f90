! This module contains the various parameters needed for the sea ice dynamics solver
! Original Author: Clinton Seinen

module modelparams

implicit none
!-----------------------------------------------------------------------------------------------------!
    !                   Kind Parameters                            !
! ----------------------------------------------------------------------------------------------------!
    integer, parameter :: &
        int_kind    = selected_int_kind(6),     &
        real_kind   = selected_real_kind(6),    &
        dbl_kind    = selected_real_kind(13)
!-----------------------------------------------------------------------------------------------------!
    !                   Misc Parameters                            !
! ----------------------------------------------------------------------------------------------------!
    real(kind = dbl_kind), parameter :: &
        pi          = 4.0*atan(1.0),    &   ! pi
        machine_eps = epsilon(pi)           ! machine epsilon with same kind as dbl_kind           
! ----------------------------------------------------------------------------------------------------!
    !                   Physical Parameters                             !
    !       Note: most current values are taken from Lemieux 2012       !
! ----------------------------------------------------------------------------------------------------!
    real(kind = dbl_kind), parameter :: &
        rho         = 900_dbl_kind,         &   ! sea ice density (kg/m^3)
        rho_a       = 1.3_dbl_kind,         &   ! air density (kg/m^3)
        rho_w       = 1026_dbl_kind,        &   ! water density (kg/m^3)
        Cd_a        = 1.2e-3_dbl_kind,      &   ! air drag coef
        Cd_w        = 5.5e-3_dbl_kind,      &   ! water drag coef
        thetad_a    = 25*pi/180_dbl_kind,   &   ! air drag turning angle
        cos_a       = cos(thetad_a),        &   ! cosine of air drag turning angle
        sin_a       = sin(thetad_a),        &   ! sine of air drag turning angle
        thetad_w    = 25*pi/180_dbl_kind,   &   ! water drag turning angle
        cos_w       = cos(thetad_w),        &   ! cosine of water drag turning angle
        sin_w       = sin(thetad_w),        &   ! sine of water drag turning angle
        f           = 1.46e-4_dbl_kind,     &   ! f-plane approximation coriolis parameter (1/sec)
        str_param   = 27.5e3_dbl_kind,      &   ! ice strength parameter (N/m^2)
        conc_param  = 20_dbl_kind,          &   ! ice concentration parameter
        ellip_rat   = 2_dbl_kind,           &   ! ellipse ratio of yield curve
        grav        = 9.81_dbl_kind,        &   ! acceleration due to gravity (m/s^2)
        maxvisc_cf  = 2.5e8_dbl_kind,       &   ! coefficient used to set the max zeta values (s)
        land_inf    = 0.2e3_dbl_kind,       &   ! land influence distance (m) - this is used to define how close ice needs to be to land for a land BC to be activated
        h_init      = 1.0_dbl_kind              ! initial ice thickness                      
        
! ----------------------------------------------------------------------------------------------------!
    !                   Grid Parameters                                 !
! ----------------------------------------------------------------------------------------------------!
    real(kind = dbl_kind), parameter :: &
        x_extent = 2000e3_dbl_kind,      &  ! "east-west" extent (m)
        y_extent = 2000e3_dbl_kind,      &  ! "north-south" extent (m)
        dx       = 20e3_dbl_kind            ! spatial resolution (m)
            
! ----------------------------------------------------------------------------------------------------!
    !                   Numerical Parameters                            !
! ----------------------------------------------------------------------------------------------------!
    real(kind = dbl_kind), parameter :: &
        !T              = 52*7*24*60*60_dbl_kind, &   ! total time (sec)
         T              = 7*24*60*60_dbl_kind,    &    
        ! T             = 20*3600_dbl_kind,       &   ! total time (sec) for tests
        !dt             = 1*60*60_dbl_kind,       &   ! time step (sec) [Lemieux uses 10, 20, and 30 min in the 2012 paper]
        dt              = 10*60_dbl_kind,         &
        sv_interval     = 24*60*60_dbl_kind,      &   ! defines how often we want the solver the save the solutions
       !sv_interval     = 4*24*60*60_dbl_kind,    &
        ! sv_interval     = 3600_dbl_kind,        &   ! 
        tol_nl          = 1e-7_dbl_kind,          &   ! non-linear tolerance as used by Lemieux [NOT USED]
        disctol_coef_nl = 1e+1_dbl_kind,          &   ! used in the calculation of the lower limit tolerance for the non-linear residual [typically use 10]
        plat_tol        = 0.997_dbl_kind,         &   ! defines the required decrease in residual from iteration to iteration
        AD_plat_tol     = 0.997_dbl_kind,         &   ! defines the plateau tolerance when adaptive damping is used
        ResInc_tol      = 1.1_dbl_kind,           &   ! defines the allowable residual increase between non-linear iterations
        typ_vel         = 0.1_dbl_kind,           &   ! used in the calculation of the lower limit tolerance for the non-linear residual. This value represents the order of magnitude for a typical velocity
        eps             = 1e-6_dbl_kind,          &   ! epsilon used in Jacobian approximation
        gmres_tol_ini   = 0.99_dbl_kind,          &   ! initial tolerance for the linear solver
        gmres_tol_min   = 0.1_dbl_kind,           &   ! minimum tolerance for the linear solver
        res_t           = 0.625_dbl_kind,         &   ! denotes when the linear tolerance starts tightening up [Lemieux changes this with dx, i.e. (dx/res_t) 80km/0.05, 40km/0.25, 20km/0.625, 10km/1.25]
        valid_pblm_vel  = 5e-6_dbl_kind,        &   ! for the validation problem. defines the speed of wave propoagation (1/sec)
        tau_init        = 1_dbl_kind,           &   ! default value for "damping" (typically 10 for sea ice sims)
        tau_incfact     = 2_dbl_kind,           &   ! if a residual increase is noted, set tau = tau*tau_incfact
        tau_decfact     = 2_dbl_kind,           &   ! if a small residual decrease is noted, set tau = tau*tau_decfact
        tau_lim         = 32_dbl_kind,          &   ! specifies the maximum tau, the solver will terminate if unable to decrease the res with a value this large
        dectau_tol      = 0.9_dbl_kind,         &   ! if residual decreased by 1 - dectau_tol %, increase step size (used in adaptive damping) 
        slope_theta     = 2_dbl_kind,           &   ! in slope calculations, defines how much to increase 1 sided slopes for comparison (must be between 0 and 2)
        dt_smth         = 60*60_dbl_kind,       &   ! the time step used in the smoothing process defined in the SmoothUV routine (solver routines)
        visc_smth       = 1e12_dbl_kind             ! the hyperviscosity used in SmoothUV (solver_routines)

    integer(kind = int_kind), parameter :: &
        lin_tol_reset   = 100,                  & ! reset the linear tolerance back to gmres_tol_ini after this many non-linear iterations
        gmres_iter_max  = 2000,                 & ! maximum number of linear iterations
        gmres_rest      = 10000,                & ! restart value for the gmres routine (set to be very high to avoid restarts at the moment)
        !k_max           = 30,                   & ! max number of non-linear iterations [Lemieux sets to 200]
         k_max           = 200,                 & 
        platinc_check   = 5,                    & ! the minimum number of non-lin iterations before the solver checks for residual plateau
        distcrrctn      = 10,                   & ! if dist_correct = .true., this defines how often we want the distance function corrected (in terms of time-steps)
        distcrrctn_lim  = 10,                   & ! if dist_correct = .true., this defines how many iterations we allow for each correction
        gc_T            = 3,                    & ! number of ghost cells for T point arrays. Three has been chosen here for smoothing purposes
        gc_N            = 0,                    & ! number of ghost cells for N point arrays
        gc_U            = 2,                    & ! number of ghost cells for U point arrays
        gc_V            = 2,                    & ! number of ghost cells for V point arrays         
        smth_num        = 3,                    & ! defines the number of points included in the smoothing process
        domainconfig    = 9                       ! see initialization module, init_T_msk. This value defines the configuration type for the domain.

    logical, parameter :: &
        lin_precond         = .false.,          &   ! apply linear preconditioning or not
        preload_init_vels   = .false.,          &   ! no use in other codes
        toy_init_vels       = .false.,          &   ! if set to true, a toy velocity field is used as the initial condition. This functionality was implemented for test purposes and doesn't represent a physical situation (no use in other codes). 
        valid_pblm          = .true.,           &   ! if set to true, the JFNK solver solves the validation problem
        second_ord_Jac      = .true.,           &   ! if set to true, the 2nd order approximation is used 
        Adaptive_Damp       = .false.,          &   ! if set to true, the JFNK solver uses adaptive damping 
        AD_plateau_check    = .false.,          &   ! ONLY AFFECTS ADAPTIVE DAMPING - if set to true, the solver checks for plateuing if tau = 1
        ResInc_Term         = .false.,          &   ! if set to true, the JFNK solver will check for large residual increases; if one is noted the previous NL iterate is used as the solution (conditional termination in section 3.3) .
        plateau_check       = .false.,          &   ! if set the true, the JFNK solver checks for plateau'ing of solution (constant damping).
        dist_correct        = .false.,          &   ! if set to true, the solver corrects the distance function according to distcrrctn and distcrrctn_lim
        smooth_vels         = .false.,          &   ! if set to true, the solver smooths the resulting solution from the JFNK solver
        init_h_homo         = .true.                ! If init_h_homo is set to true, this routine makes h = 1 every where there is ice.   If not it uses undulating sine waves (values vary between 2.1 and 0.1 m). 
end module modelparams
